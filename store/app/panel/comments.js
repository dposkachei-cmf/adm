import api from "../../../api";
import serialize from "../../../plugins/common/serialize";
import beforeFetch from "../../../plugins/common/beforeFetch";
import axiosStore from "../../../plugins/common/axiosStore";

const state = () => ({
    locale: 'ru',
    model: null, // users, articles
    id: null, // {} active object from table
    errors: {},
    data: [],
    pending: false,
    loading: false,
    created: false,
    withoutLoading: false,
});

const getters = {
    locale(state) {
        return state.locale;
    },
    model(state) {
        return state.model;
    },
    id(state) {
        return state.id;
    },
    errors(state) {
        return state.errors;
    },
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    loading(state) {
        return state.loading;
    },
    created(state) {
        return state.created;
    },
    withoutLoading(state) {
        return state.withoutLoading;
    },
};

const actions = {

    /**
     *
     * @param commit
     * @param {string} locale
     */
    setLocale({ commit }, locale) {
        commit('locale', locale);
    },

    /**
     *
     * @param commit
     * @param state
     * @param data
     * @returns {Promise<void>}
     */
    async fetchComments({ commit, state }, data) {
        const self = this;
        commit('errors', {});
        commit('loading', true);
        commit('data', []);
        commit('model', data.model);
        commit('id', data.id);
        commit('withoutLoading', false);
        const url = beforeFetch.getPanelCommentsUrl(api.panel.comments, data.model, data.id);
        let query = {};
        if (data.locale !== undefined) {
            commit('locale', data.locale);
        }
        if (state.locale !== 'ru') {
            query.locale = state.locale;
        }
        await axiosStore.init(self.$axios).get(url, query, function (result) {
            commit('loading', false);
            commit('data', result.data);
        }, function (result) {
            commit('loading', false);
        });
    },

    /**
     *
     * @param commit
     * @param state
     * @param data
     * @returns {Promise<void>}
     */
    async fetchCommentsWithoutPending({ commit, state }, data) {
        const self = this;
        commit('withoutLoading', true);
        self.dispatch('app/panel/comments/fetchComments', data);
    },

    /**
     *
     * @param commit
     * @param state
     * @param data
     * @returns {Promise<void>}
     */
    async fetchComment({ commit, state }, data) {
        const self = this;
        const save = Object.assign({}, data);
        commit('pending', true);
        commit('created', false);
        commit('errors', {});
        const url = beforeFetch.getPanelCommentUrl(api.panel.comments, state.model, state.id);
        save.locale = 'ru';
        await axiosStore.init(self.$axios).post(url, save, function (result) {
            commit('created', true);
            commit('pending', false);
            if (result.success && result.message !== undefined) {
                self.$toast.success(result.message);
            }
        }, function (result) {
            commit('pending', false);
            if (result.errors !== undefined) {
                commit('errors', result.errors);
            }
        });
    },
    setModel({ commit }, value) {
        commit('model', value);
    },
};

const mutations = {
    locale: (state, data) => {
        state.locale = data;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    loading: (state, val) => {
        // if (state.withoutLoading && !val) {
        //     state.withoutLoading = false;
        // }
        if (!state.withoutLoading) {
            state.loading = val;
        }
    },
    withoutLoading: (state, val) => {
        state.withoutLoading = val;
    },
    model: (state, val) => {
        state.model = val;
    },
    id: (state, data) => {
        state.id = data;
    },
    data: (state, data) => {
        // if (state.withoutLoading) {
        //     state.withoutLoading = false;
        // }
        if (!state.withoutLoading) {
            state.data = data;
        }
    },
    created: (state, val) => {
        state.created = val;
    },
    errors: (state, data) => {
        state.errors = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
