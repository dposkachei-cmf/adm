/**
 * Build styles
 */
require('./index.css').toString();

/**
 * Delimiter Block for the Editor.js.
 *
 * @author CodeX (team@ifmo.su)
 * @copyright CodeX 2018
 * @license The MIT License (MIT)
 * @version 2.0.0
 */

/**
 * @typedef {Object} DelimiterData
 * @description Tool's input and output data format
 */
class Delimiter {
    /**
   * Notify core that read-only mode is supported
   * @return {boolean}
   */
    static get isReadOnlySupported() {
        return true;
    }

    /**
   * Allow Tool to have no content
   * @return {boolean}
   */
    static get contentless() {
        return true;
    }

    /**
   * Render plugin`s main Element and fill it with saved data
   *
   * @param {{data: DelimiterData, config: object, api: object}}
   *   data — previously saved data
   *   config - user config for Tool
   *   api - Editor.js API
   */
    constructor({ data, config, api }) {
        this.api = api;

        this._CSS = {
            block: this.api.styles.block,
            wrapper: 'ce-asterisks'
        };

        this._data = {};
        this._element = this.drawView();

        this.data = data;
    }

    /**
   * Create Tool's view
   * @return {HTMLElement}
   * @private
   */
    drawView() {
        const div = document.createElement('DIV');

        div.classList.add(this._CSS.wrapper, this._CSS.block);

        return div;
    }

    /**
   * Return Tool's view
   * @returns {HTMLDivElement}
   * @public
   */
    render() {
        return this._element;
    }

    /**
   * Extract Tool's data from the view
   * @param {HTMLDivElement} toolsContent - Paragraph tools rendered view
   * @returns {DelimiterData} - saved data
   * @public
   */
    save(toolsContent) {
        return {};
    }

    /**
   * Get Tool toolbox settings
   * icon - Tool icon's SVG
   * title - title to show in toolbox
   *
   * @return {{icon: string, title: string}}
   */
    static get toolbox() {
        return {
            icon: '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="asterisk" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-asterisk fa-w-16 fa-3x"><path fill="currentColor" d="M478.21 334.093L336 256l142.21-78.093c11.795-6.477 15.961-21.384 9.232-33.037l-19.48-33.741c-6.728-11.653-21.72-15.499-33.227-8.523L296 186.718l3.475-162.204C299.763 11.061 288.937 0 275.48 0h-38.96c-13.456 0-24.283 11.061-23.994 24.514L216 186.718 77.265 102.607c-11.506-6.976-26.499-3.13-33.227 8.523l-19.48 33.741c-6.728 11.653-2.562 26.56 9.233 33.037L176 256 33.79 334.093c-11.795 6.477-15.961 21.384-9.232 33.037l19.48 33.741c6.728 11.653 21.721 15.499 33.227 8.523L216 325.282l-3.475 162.204C212.237 500.939 223.064 512 236.52 512h38.961c13.456 0 24.283-11.061 23.995-24.514L296 325.282l138.735 84.111c11.506 6.976 26.499 3.13 33.227-8.523l19.48-33.741c6.728-11.653 2.563-26.559-9.232-33.036z" class=""></path></svg>',
            title: 'Asterisks'
        };
    }
}

module.exports = Delimiter;
