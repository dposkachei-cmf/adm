import Vue from 'vue';
import VueTippy, { TippyComponent } from 'vue-tippy';

Vue.use(VueTippy, {
    directive: 'tippy',
    animateFill: false,
    animation: 'shift-away',
});
//Vue.component('Tippy', TippyComponent);
