import { first, indexOf, nth } from 'lodash';

export default {
    readerScroll() {
        const self = this;

        /**
         *
         */
        const options = () => {
            return {
                container: 'body',
                duration: 500,
                easing: 'ease',
                offset: -80,
                force: true,
                cancelable: true,
                onStart: false,
                onDone: false,
                onCancel: false,
                x: false,
                y: true
            };
        };

        /**
         *
         * @param href
         * @param params
         */
        const toHref = (href, params) => {
            const parameters = Object.assign({}, options(), params);
            self.$scrollTo(href, 500, parameters);
        };

        /**
         *
         * @param id
         */
        const toArticle = (id) => {
            if (id === 0) {
                toHref('#content-title');
                return;
            }
            toHref(`#article_${id}`);
        };

        /**
         *
         * @param hash
         * @param sorted
         * @param hashes
         */
        const toHash = (hash, sorted, hashes) => {
            if (hash === '') {
                return;
            }
            let index = null;
            let articleId;
            if (hash !== null) {
                index = indexOf(hashes, hash);
            }
            if (index !== null && index !== -1) {
                articleId = nth(sorted, index);
                toArticle(articleId);
            }
        };

        /**
         *
         * @param sorted
         * @param activeId
         */
        const toNext = (sorted, activeId) => {
            let articleId;
            let index = null;
            if (activeId !== null) {
                index = indexOf(sorted, activeId);
            }
            if (index === null) {
                index = 0;
                articleId = first(sorted);
            } else {
                index = index + 1;
                articleId = nth(sorted, index);
            }
            toArticle(articleId);
        };

        /**
         *
         * @param sorted
         * @param activeId
         */
        const toPrev = (sorted, activeId) => {
            let articleId;
            let index = null;
            if (activeId !== null) {
                index = indexOf(sorted, activeId);
            }
            if (index === null || index === 0) {
                return;
            } else {
                index = index - 1;
                articleId = nth(sorted, index);
                if (articleId === undefined) {
                    index = null;
                }
            }
            toArticle(articleId);
        };

        /**
         *
         * @param activeArticle
         * @returns {{}|null}
         */
        const getActive = (activeArticle) => {
            const windowScroll = window.scrollY;
            const imageHeight = document.querySelector('.pr--reader .section-image').offsetHeight;
            let beSet = false;
            let active = {};
            document.querySelectorAll('.article-html .article-html-item').forEach(function (item) {
                //const articleScroll = item.scrollTop - 180;
                //const articleScroll = item.offsetTop - 60;
                const articleScroll = item.offsetTop + imageHeight - 60 - 10;
                if (windowScroll > articleScroll) {
                    const id = parseInt(item.id.replace(/[^0-9.]/g, ''));
                    active = {
                        id,
                        hash: `#${item.dataset.slug}`,
                        name: 'article_' + id,
                        title: item.dataset.title,
                    };
                    beSet = true;
                }
            });
            if (!beSet) {
                const item = document.querySelector('.pr--reader--body .contents');
                const articleScroll = item.offsetTop + imageHeight - 60 - 10;
                if (windowScroll > articleScroll) {
                    if (activeArticle.id !== 0) {
                        active = {
                            id: 0,
                            hash: '#content-title',
                            name: 'content-title',
                            title: 'Содержание',
                        };
                        beSet = true;
                    }
                } else {
                    if (activeArticle.id !== null) {
                        active = {
                            id: null,
                            hash: ' ',
                            name: null,
                            title: null,
                        };
                        beSet = true;
                    }
                }
            }
            if (beSet) {
                window.history.replaceState(null, null, active.hash);
                return active;
            }
            return null;
        };
        return {
            toArticle,
            toHref,
            toHash,
            toNext,
            toPrev,
            getActive,
        };
    },

    scrollBase() {
        const self = this;

        /**
         *
         */
        const options = () => {
            return {
                container: 'body',
                duration: 500,
                easing: 'ease',
                offset: 0,
                force: true,
                cancelable: true,
                onStart: false,
                onDone: false,
                onCancel: false,
                x: false,
                y: true
            };
        };

        /**
         *
         * @param href
         * @param params
         */
        const toHref = (href, params) => {
            const parameters = Object.assign({}, options(), params);
            self.$scrollTo(href, 500, parameters);
        };

        return {
            toHref
        };
    },

    scrollArticle() {
        const self = this;

        /**
         *
         */
        const options = () => {
            return {
                container: 'body',
                duration: 500,
                easing: 'ease',
                offset: -100,
                force: true,
                cancelable: true,
                onStart: false,
                onDone: false,
                onCancel: false,
                x: false,
                y: true
            };
        };

        /**
         *
         * @param href
         * @param params
         */
        const toHref = (href, params) => {
            const parameters = Object.assign({}, options(), params);
            self.$scrollTo(href, 500, parameters);
        };

        return {
            toHref
        };
    },
    scrollEditorPreview() {
        const self = this;

        /**
         *
         */
        const options = () => {
            return {
                container: '.pr--main--left.__right',
                duration: 500,
                easing: 'ease',
                offset: -300,
                force: true,
                cancelable: true,
                onStart: false,
                onDone: false,
                onCancel: false,
                x: false,
                y: true
            };
        };

        /**
         *
         * @param href
         * @param params
         */
        const toHref = (href, params) => {
            const parameters = Object.assign({}, options(), params);
            self.$scrollTo(href, 500, parameters);
        };

        return {
            toHref
        };
    },
    scrollChat() {
        const self = this;

        /**
         *
         */
        const options = () => {
            return {
                container: '.pr--chat--messages.--right',
                duration: 500,
                easing: 'ease',
                offset: 0,
                force: true,
                cancelable: true,
                onStart: false,
                onDone: false,
                onCancel: false,
                x: false,
                y: true
            };
        };

        /**
         *
         * @param href
         * @param params
         */
        const toHref = (href, params) => {
            const parameters = Object.assign({}, options(), params);
            self.$scrollTo(href, 500, parameters);
        };

        return {
            toHref
        };
    }
};
