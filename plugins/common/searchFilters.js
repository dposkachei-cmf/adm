import moment from 'moment';
import serialize from '../../plugins/serialize/index';

export default {

    /**
     *
     * @returns {string[]}
     */
    searchPublishedDateGetCurrentWeek() {
        return [
            moment().startOf('isoWeek').format('YYYY-MM-DD'),
            moment().endOf('isoWeek').format('YYYY-MM-DD'),
        ];
    },

    /**
     *
     * @returns {string[]}
     */
    searchPublishedDateGetCurrentMonth() {
        return [
            moment().startOf('month').format('YYYY-MM-DD'),
            moment().endOf('month').format('YYYY-MM-DD'),
        ];
    },

    /**
     *
     * @returns {string[]}
     */
    searchPublishedDateGetCurrentYear() {
        return [
            moment().startOf('year').format('YYYY-MM-DD'),
            moment().endOf('year').format('YYYY-MM-DD'),
        ];
    },

    /**
     *
     * @param author
     * @returns {*}
     */
    searchRouteForAuthor(author) {
        const object = {
            type: 'articles',
            author,
        };
        const string = serialize.query(object);
        return this.localePath(`/search?${string}`);
    },

    /**
     *
     * @param keyword
     * @returns {*}
     */
    searchRouteForKeyword(keyword) {
        const object = {
            type: 'articles',
            keywords: keyword,
        };
        const string = serialize.query(object);
        return this.localePath(`/search?${string}`);
    },

    /**
     *
     * @param query
     * @param options
     * @param data
     * @returns {{}}
     */
    searchPrepareQueryToForm(query, options, data) {
        const self = this;
        const form = {};

        // Q
        if (query.q !== undefined) {
            form.q = query.q;
        }
        if (form.q === '') {
            form.q = '';
            //delete form.q;
        }

        // CATEGORY
        if (query.category_id !== undefined) {
            query.category_id = parseInt(query.category_id);
            const category = data.select.categories.find(j => j.id === query.category_id);
            if (category !== undefined) {
                form.categoryObject = category;
                //form.categoryObject = { id: category.id, slug: category.slug, title: category.title };
            } else {
                form.categoryObject = { id: query.category_id };
            }
        } else {
            form.categoryObject = null;
        }

        // GROUP
        if (query.group_id !== undefined) {
            query.group_id = parseInt(query.group_id);
            const group = data.select.groups.find(j => j.id === query.group_id);
            if (group !== undefined) {
                form.groupObject = group;
                //form.groupObject = { id: group.id, slug: group.slug, title: group.title };
            } else {
                form.groupObject = { id: query.group_id };
            }
        } else {
            form.groupObject = null;
        }

        const tagsArray = [];
        // TAGS
        if (query.tags !== undefined) {
            if (typeof query.tags === 'string') {
                query.tags = [query.tags];
            }
            if (data.tags.length) {
                query.tags.forEach((value) => {
                    const tagFind = data.tags.find(j => j.id === parseInt(value));
                    tagsArray.push(tagFind);
                });
            }
            form.tagsArray = tagsArray;
        } else {
            form.tagsArray = [];
        }

        // SORT_BY
        if (query.sort_by !== undefined) {
            form.sort_by = query.sort_by;
        } else {
            form.sort_by = 'date';
        }

        // PUBLISHED
        if (query.published !== undefined) {
            form.published = query.published;
        } else {
            form.published = 'all';
        }

        if (query.active_from !== undefined && query.active_to !== undefined) {
            form.published_rg = [];
            form.published_rg.push(moment(query.active_from, 'DD.MM.YYYY').format('YYYY-MM-DD'));
            form.published_rg.push(moment(query.active_to, 'DD.MM.YYYY').format('YYYY-MM-DD'));

            const jsonPublishedRg = JSON.stringify(form.published_rg);
            const jsonWeek = JSON.stringify(self.searchPublishedDateGetCurrentWeek());
            const jsonMonth = JSON.stringify(self.searchPublishedDateGetCurrentMonth());
            const jsonYear = JSON.stringify(self.searchPublishedDateGetCurrentYear());

            if (jsonPublishedRg === jsonWeek) {
                form.published_value = 'week';
            } else if (jsonPublishedRg === jsonMonth) {
                form.published_value = 'month';
            } else if (jsonPublishedRg === jsonYear) {
                form.published_value = 'year';
            }
        }

        // PAGE
        if (query.page !== undefined) {
            //form.page = query.page;
        }
        console.log(form);
        return form;
    },
};
