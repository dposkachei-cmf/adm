import response from '../../../../plugins/common/response';
import api from './../../../../api';
import beforeFetch from "../../../../plugins/common/beforeFetch";
import axiosStore from "../../../../plugins/common/axiosStore";

const state = () => ({
    destroyed: false,
    pending: false,
    pendingArray: [],
});

const getters = {
    destroyed(state) {
        return state.destroyed;
    },
    pending(state) {
        return state.pending;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
};

const actions = {
    async fetchDestroy({ state, commit }, data) {
        const self = this;
        commit('pending', true);
        commit('pendingArray', response.pendingAdd(state.pendingArray, data.article_id, 'article'));
        const url = beforeFetch.getUrlModelIdArticleId(api.editor.articles.destroy, data.model, data.id, data.article_id);
        await axiosStore.init(self.$axios).delete(url, {}, function (result) {
            commit('pending', false);
            commit('destroyed', true);
            commit('pendingArray', response.pendingRemove(state.pendingArray, data.article_id, 'article'));
            self.dispatch('app/panel/base/fetchReloadItem');
            self.dispatch('app/table/base/fetchReload');
            if (result.success && result.message !== undefined) {
                self.$toast.success(result.message);
            }
        }, function (result) {
            commit('pending', false);
            commit('pendingArray', response.pendingRemove(state.pendingArray, data.article_id, 'article'));
            self.dispatch('app/panel/base/fetchReloadItem');
            if (result.message !== undefined) {
                self.$toast.error(result.message);
            }
        });
    },
};

const mutations = {
    destroyed: (state, val) => {
        state.destroyed = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    pendingArray: (state, val) => {
        state.pendingArray = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
