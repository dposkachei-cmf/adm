import response from '../../../plugins/common/response';
import api from './../../../api';
import axiosStore from "../../../plugins/common/axiosStore";

const state = () => ({
    pending: false,
    data: {},
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
};

// actions
const actions = {
    async fetchCounters({ commit }, data) {
        const self = this;
        commit('pending', true);
        const url = api.comments.counters.replace(':id', data.id);
        await axiosStore.init(self.$axios).get(url, {}, function (result) {
            commit('data', result.data);
        }, function () {
            commit('pending', false);
        });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
