import response from '../../../../../plugins/common/response';
import serialize from '../../../../../plugins/common/serialize';
import api from '../../../../../api';

const state = () => ({
    loaded: false,
    pending: false,
    withPending: true,
    data: [],
    pagination: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    loaded(state) {
        return state.loaded;
    },
    pending(state) {
        return state.pending;
    },
    withPending(state) {
        return state.withPending;
    },
    pagination(state) {
        return state.pagination;
    },
};

// actions
const actions = {
    async fetchData({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        const url = api.user.chats.messages.index.replace(':id', data.id);
        self.dispatch('user/chats/setReadChatMessages', {
            id: data.id,
        });
        const string = serialize.query(data);
        await self.$axios.get(url + '?' + string)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                if (!state.loaded) {
                    commit('loaded', true);
                }
                const result = response.success(res);
                commit('data', result.data);
                if (result.pagination !== undefined) {
                    commit('pagination', result.pagination);
                }
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                }
            });
    },
    async fetchDataWithoutPending({ commit, state }, data) {
        const self = this;
        commit('withPending', false);
        await self.dispatch('user/chats/messages/fetchData', data);
    },
    async fetchMessagesInitial({ commit, state }, data) {
        const self = this;
        commit('data', []);
        commit('loaded', false);
        await self.dispatch('user/chats/messages/fetchData', data);
    },
    setEmpty({ commit }, data) {
        commit('data', []);
    },
    appendMessage({ commit }, data) {
        commit('appendData', data);
    }
};

// mutations
const mutations = {
    pending: (state, val) => {
        if (state.withPending) {
            state.pending = val;
        }
        if (!val) {
            state.pending = val;
            state.withPending = true;
        }
    },
    loaded: (state, val) => {
        state.loaded = val;
    },
    data: (state, data) => {
        if (data.length === 0) {
            state.data = [];
        } else {
            data.forEach(function (value) {
                state.data.push(value);
            });
        }
    },
    withPending: (state, val) => {
        state.withPending = val;
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
    appendData: (state, data) => {
        state.data.unshift(data);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
