import api from "../../../../../api";
import beforeFetch from "../../../../../plugins/common/beforeFetch";
import axiosStore from "../../../../../plugins/common/axiosStore";

const state = () => ({
    model: null,
    item: null,

    success: false,
    pending: false,
    errors: {},
    closeModal: false,
});

const getters = {
    model(state) {
        return state.model;
    },
    item(state) {
        return state.item;
    },
    success(state) {
        return state.success;
    },
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
    closeModal(state) {
        return state.closeModal;
    },
};

const actions = {
    async fetchUpload({ commit, state }, data) {
        const self = this;
        commit('success', false);
        commit('pending', true);
        commit('errors', {});
        const url = beforeFetch.getUrlModelId(api.panel.upload.image, state.model, state.item.id);
        await axiosStore.init(self.$axios).post(url, data, function () {
            commit('pending', false);
            commit('success', true);
            self.dispatch('app/panel/panel/base/fetchReloadItem');
            if (state.model === 'users') {
                self.dispatch('user/base/fetchUser');
            }
        }, function (result) {
            commit('pending', false);
            if (result.errors !== undefined) {
                commit('errors', result.errors);
            }
        });
    },
    async fetchUploadAndUpdate({ commit, state }, data) {
        const self = this;
        commit('success', false);
        commit('pending', true);
        commit('errors', {});
        commit('closeModal', false);
        const url = beforeFetch.getUrlModelId(api.panel.upload.image, state.model, state.item.id);
        await axiosStore.init(self.$axios).post(url, data, function () {
            commit('pending', false);
            commit('success', true);
            self.dispatch('app/panel/panel/base/fetchReloadItem');
            commit('closeModal', true);
            if (state.model === 'users') {
                self.dispatch('user/base/fetchUser');
            }
        }, function (result) {
            commit('pending', false);
            if (result.errors !== undefined) {
                commit('errors', result.errors);
            }
        });
    },
    async setModel({ commit }, data) {
        await commit('model', data.model);
        await commit('item', data.item);
    }
};

const mutations = {
    model: (state, val) => {
        state.model = val;
    },
    item: (state, val) => {
        state.item = val;
    },
    success: (state, val) => {
        state.success = val;
        setTimeout(function () {
            state.success = false;
        }, 1500);
    },
    pending: (state, val) => {
        state.pending = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
    closeModal: (state, val) => {
        state.closeModal = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
