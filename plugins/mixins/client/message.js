export default {
    message() {
        const self = this;
        const needLoginForSearches = () => {
            self.$toast.error(self.$t('message.need-login.searches'), {
                duration: 3000,
            });
        };

        const citationSuccess = () => {
            self.$toast.success(self.$t('message.citation.success'), {
                duration: 3000,
            });
        };

        const needLoginForFavorites = () => {
            self.$toast.error(self.$t('message.need-login.favorites'), {
                duration: 3000,
            });
        };

        const needLoginForWatches = () => {
            self.$toast.error(self.$t('message.need-login.watches'), {
                duration: 3000,
            });
        };

        const needLoginForReports = () => {
            self.$toast.error(self.$t('message.need-login.reports'), {
                duration: 3000,
            });
        };

        const needLoginForAddBasket = () => {
            self.$toast.error(self.$t('message.need-login.basket'), {
                duration: 3000,
            });
        };

        const readerNeedAccess = () => {
            self.$toast.error(self.$t('message.need-access.reader'), {
                duration: 300000,
            });
        };
        const needLoginForRating = () => {
            self.$toast.error(self.$t('message.need-login.rating'), {
                duration: 3000,
            });
        };
        const needLoginForComment = () => {
            self.$toast.error(self.$t('message.need-login.comment'), {
                duration: 3000,
            });
        };
        const needLoginForRateArticle = () => {
            self.$toast.error(self.$t('message.need-login.rate-article'), {
                duration: 3000,
            });
        };
        const needLoginForUsersLock = () => {
            self.$toast.error(self.$t('message.need-login.users-lock'), {
                duration: 3000,
            });
        };
        const needLoginForRateComment = () => {
            self.$toast.error(self.$t('message.need-login.rate-comment'), {
                duration: 3000,
            });
        };
        const needLoginForSubscription = () => {
            self.$toast.error(self.$t('message.need-login.subscription'), {
                duration: 3000,
            });
        };
        const needLoginForChat = () => {
            self.$toast.error(self.$t('message.need-login.chat'), {
                duration: 3000,
            });
        };

        return {
            needLoginForSearches,
            needLoginForFavorites,
            needLoginForAddBasket,
            readerNeedAccess,
            citationSuccess,
            needLoginForRating,
            needLoginForRateArticle,
            needLoginForRateComment,
            needLoginForComment,
            needLoginForSubscription,
            needLoginForChat,
            needLoginForWatches,
            needLoginForReports,
            needLoginForUsersLock,
        };
    },
};
