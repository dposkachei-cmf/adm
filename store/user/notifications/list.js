import response from '../../../plugins/common/response';
import api from './../../../api';
import axios from "../../../plugins/axios";
import axiosStore from "../../../plugins/common/axiosStore";

// initial state
const state = () => ({
    pending: false,
    withPending: true,
    data: [],
    pagination: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    withPending(state) {
        return state.withPending;
    },
    pagination(state) {
        return state.pagination;
    },
};

// actions
const actions = {
    async fetchNotifications({ commit }) {
        const self = this;
        commit('pending', true);
        await axiosStore.init(self.$axios).get(api.user.notifications.index, {}, function (result) {
            commit('pending', false);
            commit('data', result.data);
            if (result.pagination !== undefined) {
                commit('pagination', result.pagination);
            }
        }, function () {
            commit('pending', false);
        });
    },
    async fetchNotificationsWithoutPending({ commit, state }) {
        const self = this;
        commit('withPending', false);
        self.dispatch('user/notifications/list/fetchNotifications')
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        if (state.withPending) {
            state.pending = val;
        }
        if (!val) {
            state.pending = val;
            state.withPending = true;
        }
    },
    data: (state, data) => {
        state.data = data;
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
    withPending: (state, val) => {
        state.withPending = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
