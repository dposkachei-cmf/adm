const path = require('path');

module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        query: {
                            presets: [ '@babel/preset-env' ],
                        },
                    },
                ]
            },
            {
                test: /\.png$/,
                exclude: "/node_modules/",
                use: ['file-loader'],
            },
            {
                test: /\.scss$/,
                include: path.resolve(__dirname, '../'),
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                    //'url-loader',
                    // {
                    //     loader: "sass-loader",
                    //     options: {
                    //         sourceMap: true,
                    //     },
                    // },
                    // {
                    //     loader: 'postcss-loader',
                    //     options: {
                    //         plugins: [
                    //             require('postcss-nested-ancestors'),
                    //             require('postcss-nested')
                    //         ]
                    //     }
                    // }
                ]
            },
            {
                test: /\.svg$/,
                loader: 'svg-inline-loader?removeSVGTagAttrs=false'
            }
        ]
    },
    output: {
        path: path.join(__dirname, '/dist'),
        publicPath: '/',
        filename: 'bundle.js',
        library: 'ImageTool',
        libraryTarget: 'umd',
        libraryExport: 'default'
    }
};
