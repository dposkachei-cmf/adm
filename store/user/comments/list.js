import response from '../../../plugins/common/response';
import api from './../../../api';
import axiosStore from "../../../plugins/common/axiosStore";

// initial state
const state = () => ({
    withPending: true,
    pending: true,
    data: {},
    pagination: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    withPending(state) {
        return state.withPending;
    },
    pending(state) {
        return state.pending;
    },
    pagination(state) {
        return state.pagination;
    },
};

// actions
const actions = {
    async fetchComments({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        await axiosStore.init(self.$axios).get(api.user.comments.index, function (result) {
            commit('pending', false);
            if (!state.withPending) {
                commit('withPending', true);
            }
            commit('data', result.data);
            commit('pagination', null);
        }, function (result) {
            //
        });
    },
    async fetchCommentsWithoutPending({ commit }, data) {
        const self = this;
        commit('withPending', false);
        await self.dispatch('user/comments/list/fetchComments', data);
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        if (state.withPending) {
            state.pending = val;
        }
    },
    data: (state, data) => {
        state.data = data;
    },
    withPending: (state, data) => {
        state.withPending = data;
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
