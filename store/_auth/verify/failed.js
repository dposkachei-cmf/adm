import response from '../../../plugins/common/response';
import api from '../../../api';

const state = () => ({
    pending: false,
    errors: {},
});

const getters = {
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
};

const actions = {
    async fetchFailed({ commit }, data) {
        const self = this;
        commit('pending', true);
        commit('errors', {});
        await self.$axios.post(api.auth.verifyFailed, data)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                if (result.success && result.message !== undefined) {
                    self.$toast.success(result.message);
                }
                setTimeout(function () {
                    self.$router.push('/');
                }, 1000);
                self.$auth.fetchUser().then(function () {});
                //self.$router.push('/');
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.message !== undefined && result.status_code !== 422) {
                        self.$toast.error(result.message);
                    }
                }
            });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    errors: (state, data) => {
        state.errors = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
