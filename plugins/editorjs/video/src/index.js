import './index.css';
import Uploader from './uploader';
import Icon from './svg/toolbox.svg';
import FileIcon from './svg/standard.svg';
import CustomFileIcon from './svg/custom.svg';
import DownloadIcon from './svg/arrow-download.svg';
const LOADER_TIMEOUT = 500;
import SERVICES from './services';

/**
 * @typedef {object} AttachesToolData
 * @description Attaches Tool's output data format
 * @property {AttachesFileData} file - object containing information about the file
 * @property {string} title - file's title
 */

/**
 * @typedef {object} AttachesFileData
 * @description Attaches Tool's file format
 * @property {string} [url] - file's upload url
 * @property {string} [size] - file's size
 * @property {string} [extension] - file's extension
 * @property {string} [name] - file's name
 */

/**
 * @typedef {object} FileData
 * @description Attaches Tool's response from backend
 * @property {string} url - file's url
 * @property {string} name - file's name with extension
 * @property {string} extension - file's extension
 */

/**
 * @typedef {object} UploadResponseFormat
 * @description This format expected from backend on file upload
 * @property {number} success  - 1 for successful uploading, 0 for failure
 * @property {FileData} file - backend response with uploaded file data.
 */

/**
 * @typedef {object} AttachesToolConfig
 * @description Config supported by Tool
 * @property {string} endpoint - file upload url
 * @property {string} field - field name for uploaded file
 * @property {string} types - available mime-types
 * @property {string} placeholder
 * @property {string} errorMessage
 */

/**
 * @class Video
 * @classdesc AttachesTool for Editor.js 2.0
 *
 * @property {API} api - Editor.js API
 * @property {AttachesToolData} data
 * @property {AttachesToolConfig} config
 */
export default class Video {
    /**
   * @param {AttachesToolData} data
   * @param {Object} config
   * @param {API} api
   */
    constructor({ data, config, api }) {
        this.api = api;

        this.nodes = {
            wrapper: null,
            button: null,
            title: null,
            input: null,
            iframe: null,
            file: null,
        };

        this._data = {
            file: null,
            embed: null,
            title: null,
        };

        this.config = {
            endpoint: config.endpoint || '',
            field: config.field || 'file',
            types: config.types || 'video/*',
            buttonText: config.buttonText || 'Select file to upload',
            errorMessage: config.errorMessage || 'File upload failed',
            additionalRequestHeaders: config.additionalRequestHeaders || {}
        };

        this.data = data;

        /**
     * Module for files uploading
     */
        this.uploader = new Uploader({
            config: this.config,
            onUpload: (response) => {
                this.onUpload(response);
                config.onUpload();
            },
            onError: error => this.uploadingFailed(error)
        });

        this.enableFileUpload = this.enableFileUpload.bind(this);
    }

    /**
   * Get Tool toolbox settings
   * icon - Tool icon's SVG
   * title - title to show in toolbox
   */
    static get toolbox() {
        return {
            icon: Icon,
            title: 'Video'
        };
    }

    /**
   * Tool's CSS classes
   */
    get CSS() {
        return {
            baseClass: this.api.styles.block,
            apiButton: this.api.styles.button,
            loader: this.api.styles.loader,
            /**
       * Tool's classes
       */
            wrapper: 'cdx-video',
            wrapperWithFile: 'cdx-video--with-file',
            wrapperWithUrl: 'cdx-video--with-url',
            wrapperLoading: 'cdx-video--loading',
            file: 'cdx-video__file',
            iframe: 'cdx-video__iframe',
            button: 'cdx-video__button',
            title: 'cdx-video__title',
            size: 'cdx-video__size',
            downloadButton: 'cdx-video__download-button',
            fileInfo: 'cdx-video__file-info',
            fileIcon: 'cdx-video__file-icon',
            input: 'cdx-input',
            inputEl: 'cdx-video__input',
            inputHolder: 'cdx-video__input-holder',
            inputError: 'cdx-video__input-holder--error',
        };
    }

    /**
   * Possible files' extension colors
   */
    get EXTENSIONS() {
        return {
            doc: '#3e74da',
            docx: '#3e74da',
            odt: '#3e74da',
            pdf: '#d47373',
            rtf: '#656ecd',
            tex: '#5a5a5b',
            txt: '#5a5a5b',
            pptx: '#e07066',
            ppt: '#e07066',
            mp3: '#eab456',
            mp4: '#f676a6',
            xls: '#3f9e64',
            html: '#2988f0',
            htm: '#2988f0',
            png: '#f676a6',
            jpg: '#f67676',
            jpeg: '#f67676',
            gif: '#f6af76',
            zip: '#4f566f',
            rar: '#4f566f',
            exe: '#e26f6f',
            svg: '#bf5252',
            key: '#e07066',
            sketch: '#df821c',
            ai: '#df821c',
            psd: '#388ae5',
            dmg: '#e26f6f',
            json: '#2988f0',
            csv: '#3f9e64'
        };
    }

    /**
   * Return Block data
   * @param {HTMLElement} toolsContent
   * @return {AttachesToolData}
   */
    save(toolsContent) {
    /**
     * If file was uploaded
     */
        if (this.pluginHasFile()) {
            //const title = toolsContent.querySelector(`.${this.CSS.title}`).innerHTML;

            //Object.assign(this.data, { title });
        }

        return this.data;
    }

    /**
   * Renders Block content
   * @return {HTMLDivElement}
   */
    render() {
        const holder = this.make('div', this.CSS.baseClass);

        this.nodes.wrapper = this.make('div', this.CSS.wrapper);
        this.nodes.file = this.make('div', this.CSS.file);
        this.nodes.iframe = this.make('div', this.CSS.iframe);

        if (this.pluginHasEmbed()) {
            this.createIframe(this.data.embed.source);
        } else {
            this.makeInputHolder();
        }

        if (this.pluginHasFile()) {
            this.showFileData();
        } else {
            this.prepareUploadButton();
        }
        this.nodes.wrapper.appendChild(this.nodes.file);
        this.nodes.wrapper.appendChild(this.nodes.iframe);

        holder.appendChild(this.nodes.wrapper);

        return holder;
    }

    /**
     * Prepare input holder
     *
     */
    makeInputHolder() {
        this.nodes.input = this.make('div', [this.CSS.input, this.CSS.inputEl], {
            contentEditable: true,
        });

        this.nodes.input.dataset.placeholder = this.api.i18n.t('Или ссылка на видео YouTube');
        //this.nodes.input.dataset.placeholder = '';

        this.nodes.input.addEventListener('paste', (event) => {
            this.startFetching(event);
        });

        this.nodes.input.addEventListener('keydown', (event) => {
            const [ENTER, A] = [13, 65];
            const cmdPressed = event.ctrlKey || event.metaKey;

            switch (event.keyCode) {
            case ENTER:
                event.preventDefault();
                event.stopPropagation();

                this.startFetching(event);
                break;
            case A:
                if (cmdPressed) {
                    this.selectLinkUrl(event);
                }
                break;
            }
        });

        this.nodes.iframe.appendChild(this.nodes.input);
    }

    /**
     * Select LinkTool input content by CMD+A
     *
     * @param {KeyboardEvent} event
     */
    selectLinkUrl(event) {
        event.preventDefault();
        event.stopPropagation();

        const selection = window.getSelection();
        const range = new Range();

        const currentNode = selection.anchorNode.parentNode;
        const currentItem = currentNode.closest(`.${this.CSS.inputHolder}`);
        const inputElement = currentItem.querySelector(`.${this.CSS.inputEl}`);

        range.selectNodeContents(inputElement);

        selection.removeAllRanges();
        selection.addRange(range);
    }

    /**
     * Activates link data fetching by url
     *
     * @param {PasteEvent} event
     */
    startFetching(event) {
        let url = this.nodes.input.textContent;

        if (event.type === 'paste') {
            url = (event.clipboardData || window.clipboardData).getData('text');
        }
        if (this.pluginHasFile()) {
            this.nodes.file.innerHTML = '';
            this.nodes.wrapper.classList.remove(this.CSS.wrapperWithFile);
            this.prepareUploadButton();
        }
        this.createIframe(url);
        //this.removeErrorStyle();
        //this.fetchLinkData(url);
        //this.showProgress();
        //this.nodes.wrapper.textContent = url;
    }

    /**
     *
     * Create iframe for YouTube embed
     * @private
     * @param {string} url
     *
     */
    createIframe(url) {
        const { regex, embedUrl, width, height, id = ids => ids.shift() } = SERVICES.youtube;
        const result = regex.exec(url).slice(1);
        const embed = embedUrl.replace(/<%= remote_id %>/g, id(result));
        const videoId = id(result);
        if (videoId !== null) {
            const iframe = document.createElement('iframe');
            iframe.setAttribute('src', `https://www.youtube.com/embed/${videoId}`);
            iframe.setAttribute('allowfullscreen', true);
            iframe.classList.add(this.CSS.iframe + '--content');
            this.nodes.iframe.innerHTML = '';
            this.nodes.iframe.appendChild(iframe);
            this.data = {
                file: null,
                embed: {
                    service: 'youtube',
                    source: url,
                    embed,
                    width,
                    height,
                    caption: '',
                },
                title: '',
            };
            this.nodes.wrapper.classList.add(this.CSS.wrapperWithUrl);
        } else {
            this.api.notifier.show({
                message: 'Неверная ссылка',
                style: 'error'
            });
        }
    }

    /**
   * Prepares button for file uploading
   */
    prepareUploadButton() {
        this.nodes.button = this.make('div', [this.CSS.apiButton, this.CSS.button]);
        this.nodes.button.innerHTML = `${Icon} ${this.config.buttonText}`;
        this.nodes.button.addEventListener('click', this.enableFileUpload);
        this.nodes.file.appendChild(this.nodes.button);
    }

    /**
   * Fires after clicks on the Toolbox AttachesTool Icon
   * Initiates click on the Select File button
   * @public
   */
    appendCallback() {
        //this.nodes.button.click();
    }

    /**
   * Checks if any of Tool's fields have data
   * @return {boolean}
   */
    pluginHasFile() {
        return this.data.file !== null;
    }

    /**
    * Checks if any of Tool's fields have data
    * @return {boolean}
   */
    pluginHasEmbed() {
        return this.data.embed !== null;
    }

    /**
   * Allow to upload files on button click
   */
    enableFileUpload() {
        this.uploader.uploadSelectedFile({
            onPreview: () => {
                this.nodes.wrapper.classList.add(this.CSS.wrapperLoading, this.CSS.loader);
            }
        });
    }

    /**
   * File uploading callback
   * @param {UploadResponseFormat} response
   */
    onUpload(response) {
        const body = response.body;

        if (body.success && body.file) {
            const { url, name, size } = body.file;
            if (this.pluginHasEmbed()) {
                this.nodes.iframe.querySelector('.cdx-video__iframe--content').remove();
                this.nodes.wrapper.classList.remove(this.CSS.wrapperWithUrl);
                this.makeInputHolder();
            }
            this.data = {
                file: {
                    url,
                    extension: name.split('.').pop(),
                    name,
                    size,
                },
                embed: null,
                title: name
            };

            this.nodes.button.remove();
            this.showFileData();
            this.moveCaretToEnd(this.nodes.title);
            this.nodes.title.focus();
            this.removeLoader();
        } else {
            this.uploadingFailed(this.config.errorMessage);
        }
    }

    /**
   * Handles uploaded file's extension and appends corresponding icon
   */
    appendFileIcon() {
        const extension = this.data.file.extension || '';
        const extensionColor = this.EXTENSIONS[extension];

        const fileIcon = this.make('div', this.CSS.fileIcon, {
            innerHTML: extensionColor ? CustomFileIcon : FileIcon
        });

        if (extensionColor) {
            fileIcon.style.color = extensionColor;
            fileIcon.setAttribute('data-extension', extension);
        }

        this.nodes.file.appendChild(fileIcon);
    }

    /**
   * Removes tool's loader
   */
    removeLoader() {
        setTimeout(() => this.nodes.wrapper.classList.remove(this.CSS.wrapperLoading, this.CSS.loader), LOADER_TIMEOUT);
    }

    /**
   * If upload is successful, show info about the file
   */
    showFileData() {
        this.nodes.wrapper.classList.add(this.CSS.wrapperWithFile);

        const { file: { size, url }, title } = this.data;

        this.appendFileIcon();

        const fileInfo = this.make('div', this.CSS.fileInfo);

        if (title) {
            this.nodes.title = this.make('div', this.CSS.title, {
                contentEditable: true
            });

            this.nodes.title.textContent = title;
            fileInfo.appendChild(this.nodes.title);
        }

        if (size) {
            let sizePrefix;
            let formattedSize;
            const fileSize = this.make('div', this.CSS.size);

            if (Math.log10(+size) >= 6) {
                sizePrefix = 'Мб';
                formattedSize = size / Math.pow(2, 20);
            } else {
                sizePrefix = 'Кб';
                formattedSize = size / Math.pow(2, 10);
            }

            fileSize.textContent = formattedSize.toFixed(1);
            fileSize.setAttribute('data-size', sizePrefix);
            fileInfo.appendChild(fileSize);
        }

        this.nodes.file.appendChild(fileInfo);

        const downloadIcon = this.make('a', this.CSS.downloadButton, {
            innerHTML: DownloadIcon,
            href: url,
            target: '_blank',
            rel: 'nofollow noindex noreferrer'
        });

        this.nodes.file.appendChild(downloadIcon);
    }

    /**
   * If file uploading failed, remove loader and show notification
   * @param {string} errorMessage -  error message
   */
    uploadingFailed(errorMessage) {
        this.api.notifier.show({
            message: errorMessage,
            style: 'error'
        });

        this.removeLoader();
    }

    /**
   * Return Attaches Tool's data
   * @return {AttachesToolData}
   */
    get data() {
        return this._data;
    }

    /**
   * Stores all Tool's data
   * @param {AttachesToolData} data
   */
    set data({ file, title, embed }) {
        const embedObject = embed !== undefined && embed !== null
            ? {
                service: 'youtube',
                source: embed.source,
                embed: embed.embed,
                width: embed.width,
                height: embed.height,
                caption: embed.caption,
            }
            : null;
        const fileObject = file !== undefined && file !== null
            ? {
                url: file.url,
                name: file.name,
                extension: file.extension,
                size: file.size
            }
            : null;
        this._data = Object.assign({}, {
            file: fileObject,
            embed: embedObject,
            title: title || null,
        });
    }

    /**
   * Moves caret to the end of contentEditable element
   * @param {HTMLElement} element - contentEditable element
   */
    moveCaretToEnd(element) {
        const range = document.createRange();
        const selection = window.getSelection();

        range.selectNodeContents(element);
        range.collapse(false);
        selection.removeAllRanges();
        selection.addRange(range);
    }

    /**
   * Helper method for elements creation
   * @param tagName
   * @param classNames
   * @param attributes
   * @return {HTMLElement}
   */
    make(tagName, classNames = null, attributes = {}) {
        const el = document.createElement(tagName);

        if (Array.isArray(classNames)) {
            el.classList.add(...classNames);
        } else if (classNames) {
            el.classList.add(classNames);
        }

        for (const attrName in attributes) {
            el[attrName] = attributes[attrName];
        }

        return el;
    }
}
