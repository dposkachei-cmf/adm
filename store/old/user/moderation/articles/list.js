import response from '../../../../../plugins/common/response';
import api from '../../../../../api';

// initial state
const state = () => ({
    pending: true,
    hasPending: true,
    data: [],
    pagination: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    hasPending(state) {
        return state.hasPending;
    },
    pagination(state) {
        return state.pagination;
    },
};

// actions
const actions = {
    async fetchArticles({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        await self.$axios.get(api.user.moderation.articles.index)
            .then((res) => {
                commit('pending', false);
                if (!state.hasPending) {
                    commit('hasPending', true);
                }
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                if (result.pagination !== undefined) {
                    commit('pagination', result.pagination);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
    async fetchWaiting({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        await self.$axios.get(api.user.moderation.articles.index + '?waiting=1')
            .then((res) => {
                commit('pending', false);
                if (!state.hasPending) {
                    commit('hasPending', true);
                }
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                if (result.pagination !== undefined) {
                    commit('pagination', result.pagination);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
    async fetchArticlesWithoutPending({ commit }, data) {
        const self = this;
        commit('hasPending', false);
        await self.dispatch('user/moderation/articles/list/fetchArticles', data);
    },
    async fetchWaitingWithoutPending({ commit }, data) {
        const self = this;
        commit('hasPending', false);
        await self.dispatch('user/moderation/articles/list/fetchWaiting', data);
    },
    async fetchArticlesInitial({ commit }, data) {
        const self = this;
        commit('data', []);
        await self.dispatch('user/moderation/articles/list/fetchArticles', data);
    },
    async fetchWaitingInitial({ commit }, data) {
        const self = this;
        commit('data', []);
        await self.dispatch('user/moderation/articles/list/fetchWaiting', data);
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        if (state.hasPending) {
            state.pending = val;
        }
    },
    hasPending: (state, val) => {
        state.hasPending = val;
    },
    data: (state, data) => {
        state.data = data;
        // if (data.length === 0) {
        //     state.data = [];
        // } else {
        //     data.forEach(function (value) {
        //         state.data.push(value);
        //     });
        // }
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
