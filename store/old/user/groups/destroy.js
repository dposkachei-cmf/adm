import response from '../../../../plugins/common/response';
import api from '../../../../api';

const state = () => ({
    destroyed: false,
    pending: false,
    pendingArray: [],
});

const getters = {
    destroyed(state) {
        return state.destroyed;
    },
    pending(state) {
        return state.pending;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
};

const actions = {
    async fetchDestroy({ state, commit }, data) {
        const self = this;
        commit('pending', true);
        commit('pendingArray', response.pendingAdd(state.pendingArray, data.id, 'group'));
        const url = api.user.groups.destroy.replace(':id', data.id);
        await self.$axios.delete(url)
            .then((res) => {
                commit('pending', false);
                commit('destroyed', true);
                commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, 'group'));
                self.$auth.fetchUser().then(function () {});
                self.dispatch('user/groups/list/fetchGroupsWithoutPending', data);
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                    commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, 'group'));
                }
            });
    },
};

const mutations = {
    destroyed: (state, val) => {
        state.destroyed = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    pendingArray: (state, val) => {
        state.pendingArray = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
