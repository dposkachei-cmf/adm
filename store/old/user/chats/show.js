import response from '../../../../plugins/common/response';
import api from '../../../../api';

const state = () => ({
    pending: false,
    data: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
};

// actions
const actions = {
    async fetchData({ commit }, data) {
        const self = this;
        commit('pending', true);
        const url = api.user.chats.show.replace(':id', data.id);
        await self.$axios.get(url)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                }
            });
    },
    setChat({ commit }, data) {
        const self = this;
        self.dispatch('user/chats/messages/setEmpty');
        commit('data', data);
    }
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
