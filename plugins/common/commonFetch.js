export default {

    /**
     *
     * @param store
     * @param params
     */
    async userShow(store, params) {
        const user = store.getters['users/show/data'];
        const aValues = params.slug.split('-');
        const id = aValues[0];
        if (user.id !== parseInt(id)) {
            await store.dispatch('users/show/fetchData', {
                id,
            });
        }
    },
};
