import Vue from 'vue';
import routes from './mixins/ssr/routes';
import markdown from './mixins/ssr/markdown';
import momentDate from './mixins/ssr/momentDate';
import cookieActions from './mixins/ssr/cookieActions';
import stringActions from './mixins/ssr/stringActions';
import loading from './mixins/ssr/loading';

Vue.mixin({
    methods: loading,
});

Vue.mixin({
    methods: routes,
});

Vue.mixin({
    methods: markdown,
});

Vue.mixin({
    methods: momentDate,
});

Vue.mixin({
    methods: cookieActions,
});

Vue.mixin({
    methods: stringActions,
});
