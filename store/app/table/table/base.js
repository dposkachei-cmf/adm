import api from "../../../../api";
import axios from "./../../../../plugins/axios";
import response from "../../../../plugins/common/response";
import serialize from "../../../../plugins/common/serialize";
import beforeFetch from "../../../../plugins/common/beforeFetch";
import axiosStore from "../../../../plugins/common/axiosStore";

const state = () => ({
    locale: 'ru',
    pending: false,
    loaded: false,
    withPending: true,
    model: null, // string
    data: null, // array of objects table items
    request: {}, // search request object
    errors: {},
    fields: null, // object
});

const getters = {
    locale(state) {
        return state.locale;
    },
    model(state) {
        return state.model;
    },
    data(state) {
        return state.data;
    },
    request(state) {
        return state.request;
    },
    loaded(state) {
        return state.loaded;
    },
    fields(state) {
        return state.fields;
    },
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
};

const actions = {
    async fetchList({ commit, state }, data) {
        const self = this;
        if (data.model !== state.model) {
            commit('loaded', false);
            commit('model', data.model);
            commit('fields', null);
        }
        if (data.locale !== undefined) {
            commit('locale', data.locale);
        }
        commit('pending', true);
        let request = {};
        if (data.request !== undefined) {
            request = beforeFetch.getTableListRequest(data.request, data, state.locale);
        } else {
            request = beforeFetch.getTableListRequest(state.request, data, state.locale);
        }
        commit('request', request);
        const url = beforeFetch.getTableListUrl(api.table.list, state.model);
        const query = state.request;
        await axiosStore.init(self.$axios).get(url, query, function (result) {
            commit('pending', false);
            if (!state.loaded) {
                commit('loaded', true);
            }
            commit('data', result.data);
            commit('fields', result.fields);
        }, function () {
            commit('pending', false);
            // if (error.response.status === 403) {
            //     self.$toast.error('Доступ запрещен');
            // }
        });
    },
    fetchListInitial({ commit, state }, data) {
        const self = this;
        commit('loaded', false);
        self.dispatch('app/table/table/base/fetchList', data)
    },
    fetchListWithoutPending({ commit, state }, data) {
        const self = this;
        commit('withPending', false);
        self.dispatch('app/table/base/fetchList', data)
    },
    async fetchDelete({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        const url = beforeFetch.getTableDeleteUrl(api.table.destroy, state.model, data.id);
        await axiosStore.init(self.$axios).delete(url, {},function (result) {
            commit('pending', false);
        }, function () {
            commit('pending', false);
        });
    },
    async setRequest({ commit }, data) {
        await commit('request', data);
    },
    async setLocale({ commit, state }, value) {
        const self = this;
        await commit('locale', value);
        self.dispatch('app/table/table/base/fetchList', {
            model: state.model,
        });
    },
    async fetchReload({ commit, state }, data) {
        const self = this;
        await self.dispatch('app/table/table/base/fetchListWithoutPending', {
            model: state.model,
            request: state.request,
        });
    },
};

const mutations = {
    locale: (state, val) => {
        state.locale = val;
    },
    pending: (state, val) => {
        if (state.withPending) {
            state.pending = val;
        }
        if (!val) {
            state.pending = val;
            state.withPending = true;
        }
    },
    data: (state, data) => {
        state.data = data;
    },
    request: (state, data) => {
        state.request = serialize.requestNormalize(data);
    },
    fields: (state, data) => {
        state.fields = data;
    },
    loaded: (state, data) => {
        state.loaded = data;
    },
    errors: (state, data) => {
        state.errors = data;
    },
    model: (state, val) => {
        state.model = val;
    },
    withPending: (state, val) => {
        state.withPending = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
