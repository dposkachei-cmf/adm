const state = () => ({
    needSave: false,
    data: null,
});

const getters = {
    needSave(state) {
        return state.needSave;
    },
    data(state) {
        return state.data;
    },
};

const actions = {
    setNeedSave({ commit }, value) {
        commit('needSave', value);
    },
    setData({ commit }, data) {
        commit('data', data);
    }
};

const mutations = {
    needSave: (state, val) => {
        state.needSave = val;
    },
    data: (state, val) => {
        state.data = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
