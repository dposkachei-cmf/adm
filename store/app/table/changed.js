import api from "../../../api";
import axiosStore from "../../../plugins/common/axiosStore";
import beforeFetch from "../../../plugins/common/beforeFetch";

const state = () => ({
    model: null, // string
});

const getters = {
    model(state) {
        return state.model;
    },
};

const actions = {
    setModel({ commit }, val) {
        commit('model', val)
    }
};

const mutations = {
    model: (state, val) => {
        state.model = val;
        setTimeout(function () {
            state.model = null;
        }, 500);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
