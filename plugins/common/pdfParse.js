import capitalize from 'lodash/capitalize';

export default {
    data: {
        /**
         * Высота межстрочного индекса, максимальная
         * Если новая высота будет меньше разницы между новой высоты и старой, то не будет считать новую колонку
         * И отнесет к старой высоте строки
         */
        hIndex: 2,

        /**
         * Максимальная высота между одной строкой и другой, если больше, то новая строка
         */
        h: 20,

        /**
         * Отступ абзаца минимальное значение
         */
        lMin: 18,

        /**
         * Отступ абзаца максимальное значение
         */
        lMax: 21,

        /**
         * Минимальное значение тэгов, для сортировки
         */
        nodesLengthMin: 10,

        /**
         * Соединить колонки
         */
        mergeColumns: true,

        /**
         * Удалить последний символ переноса строки "-"
         */
        deleteLastSymbolNewLine: true,
    },

    /**
     *
     * @param selection
     * @param {string} id
     * @returns {HTMLElement}
     */
    createNodeBySelection(selection, id) {
        let tpl = document.getElementById(id);
        if (tpl === undefined || tpl === null) {
            tpl = document.createElement('div');
            tpl.setAttribute('id', id);
            tpl.setAttribute('class', 'd-none');
            document.body.append(tpl);
        }
        tpl.innerHTML = '';
        for (let i = 0; i < selection.rangeCount; i++) {
            tpl.append(selection.getRangeAt(i).cloneContents());
        }
        return tpl;
    },

    /**
     *
     * @param array
     * @param {string} id
     * @returns {HTMLElement}
     */
    createNodeBySorted(array, id) {
        let tpl = document.getElementById(id);
        if (tpl === undefined || tpl === null) {
            tpl = document.createElement('div');
            tpl.setAttribute('id', id);
            tpl.setAttribute('class', 'd-none');
            document.body.append(tpl);
        }
        tpl.innerHTML = '';
        array.forEach((value) => {
            const dateSpan = document.createElement('span');
            dateSpan.innerHTML = value;
            tpl.append(dateSpan);
        });
        return tpl;
    },

    /**
     * @param {array} array
     * @returns {*[]}
     */
    clearArrayCapitalize(array) {
        const a = [];
        array.forEach((value) => {
            console.log(value);
            console.log(value.split(' ').map(capitalize).join(' '));
            a.push(value.split(' ').map(capitalize).join(' '));
        });
        return a;
    },

    /**
     * @param node
     * @returns {*[]}
     */
    prepareNodes(node) {
        const p = [];
        node.childNodes.forEach((item) => {
            const text = item.innerText;
            const left = item.style.left !== undefined ? parseFloat(item.style.left) : -1;
            if (text !== undefined && left > 0) {
                p.push({
                    text: text,
                    left: left,
                    top: parseFloat(item.style.top),
                });
            }
        });
        return p;
    },

    /**
     *
     * @param {string} text
     * @returns {null}
     */
    checkIfNewText(text) {
        // if (text.startsWith('—')) {
        //     return true;
        // }
        if (text.startsWith('•')) {
            return true;
        }
        return false;
    },

    /**
     *
     * @param {object: {text: string, left: integer, top: integer}} current
     * @param {object: {text: string, left: integer, top: integer}} previous
     * @returns {boolean}
     */
    checkIsNewLineByPrevious(current, previous) {
        if (current.text.startsWith('—')) {
            if (current.top > previous.top) {
                return true;
            }
        }
        return false;
    },

    /**
     *
     * @param {array} all
     * @param {array} sorted
     * @param {integer} topHead
     * @returns {*}
     */
    pushToAll(all, sorted, topHead) {
        if (sorted[`${topHead}`] !== undefined) {
            all.push(sorted[`${topHead}`]);
        }
        return all;
    },

    sortByColumns(nodes) {
        const self = this;
        let top = 0;
        let array = [];
        const array1 = [];
        const array2 = [];
        let isNewColumn = false;
        nodes.forEach((value, key) => {
            if (top > value.top) {
                const l = top - value.top;
                if (l > self.data.hIndex) {
                    isNewColumn = true;
                }
            }
            if (isNewColumn) {
                array2.push(value);
            } else {
                array1.push(value);
            }
            top = value.top;
        });
        array = [
            array1,
            array2,
        ];
        return array;
    },
    setOptions(options) {
        if (options.h !== undefined) {
            this.data.h = options.h;
        }
        return this;
    },
    sortTextsByTwoColumns(nodes) {
        const self = this;
        const result = self.sortByColumns(nodes);
        console.log('sortTextsByTwoColumns:before', result);
        let all = [];
        result.forEach((array) => {
            const res = self.sortTexts(array);
            console.log('sortTextsByTwoColumns:forEach', res);
            if (self.data.mergeColumns && all[all.length - 1] !== undefined && res.all[0] !== undefined && !res.firstIsNewLine) {
                all[all.length - 1] = all[all.length - 1].concat(res.all.shift());
            }
            all = all.concat(res.all);
        });
        console.log('sortTextsByTwoColumns:after', all);
        return all;
    },

    /**
     *
     * @param nodes
     * @returns {object}
     */
    sortTexts(nodes) {
        const self = this;
        let minLeft = self.checkMinAndUsualLeft(nodes);
        console.log('minLeft', minLeft);
        if (minLeft !== null && nodes.length < self.data.nodesLengthMin) {
            minLeft = null;
        }
        if (minLeft !== null && self.checkTwoColumns(nodes)) {
            minLeft = null;
        }
        console.log('sortTexts', nodes);
        console.log('checkTwoColumns', this.checkTwoColumns(nodes));
        console.log('minLeft', minLeft);
        let prev = {
            top: null,
            left: null,
            text: null,
            hasSpace: null,
        };
        let topHead = 0;
        const sorted = [];
        let all = [];
        let firstIsNewLine = false;
        nodes.forEach((value, key) => {
            let text = value.text;
            // удалить символ в конце строке, в статьях это обычно знак переноса
            if (self.data.deleteLastSymbolNewLine && text.endsWith('-')) {
                text = text.substring(0, text.length - 1);
            }
            if (nodes[key - 1] !== undefined && self.checkIsNewLineByPrevious(value, nodes[key - 1])) {
                all = self.pushToAll(all, sorted, topHead);
                // new
                sorted[`${value.top}`] = text;
                topHead = value.top;
                prev = {
                    top: value.top,
                    left: value.left,
                    text: value.text,
                    hasSpace: text.endsWith(' '),
                };
                console.log('checkIsNewLineByPrevious', value.text);
                return true;
            }
            if (!self.checkIfNewText(text)) {
                const l = value.top - prev.top;
                if (l < self.data.h) {
                    // old
                    if (value.top !== prev.top) {
                        if (minLeft !== null && value.left > minLeft) {
                            if (self.checkLeft(value, minLeft)) {
                                if (sorted[`${topHead}`] !== undefined) {
                                    all.push(sorted[`${topHead}`]);
                                }
                                // new
                                sorted[`${value.top}`] = text;
                                topHead = value.top;
                                console.log('NewByLastLeftAndLeft', value.text, text);
                            } else {
                                sorted[`${topHead}`] += text;
                            }
                        } else {
                            if (!prev.hasSpace && !prev.text.endsWith('-')) {
                                text = ' ' + text;
                                console.log('prevHasSpace', prev.hasSpace, text);
                            }
                            sorted[`${topHead}`] += text;
                        }
                    } else {
                        sorted[`${topHead}`] += text;
                    }
                } else {
                    if (all.length === 0 && !firstIsNewLine && self.checkLeft(value, minLeft)) {
                        firstIsNewLine = true;
                    }
                    all = self.pushToAll(all, sorted, topHead);
                    // new
                    sorted[`${value.top}`] = text;
                    topHead = value.top;
                }
            } else {
                all = self.pushToAll(all, sorted, topHead);
                // new
                sorted[`${value.top}`] = text;
                topHead = value.top;
            }
            if (nodes[key + 1] === undefined) {
                all.push(sorted[`${topHead}`]);
            }
            prev = {
                top: value.top,
                left: value.left,
                text: value.text,
                hasSpace: text.endsWith(' '),
            };
        });
        return {
            all,
            firstIsNewLine,
        };
    },

    /**
     *
     * @param {object} value
     * @param {float} minLeft
     * @returns {boolean}
     */
    checkLeft(value, minLeft) {
        const self = this;
        const lMin = self.data.lMin;
        const lMax = self.data.lMax;
        const lR = value.left - minLeft;
        const result = lR > lMin && lR < lMax;
        if (result) {
            console.log('right', lR);
        }
        return result;
    },

    /**
     *
     * @param nodes
     * @returns {float|null}
     */
    checkMinAndUsualLeft(nodes) {
        const min = this.getLeftMin(nodes);
        console.log('checkMinAndUsualLeft:min', min);
        const usual = this.getLeftUsual(nodes);
        console.log('checkMinAndUsualLeft:usual', usual);
        return parseInt(min) === parseInt(usual) ? min : null;
    },

    /**
     *
     * @param nodes
     * @returns {float}
     */
    getLeftUsual(nodes) {
        const array = [];
        const keyArray = -1;
        let length = 0;
        let usualLeft = 0;
        nodes.forEach((value, key) => {
            if (array[`${value.left}`] === undefined) {
                array[`${value.left}`] = [];
            }
            array[`${value.left}`].push(value.left);

            if (array[`${value.left}`].length > length) {
                length = array[`${value.left}`].length;
                usualLeft = value.left;
            }
        });
        //console.log(array.sort().reverse().shift());
        // const usual = array.sort().reverse().shift();
        // if (usual !== undefined) {
        //     return parseFloat(usual.shift());
        // }
        return usualLeft;
    },
    /**
     *
     * @param nodes
     * @returns {float}
     */
    getLeftMin(nodes) {
        let left = 0;
        nodes.forEach((value, key) => {
            if (left === 0) {
                left = value.left;
            }
            if (left > value.left) {
                left = value.left;
            }
        });
        return left;
    },

    /**
     *
     * @param nodes
     * @returns {number}
     */
    checkTwoColumns(nodes) {
        const self = this;
        let top = 0;
        const hIndex = 2;
        let hasTwoColumns = false;
        nodes.forEach((value, key) => {
            if (hasTwoColumns) {
                return true;
            }
            if (top === 0) {
                top = value.top;
            } else {
                if (top > value.top) {
                    const l = top - value.top;
                    if (l > self.data.hIndex) {
                        hasTwoColumns = true;
                    } else {
                        top = value.top;
                    }
                }
            }
        });
        return hasTwoColumns;
    }
};
