const state = () => ({
    pending: false,
    data: [],
});

const getters = {
    pending(state) {
        return state.pending;
    },
    data(state) {
        return state.data;
    },
};

const actions = {
    fetchLoading({ commit }, data) {
        commit('pending', true);
        setTimeout(() => {
            commit('pending', false);
        }, 1000);
    }
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, val) => {
        state.data = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
