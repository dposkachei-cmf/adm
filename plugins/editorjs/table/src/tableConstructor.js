import './styles/table-constructor.pcss';
import {create} from './documentUtils';
import {Table} from './table';

const CSS = {
    cell: 'tc-table__cell',
    editor: 'tc-editor',
    toolBarHor: 'tc-toolbar--hor',
    toolBarVer: 'tc-toolbar--ver',
    inputField: 'tc-table__inp'
};

/**
 * Entry point. Controls table and give API to user
 */
export class TableConstructor {
    /**
     * Creates
     * @param {TableData} data - previously saved data for insert in table
     * @param {object} config - configuration of table
     * @param {object} api - Editor.js API
     */
    constructor(data, config, api) {
        /** creating table */
        this._table = new Table(api);
        const size = this._resizeTable(data, config);

        this._fillTable(data, size);

        /** creating container around table */
        this._container = create('div', [CSS.editor, api.styles && api.styles.block], null, [
            this._table.htmlElement
        ]);
    }

    /**
     * returns html element of TableConstructor;
     * @return {HTMLElement}
     */
    get htmlElement() {
        return this._container;
    }

    /**
     * Returns instance of Table
     * @returns {Table}
     */
    get table() {
        return this._table;
    }

    /**
     * @private
     *
     *  Fill table data passed to the constructor
     * @param {TableData} data - data for insert in table
     * @param {{rows: number, cols: number}} size - contains number of rows and cols
     */
    _fillTable(data, size) {
        console.log(data);
        if (data.content !== undefined) {
            console.log('size', size);
            console.log(data.content.length);
            for (let i = 0; i < size.rows && i < data.content.length; i++) {
                console.log(data.content[i].length);
                for (let j = 0; j < size.cols && j < data.content[i].length; j++) {
                    // get current cell and her editable part
                    const input = this._table.body.rows[i].cells[j].querySelector('.' + CSS.inputField);

                    input.innerHTML = data.content[i][j];
                    if (data.alignment !== undefined && data.alignment[i] !== undefined && data.alignment[i][j] !== undefined) {
                        input.closest('.' + CSS.cell).classList.add('tc-table__' + data.alignment[i][j]);
                    }
                    // if (data.span !== undefined && data.span.col !== undefined && data.span.col[i] !== undefined && data.span.col[i][j] !== undefined) {
                    //     input.closest('.' + CSS.cell).colSpan = data.span.col[i][j];
                    // }
                    // if (data.span !== undefined && data.span.row !== undefined && data.span.row[i] !== undefined && data.span.row[i][j] !== undefined) {
                    //     input.closest('.' + CSS.cell).rowSpan = data.span.row[i][j];
                    // }
                }
            }
        }
    }

    /**
     * @private
     *
     * resize to match config or transmitted data
     * @param {TableData} data - data for inserting to the table
     * @param {object} config - configuration of table
     * @param {number|string} config.rows - number of rows in configuration
     * @param {number|string} config.cols - number of cols in configuration
     * @return {{rows: number, cols: number}} - number of cols and rows
     */
    _resizeTable(data, config) {
        const isValidArray = Array.isArray(data.content);
        console.log('data.content', data.content);
        const isNotEmptyArray = isValidArray ? data.content.length : false;
        const contentRows = isValidArray ? data.content.length : undefined;
        const contentCols = isNotEmptyArray ? data.content[0].length : undefined;
        const parsedRows = Number.parseInt(config.rows);
        const parsedCols = Number.parseInt(config.cols);
        // value of config have to be positive number
        const configRows = !isNaN(parsedRows) && parsedRows > 0 ? parsedRows : undefined;
        const configCols = !isNaN(parsedCols) && parsedCols > 0 ? parsedCols : undefined;
        const defaultRows = 1;
        const defaultCols = 1;
        const rows = contentRows || configRows || defaultRows;
        const cols = contentCols || configCols || defaultCols;

        // for (let i = 0; i < rows; i++) {
        //     //this._table.insertRow();
        // }
        //
        // let baseRows = 0;
        // let baseCols = 0;
        // for (let i = 0; i < data.content.length; i++) {
        //     baseRows++;
        //     for (let j = 0; j < data.content[i].length; j++) {
        //         if (data.span !== undefined && data.span.row !== undefined && data.span.row[i] !== undefined && data.span.row[i][j] !== undefined) {
        //             this._table.insertRow(0, data.span.row[i][j]);
        //         }
        //         this._table.insertColumn();
        //         baseCols++;
        //     }
        // }
        // console.log({
        //     rows: baseRows,
        //     cols: baseCols,
        // });
        // console.log({
        //     rows: rows,
        //     cols: cols,
        // });

        for (let i = 0; i < rows; i++) {
            this._table.insertRow();
        }
        for (let i = 0; i < cols; i++) {
            this._table.insertColumn();
        }

        return {
            rows: rows,
            cols: cols
        };
    }
}
