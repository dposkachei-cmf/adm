export default ({ app, redirect }) => {
    // the following look directly for the cookie created by nuxtjs/auth
    // instead of using $auth.loggedIn

    if (app.$auth.$state.loggedIn) {
        return redirect('/');
    }
};
