export default {
    /**
     *
     * @param timer
     * @param fn
     * @param delay
     * @returns {Object}
     */
    formTimeout(timer, fn, delay = 500) {
        if (timer !== null) {
            clearTimeout(timer);
            timer = null;
        }
        timer = setTimeout(() => {
            fn();
        }, delay);
        return timer;
    },
};
