import moment from 'moment';
import response from '../../../../plugins/common/response';
import api from '../../../../api';

// initial state
const state = () => ({
    loaded: false,
    pending: true,
    data: [],
    echo: [],
    pagination: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    echo(state) {
        return state.echo;
    },
    pending(state) {
        return state.pending;
    },
    pagination(state) {
        return state.pagination;
    },
    loaded(state) {
        return state.loaded;
    },
};

// actions
const actions = {
    async fetchChats({ commit }, data) {
        const self = this;
        commit('pending', true);
        let url = api.user.chats.index;
        if (data.q !== undefined && data.q !== '') {
            url += '?q=' + data.q;
        }
        await self.$axios.get(url)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                commit('echo', result.data);
                commit('loaded', true);
                commit('pagination', null);
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
    setLastMessage({ commit, state }, data) {
        commit('changeDataLastMessage', data);
    },
    setReadChatMessages({ commit, state }, data) {
        commit('changeDataRead', data);
    },
    setLoaded({ commit, state }, value) {
        commit('loaded', value);
    }
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
    echo: (state, data) => {
        const echo = [];
        data.forEach(function (item, key) {
            echo.push({
                id: item.id,
            });
        });
        state.echo = echo;
    },
    changeDataLastMessage: (state, data) => {
        const value = state.data;
        let lastChat = null;
        let lastChatKey = null;
        value.forEach(function (item, key) {
            if (item.id === data.chat_id) {
                item.last_message = data.text;
                item.last_message_user_id = data.user_id;
                item.last_message_at = data.send_at;
                item.read_at = data.read_at;
                //
                lastChat = item;
                lastChatKey = key;
            }
        });
        if (lastChat !== null && lastChatKey !== null && lastChatKey !== 0) {
            value.splice(lastChatKey, 1);
            //delete value[lastChatKey];
            value.unshift(lastChat);
        }
        state.data = value;
    },
    changeDataRead: (state, data) => {
        const value = state.data;
        value.forEach(function (item) {
            if (item.id === data.id) {
                item.read_at = moment().toISOString();
            }
        });
        state.data = value;
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
    loaded: (state, val) => {
        state.loaded = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
