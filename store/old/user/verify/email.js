import response from '../../../../plugins/common/response';
import api from '../../../../api';

const state = () => ({
    pending: false,
});

const getters = {
    pending(state) {
        return state.pending;
    },
};

const actions = {
    async fetchEmail({ commit }, data) {
        const self = this;
        commit('pending', true);
        await self.$axios.post(api.user.verify.email, data)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                if (result.success && result.message !== undefined) {
                    self.$toast.success(result.message);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    response.error(error);
                }
            });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
