import Vue from 'vue';
import Preloader from './../components/Base/Preloader';
import FormError from './../components/Base/Form/FormError';
//import IconFavorite from './../components/Base/Icon/IconFavorite';
//import ButtonFavorite from './../components/Base/Button/ButtonFavorite';
//import TitleLine from './../components/Base/Title/TitleLine';
//import FormCheckboxToggle from './../components/Base/Form/FormCheckboxToggle';
//import Markdown from './../components/Base/Markdown/BaseMarkdown';

Vue.component('Preloader', Preloader);
Vue.component('FormError', FormError);
//Vue.component('IconFavorite', IconFavorite);
//Vue.component('ButtonFavorite', ButtonFavorite);
//Vue.component('TitleLine', TitleLine);
//Vue.component('FormCheckboxToggle', FormCheckboxToggle);
//Vue.component('Markdown', Markdown);
