export default {
    'need-login': {
        favorites: 'Необходимо авторизоваться чтобы добавлять в избранное',
        watches: 'Необходимо авторизоваться чтобы добавлять в "Читать позднее"',
        reports: 'Необходимо авторизоваться чтобы пожаловаться',
        rating: 'Необходимо авторизоваться чтобы оценивать статью',
        comment: 'Необходимо авторизоваться чтобы оставлять комментарии',
        subscription: 'Необходимо авторизоваться чтобы подписаться',
        chat: 'Необходимо авторизоваться чтобы написать пользователю',
        'rate-article': 'Необходимо авторизоваться чтобы оценивать статью',
        'rate-comment': 'Необходимо авторизоваться чтобы оценивать комментарий',
        'users-lock': 'Необходимо авторизоваться чтобы блокировать пользователей',
    }
};
