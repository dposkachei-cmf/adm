import response from '../../../../../plugins/common/response';
import api from '../../../../../api';

const state = () => ({
    pending: false,
    pendingArray: [],
});

const getters = {
    pending(state) {
        return state.pending;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
};

const actions = {
    async fetchMagnet({ state, commit }, data) {
        const self = this;
        commit('pending', true);
        commit('pendingArray', response.pendingAdd(state.pendingArray, data.article_id, 'article'));
        let url = api.user.groups.articles.magnet;
        url = url.replace(':id', data.id);
        url = url.replace(':article_id', data.article_id);
        await self.$axios.post(url)
            .then((res) => {
                commit('pending', false);
                commit('pendingArray', response.pendingRemove(state.pendingArray, data.article_id, 'article'));
                //self.$auth.fetchUser().then(function () {});
                self.dispatch('groups/articles/fetchGroupArticles', data);
                self.dispatch('user/groups/show/fetchDataForce', data);
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                    commit('pendingArray', response.pendingRemove(state.pendingArray, data.article_id, 'article'));
                }
            });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    pendingArray: (state, val) => {
        state.pendingArray = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
