const path = require('path');

module.exports = {
    entry: './src/index.js',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                    },
                ],
            },
            {
                test: /\.css$/,
                use: [
                    { loader: 'style-loader', options: { attributes: { id: 'editorjs-tooltip' } } },
                    { loader: 'css-loader' },
                ],
            },
            {
                test: /\.scss$/,
                include: path.resolve(__dirname, '../'),
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: 'svg-inline-loader',
                        options: {
                            removeSVGTagAttrs: false,
                        },
                    },
                ],
            },
        ],
    },
    output: {
        path: path.join(__dirname, '/dist'),
        publicPath: '/',
        filename: 'bundle.js',
        library: 'Tooltip',
        libraryTarget: 'umd',
        libraryExport: 'default',
    },
};
