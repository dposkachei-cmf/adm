import response from '../../../../plugins/common/response';
import api from './../../../../api';
import axios from "./../../../../plugins/axios";

const state = () => ({
    published: false,
    pending: false,
    pendingArray: [],
});

const getters = {
    published(state) {
        return state.published;
    },
    pending(state) {
        return state.pending;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
};

// actions
const actions = {
    async fetchPublish({ commit, state }, data) {
        const self = this;
    },
};

// mutations
const mutations = {
    published: (state, val) => {
        state.published = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    pendingArray: (state, val) => {
        state.pendingArray = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
