import api from "../../api";

export default {



    getPanelUpdateUrl(url, model, id) {
        url = url.replace(':id', id);
        url = url.replace(':model', model);
        return url;
    },
    getPanelShowUrl(url, model, id) {
        url = url.replace(':id', id);
        url = url.replace(':model', model);
        return url;
    },
    getPanelCommentsUrl(url, model, id) {
        url = url.replace(':id', id);
        url = url.replace(':model', model);
        return url;
    },
    getPanelEditUrl(url, model, id) {
        url = url.replace(':id', id);
        url = url.replace(':model', model);
        return url;
    },
    getTableBanned(url, model, id) {
        url = url.replace(':id', id);
        url = url.replace(':model', model);
        return url;
    },
    getUrlModel(url, model) {
        url = url.replace(':model', model);
        return url;
    },
    getUrlModelId(url, model, id) {
        url = url.replace(':id', id);
        url = url.replace(':model', model);
        return url;
    },
    getUrlModelIdImageId(url, model, id, image_id) {
        url = url.replace(':id', id);
        url = url.replace(':model', model);
        url = url.replace(':image_id', image_id);
        return url;
    },
    getUrlModelIdArticleId(url, model, id, article_id) {
        url = url.replace(':id', id);
        url = url.replace(':model', model);
        url = url.replace(':article_id', article_id);
        return url;
    },

    getPanelStoreUrl(url, model) {
        url = url.replace(':model', model);
        return url;
    },
    getPanelCommentUrl(url, model, id) {
        url = url.replace(':id', id);
        url = url.replace(':model', model);
        return url;
    },
    getPanelFieldsUrl(url, model) {
        url = url.replace(':model', model);
        return url;
    },

    /**
     *
     * @param {string} url
     * @param {string} model
     * @returns {string}
     */
    getTableListUrl(url, model) {
        return url.replace(':model', model);
    },

    /**
     *
     * @param {string} url
     * @param {string} model
     * @param {int} id
     * @returns {string}
     */
    getTableDeleteUrl(url, model, id) {
        url = url.replace(':id', '' + id);
        url = url.replace(':model', model);
        return url;
    },

    /**
     *
     * @param {object} request
     * @param {{model: string, locale: string|undefined, page: int, request: object}} data
     * @param {string} locale
     * @returns {object}
     */
    getTableListRequest(request, data, locale) {
        let values = Object.assign({}, request);
        if (data.page !== undefined) {
            values.page = data.page;
        }
        if (data.request !== undefined) {
            values = Object.assign({}, values, data.request);
        }
        // @todo locale Убрать комментарий если надо выводить с локальностью в url
        //values.locale = locale;
        return values;
    },
};
