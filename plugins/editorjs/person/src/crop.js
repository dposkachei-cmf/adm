import Cropper from 'cropperjs';
/**
 * Module for file uploading. Handle 3 scenarios:
 *  1. Select file from device and upload
 *  2. Upload by pasting URL
 *  3. Upload by pasting file from Clipboard or by Drag'n'Drop
 */
export default class Crop {
    /**
     * @param {object} params - uploader module params
     * @param {CropConfig} params.config - image tool config
     * @param {Function} params.onCrop - one callback for all uploading (file, url, d-n-d, pasting)
     * @param {Function} params.onError - callback for uploading errors
     */
    constructor({
        config,
        onCrop,
        onError
    }) {
        this.config = config;
        this.onCrop = onCrop;
        //this.onError = onError;
    }

    /**
     * CSS classes
     *
     * @returns {object}
     */
    get CSS() {
        return {
            containerId: 'cropper-container',
            container: 'vm--cropper-container',
            containerRounded: '__rounded',
            imageId: 'cropper-image',
        };
    }

    /**
     *
     * @param sourceCanvas
     * @returns {HTMLCanvasElement}
     */
    getRoundedCanvas(sourceCanvas) {
        let canvas = document.createElement('canvas');
        let context = canvas.getContext('2d');
        let width = sourceCanvas.width;
        let height = sourceCanvas.height;

        canvas.width = width;
        canvas.height = height;
        context.imageSmoothingEnabled = true;
        context.drawImage(sourceCanvas, 0, 0, width, height);
        context.globalCompositeOperation = 'destination-in';
        context.beginPath();
        context.arc(width / 2, height / 2, Math.min(width, height) / 2, 0, 2 * Math.PI, true);
        context.fill();
        return canvas;
    }

    /**
     *
     */
    createBootstrapModal() {
        const self = this;
        const body = document.querySelector('body');
        const modalContainer = document.createElement('div');
        modalContainer.id = self.CSS.containerId;
        modalContainer.classList.add(self.CSS.container);
        body.appendChild(modalContainer);

        if (self.config.crop.rounded) {
            modalContainer.classList.add(self.CSS.containerRounded);
        }

        //
        const modalOverlay = document.createElement('div');
        modalOverlay.classList.add('modal-backdrop');
        modalOverlay.classList.add('fade');
        modalOverlay.classList.add('show');
        modalContainer.appendChild(modalOverlay);
        modalOverlay.addEventListener('click', (event) => {
            self.removeModal();
        });

        //
        const modal = document.createElement('div');
        modal.classList.add('modal');
        modal.classList.add('fade');
        modal.classList.add('show');
        modal.style.display = 'block';
        modalContainer.appendChild(modal);

        //
        const dialog = document.createElement('div');
        dialog.classList.add('modal-dialog');
        dialog.classList.add('modal-dialog-centered');
        dialog.classList.add('modal-dialog-scrollable');
        dialog.classList.add('modal-dialog-croppable');
        modal.appendChild(dialog);

        //
        const modalContent = document.createElement('div');
        modalContent.classList.add('modal-content');
        dialog.appendChild(modalContent);

        // modal-header
        const modalHeader = document.createElement('div');
        modalHeader.classList.add('modal-header');

        // modal-header title
        const title = document.createElement('h5');
        title.classList.add('modal-title');
        modalHeader.appendChild(title);

        // modal-header button
        const headerButton = document.createElement('button');
        headerButton.type = 'button';
        headerButton.classList.add('close');
        headerButton.innerHTML = '<span aria-hidden="true">&times;</span>';
        modalHeader.appendChild(headerButton);
        headerButton.addEventListener('click', (event) => {
            self.removeModal();
        });
        modalContent.appendChild(modalHeader);

        // modal-body
        const modalBody = document.createElement('div');
        modalBody.classList.add('modal-body');

        const modalBodyDiv = document.createElement('div');
        modalBodyDiv.classList.add('modal-body-container');

        const modalBodyImg = document.createElement('img');
        modalBodyImg.style.width = '100%';
        modalBodyImg.style.height = '100%';
        modalBodyImg.id = self.CSS.imageId;

        modalBodyDiv.appendChild(modalBodyImg);
        modalBody.appendChild(modalBodyDiv);
        modalContent.appendChild(modalBody);

        //
        const modalFooter = document.createElement('div');
        modalFooter.classList.add('modal-footer');

        // modal-header button 1
        const footerButton1 = document.createElement('button');
        footerButton1.type = 'button';
        footerButton1.classList.add('btn');
        footerButton1.classList.add('btn-secondary');
        footerButton1.innerHTML = 'Отмена';
        modalFooter.appendChild(footerButton1);
        footerButton1.addEventListener('click', (event) => {
            self.removeModal();
        });

        // modal-header button 2
        const footerButton2 = document.createElement('button');
        footerButton2.type = 'button';
        footerButton2.classList.add('btn');
        footerButton2.classList.add('btn-primary');
        footerButton2.innerHTML = 'Применить';
        modalFooter.appendChild(footerButton2);
        footerButton2.addEventListener('click', (event) => {
            self.onSubmit();
        });

        modalContent.appendChild(modalFooter);
    }

    /**
     *
     */
    removeModal() {
        const self = this;
        const body = document.querySelector('body');
        const modalContainer = document.getElementById(self.CSS.containerId);
        body.removeChild(modalContainer);
    }

    /**
     *
     * @param {string} url
     */
    initCropper(url) {
        const self = this;
        self.createBootstrapModal();
        const image = document.getElementById(self.CSS.imageId);
        image.src = url;
        const options = {
            viewMode: 1,
            zoomable: false,
            movable: false,
            scalable: false,
            minContainerWidth: 10,
            minContainerHeight: 10,
            minCropBoxWidth: 10,
            minCropBoxHeight: 10,
            autoCropArea: 1,
            background: false,
            crop(event) {
                // console.log(event.detail.x);
                // console.log(event.detail.y);
                // console.log(event.detail.width);
                // console.log(event.detail.height);
                // console.log(event.detail.rotate);
                // console.log(event.detail.scaleX);
                // console.log(event.detail.scaleY);
                self.config.croppable = true;
            },
        };
        if (self.config.crop.rounded) {
            options.aspectRatio = 1;
        }
        self.config.cropper = new Cropper(image, options);
    }

    /**
     *
     */
    onSubmit() {
        const self = this;
        if (!self.config.croppable) {
            return;
        }
        const croppedCanvas = self.config.cropper.getCroppedCanvas();
        const roundedCanvas = self.getRoundedCanvas(croppedCanvas);
        roundedCanvas.toBlob((blob) => {
            self.onCrop(blob);
        });
        self.removeModal();
    }
}
