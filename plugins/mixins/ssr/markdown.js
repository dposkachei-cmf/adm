export default {
    markdown() {
        const self = this;
        const render = (value) => {
            if (value === null) {
                return '';
            }
            return self.$md.render(value, {
                html: true,
            });
        };
        return {
            render,
        };
    },
};
