import response from '../../../../plugins/common/response';
import api from '../../../../api';

// initial state
const state = () => ({
    loaded: false,
    pending: false,
    data: {
        articles: {
            up: [],
            down: [],
        },
        comments: {
            up: [],
            down: [],
        },
    },
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    loaded(state) {
        return state.loaded;
    },
};

// actions
const actions = {
    fetchVotesClear({ state, commit }, data) {
        commit('data', {
            articles: {
                up: [],
                down: [],
            },
            comments: {
                up: [],
                down: [],
            },
        });
    },
    async fetchVotes({ state, commit }, data) {
        const self = this;
        commit('pending', true);
        await self.$axios.get(api.user.votes.array)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                if (!state.loaded) {
                    commit('loaded', true);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
    setData({ state, commit }, data) {
        commit('data', data);
        commit('loaded', true);
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    loaded: (state, data) => {
        state.loaded = data;
    },
    data: (state, data) => {
        state.data = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
