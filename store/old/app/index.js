const state = () => ({
    fontSize: 'md', // sm, md, xl
    backgroundStyle: 'white', // pattern, sepia, dark, white
    sidebarGroupsLength: 5
});

const getters = {
    sidebar(state) {
        return state.sidebar;
    },
    sidebarRight(state) {
        return state.sidebarRight;
    },
    sidebarGroupsLength(state) {
        return state.sidebarGroupsLength;
    },
    fontSize(state) {
        return state.fontSize;
    },
    backgroundStyle(state) {
        return state.backgroundStyle;
    },
};

const actions = {
    setSidebar({ commit }, visible) {
        commit('sidebar', visible);
    },
    setSidebarRight({ commit }, visible) {
        commit('sidebarRight', visible);
    },
    setFontSize({ commit }, visible) {
        commit('fontSize', visible);
    },
    setBackgroundStyle({ commit }, visible) {
        commit('backgroundStyle', visible);
    },
};

const mutations = {
    sidebar: (state, val) => {
        state.sidebar = val;
    },
    sidebarRight: (state, val) => {
        state.sidebarRight = val;
    },
    fontSize: (state, val) => {
        state.fontSize = val;
    },
    sidebarGroupsLength: (state, val) => {
        state.sidebarGroupsLength = val;
    },
    backgroundStyle: (state, val) => {
        state.backgroundStyle = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
