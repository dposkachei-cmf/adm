export default {
    authLogout() {
        const self = this;
        setTimeout(function () {
            self.$store.dispatch('user/getMessagesCount');
            self.$store.dispatch('user/favoritesArray/fetchFavoritesClear');
        }, 1000);
    },
};
