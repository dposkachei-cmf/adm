export default {
    /**
     *
     * @param {Node} textarea
     */
    selections(textarea) {
        const start = textarea.selectionStart;
        const end = textarea.selectionEnd;
        return {
            start,
            end,
        };
    },
};
