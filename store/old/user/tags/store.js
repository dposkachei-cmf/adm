import response from '../../../../plugins/common/response';
import api from '../../../../api';

const state = () => ({
    pending: false,
    success: false,
    select: null,
    form: {
        title: '',
    },
    errors: {},
});

// getters
const getters = {
    pending(state) {
        return state.pending;
    },
    select(state) {
        return state.select;
    },
    form(state) {
        return state.form;
    },
    success(state) {
        return state.success;
    },
    errors(state) {
        return state.errors;
    },
};

// actions
const actions = {
    async fetchStore({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        commit('success', false);
        const url = api.user.tags.store;
        await self.$axios.post(url, {
            title: state.form.title,
        })
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                self.dispatch('user/articles/store/setFormValues', {
                    tags: [
                        result.data.tag.id,
                    ],
                    tagsArray: [
                        result.data.tag,
                    ],
                });
                self.dispatch('user/tags/array/setData', result.data.tags_array);
                commit('select', result.data.tag);
                commit('success', true);
                //commit('data', result.data);
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    select: (state, val) => {
        state.select = val;
    },
    form: (state, val) => {
        state.form = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
    success: (state, val) => {
        state.success = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
