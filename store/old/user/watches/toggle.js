import response from '../../../../plugins/common/response';
import api from '../../../../api';

// initial state
const state = () => ({
    pending: false,
    pendingArray: [],
});

// getters
const getters = {
    pending(state) {
        return state.pending;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
};

// actions
const actions = {
    async fetchWatchToggle({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        commit('pendingArray', response.pendingAdd(state.pendingArray, data.id, data.type));
        await self.$axios.post(api.user.watches.toggle, {
            item_type: data.type,
            item_id: data.id,
        })
            .then((res) => {
                commit('pending', false);
                commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, data.type));
                self.dispatch('user/watches/array/fetchWatches', {});
                //self.$auth.fetchUser().then(function () {});
                const result = response.success(res);
                if (result.success && result.message !== undefined) {
                    self.$toast.success(result.message);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, data.type));
                    const result = response.error(error);
                    if (result.message !== undefined) {
                        self.$toast.error(result.message);
                    }
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    pendingArray: (state, data) => {
        state.pendingArray = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
