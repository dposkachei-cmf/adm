import crpyto from './common/crpyto';

export default ({ $axios, app }) => {
    $axios.setHeader('Accept-Language', app.i18n.localeProperties.code);
    //$axios.setHeader('X-Auth-Pass', process.env.API_PASSWORD + process.env.APP_URL);
    $axios.setHeader('X-Auth-Pass', crpyto.headerXAuthPath());
    //$axios.setHeader('Access-Control-Allow-Origin', '*');
    console.log(app);
    const cookiesApp = app.$cookies.get('app.base', {
        parseJSON: true,
    });
    const modeDeveloper = cookiesApp !== undefined && cookiesApp.mode !== undefined ? cookiesApp.mode.developer : false;
    console.log(modeDeveloper);
    if (modeDeveloper) {
        $axios.setHeader('X-Developer', crpyto.headerXAuthPath());
    }
    $axios.onError((error) => {
        if (error.response !== undefined && error.response.status === 401) {
            app.$auth.logout();
            app.$auth.setUserToken(false);
            let url = '/';
            if (app.i18n.localeProperties.code === 'en') {
                url = '/en';
            }
            window.location.href = url;
        }
        if (error.response !== undefined && error.response.status === 429) {
            app.$toast.error('Слишком много запросов к апи, подождите', {
                duration: 3000,
            });
        }
    });
};
