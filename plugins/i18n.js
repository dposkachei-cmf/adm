export default function ({ app }) {
    if (app.i18n.localeProperties.code === 'en') {
        app.i18n.setLocale('ru');
    }
    // beforeLanguageSwitch called right before setting a new locale
    app.i18n.onBeforeLanguageSwitch = (oldLocale, newLocale, isInitialSetup, context) => {
        // console.log(oldLocale, newLocale, isInitialSetup);
    };
    // onLanguageSwitched called right after a new locale has been set
    app.i18n.onLanguageSwitched = (oldLocale, newLocale) => {
        // if (newLocale === 'en') {
        //     window.location.href = `/en${app.router.history.current.fullPath}`;
        // } else {
        //     window.location.href = `${app.router.history.current.fullPath.replace('/en', '')}`;
        // }
        // console.log(oldLocale, newLocale);
    };
}
