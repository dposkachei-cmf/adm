const state = () => ({
    page: 1,
    zoom: 1,
    pages: 0,
    showPages: null,
    pending: false,
    url: null,
    //url: 'http://converter.localhost/storage/tmp/article/2021-11-09/c424ba3c-5bab-4d3f-af66-9bd4b91deee1/file/GeUvQ4IsYPdY.docx',
});

const getters = {
    page(state) {
        return state.page;
    },
    pending(state) {
        return state.pending;
    },
    pages(state) {
        return state.pages;
    },
    url(state) {
        return state.url;
    },
    zoom(state) {
        return state.zoom;
    },
    showPages(state) {
        return state.showPages;
    },
};

const actions = {
    setPage({ commit }, page) {
        commit('page', page);
    },
    setZoom({ commit }, zoom) {
        commit('zoom', zoom);
    },
    setPages({ commit }, pages) {
        commit('pages', pages);
    },
    setShowPages({ commit }, pages) {
        commit('showPages', pages);
    },
    setUrl({ commit }, value) {
        const self = this;
        commit('url', value);
        if (value === null) {
            self.dispatch('app/editor/articles/store/setFormSource', {
                type: 'preview',
                source: null,
            });
        } else {
            commit('page', 1);
            self.dispatch('app/editor/articles/store/setFormSource', {
                type: 'word',
                source: value,
            });
        }
    },
    setPending({ commit }, value) {
        commit('pending', value);
    },
    setPdfDefault({ commit }) {
        commit('url', null);
        commit('page', 1);
        commit('zoom', 1);
        commit('pages', 0);
        commit('showPages', null);
    }
};

const mutations = {
    page: (state, val) => {
        state.page = val;
    },
    pages: (state, val) => {
        state.pages = val;
    },
    url: (state, val) => {
        state.url = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    zoom: (state, val) => {
        state.zoom = val;
    },
    showPages: (state, val) => {
        state.showPages = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
