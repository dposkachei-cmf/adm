import api from './../../../api';
import axiosStore from "../../../plugins/common/axiosStore";
import beforeFetch from "../../../plugins/common/beforeFetch";

const state = () => ({
    pending: false,
});

const getters = {
    pending(state) {
        return state.pending;
    },
};

const actions = {
    async fetchStore({ state, commit }, data) {
        const self = this;
        commit('pending', true);
        const url = beforeFetch.getPanelCommentUrl(api.panel.comments, data.model, data.id);
        await axiosStore.init(self.$axios).post(url, {
            text: data.text,
        }, function () {
            commit('pending', false);
        }, function () {
            commit('pending', false);
        });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
