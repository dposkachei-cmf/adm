import response from '../../../../plugins/common/response';
import api from '../../../../api';

const state = () => ({
    success: false,
    pending: false,
    errors: {},
});

const getters = {
    success(state) {
        return state.success;
    },
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
};

const actions = {
    async fetchCommentStore({ commit }, data) {
        const self = this;
        commit('success', false);
        commit('pending', true);
        commit('errors', {});
        const url = api.articles.commentsStore.replace(':id', data.id);
        await self.$axios.post(url, {
            text: data.text,
            reply_id: data.reply_id,
            parent_id: data.parent_id,
        })
            .then((res) => {
                const resp = response.success(res);
                commit('pending', false);
                commit('success', true);
                self.dispatch('article/comments/fetchCommentsInitialWithoutPending', {
                    id: data.id,
                });
                self.dispatch('article/counters/fetchCounters', {
                    id: data.id,
                });
                if (resp.success && resp.message !== undefined) {
                    console.log(self.$toast.success(resp.message));
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                }
            });
    },
};

const mutations = {
    success: (state, val) => {
        state.success = val;
        setTimeout(function () {
            state.success = false;
        }, 1500);
    },
    pending: (state, val) => {
        state.pending = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
