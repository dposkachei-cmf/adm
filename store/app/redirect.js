const state = () => ({
    route: null,
});

const getters = {
    route(state) {
        return state.route;
    },
};

const actions = {
    setRoute({ commit }, name) {
        commit('route', name);
    },
};

const mutations = {
    route: (state, val) => {
        state.route = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
