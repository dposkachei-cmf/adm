import response from '../../../plugins/common/response';

const state = () => ({
    pending: false,
    reloaded: false,
    data: {},
    errors: {},
});

const getters = {
    pending(state) {
        return state.pending;
    },
    reloaded(state) {
        return state.reloaded;
    },
    data(state) {
        return state.data;
    },
    errors(state) {
        return state.errors;
    },
};

const actions = {
    async fetchLogin({ commit }, data) {
        const self = this;
        commit('pending', true);
        commit('errors', {});
        await self.$axios.post('/auth/login', data)
            .then((res) => {
                commit('pending', false);
                commit('reloaded', true);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                if (result.success && result.message !== undefined) {
                    self.$toast.success(result.message);
                }
                if (result.data.token !== undefined) {
                    self.$auth.setUserToken(result.data.token).then(() => {
                        window.location.href = self.$router.history.current.path;
                    });
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                }
            });
    },
    setPending({ commit }, value) {
        commit('pending', value);
    }
};

const mutations = {
    reloaded: (state, val) => {
        state.reloaded = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, val) => {
        state.data = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
