import response from '../../../../plugins/common/response';
import api from '../../../../api';

// initial state
const state = () => ({
    pending: false,
    pendingArray: [],
});

// getters
const getters = {
    pending(state) {
        return state.pending;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
};

// actions
const actions = {
    async fetchToggle({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        commit('pendingArray', response.pendingAdd(state.pendingArray, data.id, data.type));
        await self.$axios.post(api.user.subscriptions.toggle, {
            item_type: data.type,
            item_id: data.id,
        })
            .then((res) => {
                commit('pending', false);
                commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, data.type));
                self.dispatch('user/subscriptions/array/fetchSubscriptions', {});
                self.dispatch('user/subscriptions/list/setLoaded', false);
                if (data.type === 'user') {
                    //self.dispatch('users/show/incrementCounterSubscribers', {});
                    self.dispatch('users/counters/fetchCounters', {
                        id: data.id,
                    });
                }
                if (data.type === 'category') {
                    //self.dispatch('categories/show/incrementCounterSubscribers', {});
                    self.dispatch('categories/counters/fetchCounters', {
                        id: data.id,
                    });
                }
                if (data.type === 'group') {
                    //self.dispatch('groups/show/incrementCounterSubscribers', {});
                    self.dispatch('groups/counters/fetchCounters', {
                        id: data.id,
                    });
                }
                //self.$auth.fetchUser().then(function () {});
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                    commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, data.type));
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    pendingArray: (state, data) => {
        state.pendingArray = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
