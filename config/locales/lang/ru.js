import basic from './ru/basic';
import button from './ru/button';
import auth from './ru/auth';
import fields from './ru/fields';
import tooltip from './ru/tooltip';
import declensions from './ru/declensions';
import route from './ru/route';
import message from './ru/message';
import modal from './ru/modal';
import table from './ru/table';
import editor from './ru/editor';
import article from './ru/article';
import project from './ru/project';

export default {
    basic,
    button,
    auth,
    fields,
    tooltip,
    declensions,
    route,
    message,
    modal,
    table,
    editor,
    article,
    project,
};
