import api from "../../../api";
import beforeFetch from "../../../plugins/common/beforeFetch";
import axiosStore from "../../../plugins/common/axiosStore";

const state = () => ({
    pending: false,
    force: false,
    restore: false,
    success: false,
    id: null,
});

const getters = {
    pending(state) {
        return state.pending;
    },
    id(state) {
        return state.id;
    },
    force(state) {
        return state.force;
    },
    restore(state) {
        return state.restore;
    },
    success(state) {
        return state.success;
    },
};

const actions = {
    async fetchDestroy({ commit }, data) {
        const self = this;
        commit('pending', true);
        const url = beforeFetch.getUrlModelId(api.table.destroy, data.model, data.id);
        const query = {};
        if (data.force !== undefined && data.force) {
            query.force = 1;
            commit('force', true);
        }
        await axiosStore.init(self.$axios).delete(url, query, function (result) {
            commit('pending', false);
            commit('force', false);
            commit('success', true);
            if (result.success && result.message !== undefined) {
                self.$toast.success(result.message);
            }
        }, function () {
            commit('pending', false);
            commit('force', false);
        });
    },
    async fetchRestore({ commit }, data) {
        const self = this;
        commit('pending', true);
        const url = beforeFetch.getUrlModelId(api.table.destroy, data.model, data.id);
        const query = {};
        commit('id', data.id);
        commit('restore', true);
        await axiosStore.init(self.$axios).delete(url, query, function (result) {
            commit('id', null);
            commit('pending', false);
            commit('restore', false);
            commit('success', true);
            if (result.success && result.message !== undefined) {
                self.$toast.success(result.message);
            }
        }, function () {
            commit('pending', false);
            commit('force', false);
        });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    force: (state, val) => {
        state.force = val;
    },
    restore: (state, val) => {
        state.restore = val;
    },
    id: (state, val) => {
        state.id = val;
    },
    success: (state, val) => {
        state.success = val;
        setTimeout(function () {
            state.success = false;
        }, 500);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
