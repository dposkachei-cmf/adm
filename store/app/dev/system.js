import api from "../../../api";
import axiosBase from "../../../plugins/common/axiosBase";

const state = () => ({
    pending: false,
    data: {},
});

const getters = {
    pending(state) {
        return state.pending;
    },
    data(state) {
        return state.data;
    },
};

const actions = {
    /**
     *
     * @param commit
     * @param state
     * @param {{action: string, callback: string|undefined}} data
     */
    async fetchSystem({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        let url = api.dev.system;
        await axiosBase.init(self.$axios).get(url, [], function (result) {
            commit('pending', false);
            commit('data', result.data);
        }, function (result) {
            commit('pending', false);
        });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, val) => {
        state.data = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
