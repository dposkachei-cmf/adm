import response from '../../../plugins/common/response';
import api from './../../../api';
import axiosStore from "../../../plugins/common/axiosStore";

// initial state
const state = () => ({
    pending: false,
    pendingArray: [],
});

// getters
const getters = {
    pending(state) {
        return state.pending;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
};

// actions
const actions = {
    async fetchToggle({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        const action = data.value === 1 ? 'up' : 'down';
        commit('pendingArray', response.pendingAdd(state.pendingArray, data.id, data.type + '_' + action));
        await axiosStore.init(self.$axios).post(api.user.votes.toggle, {
            item_type: data.type,
            item_id: data.id,
            value: data.value,
        }, function () {
            commit('pending', false);
            commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, data.type + '_' + action));
            self.dispatch('user/votes/array/fetchVotes', {});
            if (data.type === 'comment') {
                self.dispatch('app/comment/counters/fetchCounters', {
                    id: data.id,
                });
            }
        }, function () {
            commit('pending', false);
            commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, data.type + '_' + action));
        });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    pendingArray: (state, data) => {
        state.pendingArray = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
