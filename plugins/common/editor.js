export default {
    initMutationObserverFocusedBlock(selector, callback) {
        const mutationObserver = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                callback();
            });
        });
        mutationObserver.observe(document.querySelector(selector), {
            attributes: true,
            attributeFilter: ['class'],
            childList: true,
            subtree: true,
            characterData: true
        });
    },
    getFocused(item, key, focusedIndex, blocks) {
        if (item.classList.contains('ce-block--focused') && focusedIndex !== key) {
            if (blocks[key] !== undefined) {
                return {
                    index: key,
                    id: blocks[key].id,
                    isHeader: blocks[key].type === 'header' && blocks[key].data.level === 1,
                };
                //console.log(self.blocks[key].type, self.blocks[key].data.level);
            } else if (blocks.length) {
                const lastKey = blocks.length - 1;
                return {
                    index: lastKey,
                    id: blocks[lastKey].id,
                    isHeader: blocks[lastKey].type === 'header' && blocks[lastKey].data.level === 1,
                };
            }
        }
        return null;
    }
}
