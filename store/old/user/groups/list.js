import response from '../../../../plugins/common/response';
import api from '../../../../api';

// initial state
const state = () => ({
    pending: true,
    hasPending: true,
    data: [],
    pagination: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    hasPending(state) {
        return state.hasPending;
    },
    pagination(state) {
        return state.pagination;
    },
};

// actions
const actions = {
    async fetchGroups({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        await self.$axios.get(api.user.groups.index)
            .then((res) => {
                commit('pending', false);
                if (!state.hasPending) {
                    commit('hasPending', true);
                }
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                if (result.pagination !== undefined) {
                    commit('pagination', result.pagination);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
    async fetchGroupsWithoutPending({ commit }, data) {
        const self = this;
        commit('hasPending', false);
        await self.dispatch('user/groups/list/fetchGroups', data);
    },
    async fetchGroupsInitial({ commit }, data) {
        const self = this;
        commit('data', []);
        await self.dispatch('user/groups/list/fetchGroups', data);
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        if (state.hasPending) {
            state.pending = val;
        }
    },
    hasPending: (state, val) => {
        state.hasPending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
