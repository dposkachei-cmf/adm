const state = () => ({
    reply: false,
    replyId: null,
    replyText: null,
    replyParentId: null,
});

const getters = {
    reply(state) {
        return state.reply;
    },
    replyId(state) {
        return state.replyId;
    },
    replyText(state) {
        return state.replyText;
    },
    replyParentId(state) {
        return state.replyParentId;
    },
};

const actions = {
    setReplyShow({ commit }, data) {
        if (data.id !== undefined) {
            commit('reply', true);
            commit('replyId', data.id);
            if (data.parent_id === null) {
                data.parent_id = data.id;
            }
            commit('replyParentId', data.parent_id);
        } else {
            commit('reply', true);
        }
    },
    setReplyHide({ commit }) {
        commit('reply', false);
        commit('replyId', null);
        commit('replyParentId', null);
    },
};

const mutations = {
    reply: (state, val) => {
        state.reply = val;
    },
    replyId: (state, val) => {
        state.replyId = val;
    },
    replyText: (state, val) => {
        state.replyText = val;
    },
    replyParentId: (state, val) => {
        state.replyParentId = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
