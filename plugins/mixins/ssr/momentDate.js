import moment from 'moment';
import localization from 'moment/locale/ru';

export default {
    momentDate() {
        const self = this;
        const date = (dateString, format) => {
            const dateMoment = moment(dateString);
            if (format === 'latest') {
                if (dateMoment.isSame(moment(), 'hour')) {
                    return dateMoment.locale('ru', localization).fromNow();
                }
                if (dateMoment.isSame(moment(), 'day')) {
                    return dateMoment.locale('ru', localization).fromNow();
                    //return `сегодня в ${date.format('HH:mm')}`;
                }
                if (dateMoment.isSame(moment(), 'year')) {
                    return dateMoment.format('DD.MM HH:mm');
                }
            } else {
                if (dateMoment.isSame(moment(), 'day')) {
                    return dateMoment.format('HH:mm');
                }
                if (dateMoment.isSame(moment(), 'year')) {
                    return dateMoment.format('DD.MM HH:mm');
                }
                return dateMoment.format('DD.MM.YYYY HH:mm');
            }
            return dateMoment.format(format);
        };
        const year = (dateString) => {
            const dateMoment = moment(dateString);
            return dateMoment.format('YYYY');
        };
        const time = (dateString) => {
            const dateMoment = moment(dateString);
            return dateMoment.format('HH:mm');
        };
        const inHour = (date1, date2) => {
            const dateMoment = moment(date1);
            return dateMoment.isSame(date2, 'hour');
        };
        const latest = (dateString) => {
            const dateMoment = moment(dateString);
            if (dateMoment.isSame(moment(), 'hour')) {
                return dateMoment.locale('ru', localization).fromNow();
            }
            if (dateMoment.isSame(moment(), 'day')) {
                return dateMoment.locale('ru', localization).fromNow();
                //return `сегодня в ${date.format('HH:mm')}`;
            }
            if (dateMoment.isSame(moment(), 'month')) {
                return dateMoment.format('DD.MM HH:mm');
                //return dateMoment.locale('ru', localization).fromNow();
                //return `сегодня в ${date.format('HH:mm')}`;
            }
            if (dateMoment.isSame(moment(), 'month')) {
                return dateMoment.format('DD.MM HH:mm');
            }
            if (dateMoment.isSame(moment(), 'year')) {
                return dateMoment.format('DD.MM HH:mm');
            }
            return dateMoment.format('DD.MM.YYYY HH:mm');
        };

        const event = (dateString) => {
            const dateMoment = moment(dateString);
            return dateMoment.locale('ru', localization).format('DD MMM YYYY');
        };
        const eventRage = (dateStringStart, dateStringFinish) => {
            const dateStart = moment(dateStringStart);
            if (dateStringFinish === null) {
                if (dateStart.isSame(moment(), 'year')) {
                    return dateStart.locale('ru', localization).format('DD MMMM');
                }
                return dateStart.locale('ru', localization).format('DD MMMM YYYY');
            }
            const dateFinish = moment(dateStringFinish);
            if (!dateStart.isSame(dateFinish, 'year')) {
                return `${dateStart.locale('ru', localization).format('DD MMMM YYYY')} - ${dateFinish.locale('ru', localization).format('DD MMMM YYYY')}`;
            }
            if (!dateStart.isSame(dateFinish, 'month')) {
                const month1 = dateStart.locale('ru', localization).format('MMMM');
                const monthDeclension1 = self.$tc(`declensions.${month1}`, 2);
                const month2 = dateStart.locale('ru', localization).format('MMMM');
                const monthDeclension2 = self.$tc(`declensions.${month2}`, 2);
                return `${dateStart.locale('ru', localization).format('DD')} ${monthDeclension1} - ${dateFinish.locale('ru', localization).format('DD')} ${monthDeclension2}`;
            } else {
                const month = dateStart.locale('ru', localization).format('MMMM');
                const monthDeclension = self.$tc(`declensions.${month}`, 2);
                if (dateStart.isSame(moment(), 'year')) {
                    return `${dateStart.format('DD')}-${dateFinish.format('DD')} ${monthDeclension}`;
                }
                return `${dateStart.format('DD')}-${dateFinish.format('DD')} ${monthDeclension} ${dateStart.locale('ru', localization).format('YYYY')}`;
            }
        };
        return {
            date,
            latest,
            time,
            event,
            eventRage,
            year,
            inHour,
        };
    },
};
