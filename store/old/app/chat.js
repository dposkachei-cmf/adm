const state = () => ({
    active: null,
    heightClass: null,
});

const getters = {
    active(state) {
        return state.active;
    },
    heightClass(state) {
        return state.heightClass;
    },
};

const actions = {
    setActive({ commit }, chat) {
        commit('active', chat);
    },
    setHeightClass({ commit }, value) {
        commit('heightClass', value);
    },
};

const mutations = {
    active: (state, val) => {
        state.active = val;
    },
    heightClass: (state, val) => {
        state.heightClass = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
