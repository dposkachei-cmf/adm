import response from '../../../plugins/common/response';
import api from '../../../api';

const state = () => ({
    pending: false,
    error: false,
    data: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    error(state) {
        return state.error;
    },
};

// actions
const actions = {
    async fetchShow({ commit }, data) {
        const self = this;
        commit('pending', true);
        commit('error', false);
        commit('data', null);
        const url = api.verifications.show.replace(':token', data.token);
        await self.$axios.get(url + '?email=' + data.email)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                    commit('error', true);
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
    error: (state, data) => {
        state.error = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
