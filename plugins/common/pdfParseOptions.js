export default {
    data: {
        /**
         * Добавить 2px к результату оптимальной высоты, чтобы был запас на случай меньшего значения
         */
        heightToStability: 2,
    },

    /**
     *
     * @param node
     * @returns {*[]}
     */
    prepareNodes(node) {
        const p = [];
        node.childNodes.forEach((item) => {
            const text = item.innerText;
            const left = item.style.left !== undefined ? parseFloat(item.style.left) : -1;
            if (text !== undefined && left > 0) {
                p.push({
                    text: text,
                    width: parseInt(item.offsetWidth),
                    left: left,
                    top: parseFloat(item.style.top),
                });
            }
        });
        return p;
    },

    /**
     *
     * @param nodes
     * @returns {int|null}
     */
    getHeight(nodes) {
        if (nodes.length === 0) {
            return null;
        }
        const self = this;
        const h = self.getOptimalHeight(nodes);
        return h + self.data.heightToStability;
    },

    /**
     * Получить минимальную оптимальную высоту между строк
     *
     * @param nodes
     * @returns {int}
     */
    getOptimalHeight(nodes) {
        const self = this;
        const minimalOptimalHeight = self.getUsualMinimalHeight(nodes);
        if (minimalOptimalHeight !== null) {
            return minimalOptimalHeight;
        }
        const aHeight = self.getUsualArrayHeightByOptimalWidth(nodes);
        return self.getUsualHeight(aHeight);
    },

    /**
     * Отсортировать [52, 18, 34, 18, ...] в [18, 34, 52, ...] и получить минимальное значение
     *
     * @param nodes
     * @returns {null|int}
     */
    getUsualMinimalHeight(nodes) {
        const self = this;
        const h = self.getUsualArrayHeightByOptimalWidth(nodes);
        const hSort = h.sort(function (a, b) {
            return a - b;
        });
        // минимальное
        if (hSort[0] !== undefined) {
            return hSort[0];
        }
        return null;
    },

    /**
     * Получить самое популярное значение разницы высот
     *
     * @param aHeight
     * @returns {int}
     */
    getUsualHeight(aHeight) {
        let length = 0;
        let usualHeight = 0;
        let array = [];
        aHeight.forEach((value, key) => {
            if (array[`${value}`] === undefined) {
                array[`${value}`] = [];
            }
            array[`${value}`].push(value);

            if (array[`${value}`].length > length) {
                length = array[`${value}`].length;
                usualHeight = value;
            }
        });
        return usualHeight;
    },

    /**
     * Получить массив с разницами высоты [52, 18, 34, 18, ...]
     *
     * @param nodes
     * @returns {*[]}
     */
    getUsualArrayHeightByOptimalWidth(nodes) {
        const self = this;
        const aOptimalWidth = self.getOptimalArrayWidthNotes(nodes);
        let aHeight = [];
        let top = 0;
        nodes.forEach((value, key) => {
            if (aOptimalWidth.includes(value.width)) {
                if (top === 0) {
                    top = value.top;
                    return true;
                }
                let l;
                if (value.top > top) {
                    l = value.top - top;
                } else {
                    l = top - value.top;
                }
                l = parseInt(l);
                top = value.top;
                if (l !== 0) {
                    aHeight.push(l);
                }
            }
        });
        return aHeight;
    },


    /**
     * Отсортировать массив [10 => [[], []], 20 => [[], [], []],]
     * где у них [] = [width: ...]
     * Отнести width = 195 в массив и получить [190 => [[width: 195]]]
     * @param nodes
     * @returns {*[]}
     */
    getSortedArrayWidth(nodes) {
        const self = this;
        let aValues = self.getArrayWidthTen(nodes);
        let array = [];
        nodes.forEach((value, key) => {
            aValues.forEach((val, k) => {
                if (val < value.width && aValues[k + 1] !== undefined && aValues[k + 1] > value.width) {
                    if (array[val] === undefined) {
                        array[val] = [];
                    }
                    array[val].push(value);
                }
            });
        });
        return array;
    },

    /**
     * Из массива [[15, 15, 15], [18, 18, 18], [17, 17, 17], [16, 16, 16]]
     * Получить  [[18, 18, 18], [17, 17, 17], [16, 16, 16]]
     * С самыми популярными значениями и 3 массива
     * А потом их соеденить в одни
     * @param array
     * @returns {*[]}
     */
    getSortedArrayOptimalConcat(array) {
        const vSort = array.sort((a, b) => {
            return a.length - b.length;
        }).reverse();
        let sorted = [];
        vSort.forEach((value, key) => {
            sorted.push(value);
        });
        let optimal = [];
        optimal = optimal.concat(sorted.shift());
        optimal = optimal.concat(sorted.shift());
        optimal = optimal.concat(sorted.shift());
        return optimal;
    },

    /**
     *
     *
     * @param nodes
     * @returns {*[]}
     */
    getOptimalArrayWidthNotes(nodes) {
        const self = this;
        const sorted = self.getSortedArrayWidth(nodes);
        const optimal = self.getSortedArrayOptimalConcat(sorted);
        const aWidth = [];
        //console.log(nodes, sorted, optimal);
        optimal.forEach((value, key) => {
            if (!aWidth.includes(value.width)) {
                aWidth.push(value.width);
            }
        });
        return aWidth;
    },

    /**
     * Определить минимальную ширину объекта
     *
     * @param nodes
     * @returns {null|int}
     */
    getMinWidthOfNodes(nodes) {
        let min = null;
        nodes.forEach((value, key) => {
            if (min === null) {
                min = value.width;
            }
            if (value.width < min) {
                min = value.width;
            }
        });
        return min;
    },

    /**
     * Определить максимальную ширину объекта
     *
     * @param nodes
     * @returns {null|int}
     */
    getMaxWidthOfNodes(nodes) {
        let max = null;
        nodes.forEach((value, key) => {
            if (max === null) {
                max = value.width;
            }
            if (value.width > max) {
                max = value.width;
            }
        });
        return max;
    },

    /**
     * Получить массив по типу [10, 20, 30, ..., 200]
     * где 195 максимальная ширина объекта и к массиву прибавляется еще закрывающее значение, это 200
     *
     * @param nodes
     * @returns {*[]}
     */
    getArrayWidthTen(nodes) {
        let max = this.getMaxWidthOfNodes(nodes);
        let a = 0;
        let values = [];
        while (a < max) {
            if (a % 10 === 0) {
                values.push(a);
            }
            a++;
        }
        values.push(values[values.length - 1] + 10);
        return values;
    },
};
