import response from '../../../plugins/common/response';
import api from '../../../api';

const state = () => ({
    loaded: false,
    pending: false,
    data: {
        messages: [
            { id: 1, title: 'Поскачей Дмитрий', text: 'Lorem ipsum dolor sit amet, consectetur', date: '2018-04-17T20:00:00.000Z', loading: false, },
            { id: 2, title: 'Поскачей Дмитрий', text: 'Lorem ipsum dolor sit amet, consectetur', date: '2019-04-17T20:00:00.000Z', loading: false, },
            { id: 3, title: 'Поскачей Дмитрий', text: 'Lorem ipsum dolor sit amet, consectetur', date: '2020-04-17T20:00:00.000Z', loading: false, },
            { id: 4, title: 'Поскачей Дмитрий', text: 'Lorem ipsum dolor sit amet, consectetur', date: '2021-04-17T20:00:00.000Z', loading: false, },
            { id: 5, title: 'Поскачей Дмитрий', text: 'Lorem ipsum dolor sit amet, consectetur', date: '2021-07-19T06:00:00.000Z', loading: true, },
        ],
    },
});

const getters = {
    loaded(state) {
        return state.loaded;
    },
    data(state) {
        return state.data;
    },
    messages(state) {
        return state.data.messages;
    },
    pending(state) {
        return state.pending;
    },
};

const actions = {
    async fetchMessage({ commit, state }, data) {
        const self = this;
        commit('message', data);
    },
};

const mutations = {
    loaded: (state, val) => {
        state.loaded = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, val) => {
        state.data = val;
    },
    message: (state, message) => {
        state.data.messages.push(message);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
