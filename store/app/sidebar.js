import api from "../../api";
import axiosBase from "./../../plugins/common/axiosBase";

const state = () => ({
    current: null,
    active: null,
    disabled: false,
    names: {
        article_settings: 'sidebar-article-settings',
    },
    item: {},
    left: true,
    right: false,
    feed: {
        events: true,
        theme: 'white',
    },
    form: {
        model: null, // users, articles
        item: {},
        data: {},
        errors: {},
        fields: [],
        pending: false,
    },
});

const getters = {
    form(state) {
        return state.form;
    },
    current(state) {
        return state.current;
    },
    active(state) {
        return state.active;
    },
    names(state) {
        return state.names;
    },
    disabled(state) {
        return state.disabled;
    },
    item(state) {
        return state.item;
    },
    feed(state) {
        return state.feed;
    },
};

const actions = {
    setActive({ commit }, name) {
        commit('active', name);
    },
    setCurrent({ commit }, name) {
        commit('current', name);
    },
    setDisabled({ commit }) {
        commit('disabled', true);
    },
    setItem({ commit }, data) {
        commit('item', data);
    },
    setLeft({ commit }, data) {
        commit('left', data);
    },
    setRight({ commit }, data) {
        commit('right', data);
    },
    setFeedEvents({ commit }, value) {
        commit('feedEvents', value);
    },
    setFeedTheme({ commit }, value) {
        commit('feedTheme', value);
    },
    async fetchUpdate({ commit, state }) {
        const form = state.form;
        commit('pending', true);
        let url = api.sidebar.update;
        url = url.replace(':id', form.item.id);
        url = url.replace(':model', form.model);
        delete form.id;
        await axiosBase.init(self.$axios).post(url, form.data, function (result) {
            commit('pending', false);
        }, function () {
            commit('pending', false);
        });
    },
};

const mutations = {
    form: (state, val) => {
        state.form = val;
    },
    current: (state, val) => {
        state.current = val;
    },
    active: (state, val) => {
        state.active = val;
    },
    disabled: (state, val) => {
        state.disabled = val;
    },
    item: (state, val) => {
        state.item = val;
    },
    left: (state, val) => {
        state.left = val;
    },
    right: (state, val) => {
        state.right = val;
    },
    feedEvents: (state, val) => {
        state.feed.events = val;
    },
    feedTheme: (state, val) => {
        state.feed.theme = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
