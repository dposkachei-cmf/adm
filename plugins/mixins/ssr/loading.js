export default {
    isLoading(array) {
        const basic = (id) => array.includes(id);
        const article = (article) => array.includes('article_' + article.id);
        const notification = (notification) => array.includes(notification.id);
        const action = (item, action) => array.includes(action + '_' + item.id);
        const actionPriority = (item, action, value) => array.includes(action + '_' + value + '_' + item.id);
        return {
            basic,
            notification,
            article,
            action,
            actionPriority,
        };
    },

    /**
     *
     */
    loadingStart() {
        this.$nuxt.$loading.start();
    },

    /**
     *
     */
    loadingFinish() {
        this.$nuxt.$loading.finish();
    },

    /**
     *
     * @param value
     */
    loadingState(value) {
        value ? this.loadingStart() : this.loadingFinish();
    }
};
