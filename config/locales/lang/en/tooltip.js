export default {
    editor: {
        'preview-close': 'Закрыть предпросмотр',
        'preview-open': 'Открыть предпросмотр',
        settings: 'Открыть настройки статьи',
    },
};
