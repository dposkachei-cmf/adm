export default {

    /**
     *
     */
    modalActions() {
        const self = this;
        const open = (name) => self.$store.dispatch('app/modal/setCurrent', name);
        const closeIfOpen = () => {
            const active = self.$store.getters['app/modal/active'];
            if (active !== null) {
                close();
            }
        };
        const openPanelUploadImage = (model, item) => {
            self.$store.dispatch('app/panel/upload/image/setModel', {
                model,
                item,
            });
            open('panel-upload-image');
        };
        const openPanelPanelUploadImage = (model, item) => {
            self.$store.dispatch('app/panel/panel/upload/image/setModel', {
                model,
                item,
            });
            open('panel-panel-upload-image');
        };
        const openEditorUploadImage = () => {
            open('editor-upload-image');
        };
        const openEditorUploadVideo = () => {
            open('editor-upload-video');
        };
        const openConfirmDelete = (model, item) => {
            self.$store.dispatch('app/modal/setItem', {
                model,
                item,
            });
            open('modal-confirm-delete');
        };
        const openPanelConfirmDelete = (model, item) => {
            self.$store.dispatch('app/modal/setItem', {
                model,
                item,
            });
            open('modal-panel-confirm-delete');
        };
        const openEditorModalUnlockRelease = () => {
            open('modal-editor-unlock-release');
            self.$store.dispatch('app/modal/setDisabled', true);
        };
        const close = () => self.$store.dispatch('app/modal/setCurrent', null);
        return {
            open,
            close,
            closeIfOpen,
            openPanelUploadImage,
            openPanelPanelUploadImage,
            openConfirmDelete,
            openEditorUploadImage,
            openEditorUploadVideo,
            openPanelConfirmDelete,
            openEditorModalUnlockRelease,
        };
    },
};
