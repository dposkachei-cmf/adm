#!/usr/bin/env make

.PHONY : help swagger

.DEFAULT_GOAL := help
SRC ?= ./app
TESTS ?= ./tests
DBS ?= ./database
ROUTES ?= ./routes
OS := $(shell uname -s)
COMPOSER := $(shell which composer)
PHP := php
NPM := npm
YARN := yarn
PHPUNIT := ./vendor/bin/phpunit
PLUGINS ?= ./plugins/editorjs

.PHONY: plugins-install plugins-build app-first app-rebuild

help: ## Показать эту подсказку
	@echo "Сборка. SMS"

plugins-install: ## Генерация сваггер файла в storage/api-docs/api-docs.json
	yarn --cwd ${PLUGINS}/asterisks install
	yarn --cwd ${PLUGINS}/audio install
	yarn --cwd ${PLUGINS}/button install
	yarn --cwd ${PLUGINS}/carousel install
	yarn --cwd ${PLUGINS}/header install
	yarn --cwd ${PLUGINS}/image install
	yarn --cwd ${PLUGINS}/incut install
	yarn --cwd ${PLUGINS}/person install
	yarn --cwd ${PLUGINS}/table install
	yarn --cwd ${PLUGINS}/tooltip install
	yarn --cwd ${PLUGINS}/video install
	yarn --cwd ${PLUGINS}/warning install

plugins-build: ## Генерация сваггер файла в storage/api-docs/api-docs.json
	yarn --cwd ${PLUGINS}/asterisks build
	yarn --cwd ${PLUGINS}/audio build
	yarn --cwd ${PLUGINS}/button build
	yarn --cwd ${PLUGINS}/carousel build
	yarn --cwd ${PLUGINS}/header build
	yarn --cwd ${PLUGINS}/image build
	yarn --cwd ${PLUGINS}/incut build
	yarn --cwd ${PLUGINS}/person build
	yarn --cwd ${PLUGINS}/table build
	yarn --cwd ${PLUGINS}/tooltip build
	yarn --cwd ${PLUGINS}/video build
	yarn --cwd ${PLUGINS}/warning build

app-first: ## Запустить контейнер
	make plugins-install
	make plugins-build
	yarn build

app-rebuild:
	cp ./nodedock/nginx/maintenance/maintenance.file.example ./nodedock/nginx/maintenance/maintenance.file
	yarn build
	supervisorctl restart artagrad-nuxt
	rm -f ./nodedock/nginx/maintenance/maintenance.file

---------------: ## ---------------
