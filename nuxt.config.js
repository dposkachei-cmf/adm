import dotenv from 'dotenv';
import i18n from './config/locales';

dotenv.config();

export default {
    server: {
        host: process.env.APP_SERVER_HOST,
        port: process.env.APP_SERVER_PORT,
    },
    modern: process.env.NODE_ENV !== 'development',
    target: 'server',
    ssr: false,
    components: false,
    buildDir: 'nuxt-dist',
    build: {
        //cache: true,
        extractCSS: true,
        //minimize: true,
        // minimizer: [
        //     'terser-webpack-plugin'
        // ],
        analyze: process.env.NODE_ENV === 'development',
        cssSourceMap: process.env.NODE_ENV === 'development',
        html: {
            minify: {
                collapseBooleanAttributes: true,
                decodeEntities: true,
                minifyCSS: process.env.NODE_ENV === 'production',
                minifyJS: process.env.NODE_ENV === 'production',
                processConditionalComments: true,
                removeEmptyAttributes: true,
                removeRedundantAttributes: true,
                trimCustomFragments: true,
                useShortDoctype: true
            }
        },
        optimization: {
            minimize: process.env.NODE_ENV === 'production',
        },
        postcss: {
            plugins: {
                // cssnano: {
                //     preset: 'default',
                // }
            },
        },
        preset: {
            autoprefixer: {
                flexbox: true,
                grid: true,
                overrideBrowserslist: ['last 3 versions', '> 1%', 'ie 8', 'ie 7'],
            }
        }
    },
    router: {
        middleware: [
            'data',
            'user',
            //'authenticate',
        ]
    },
    generate: {
        routes: [
            //'/personal',
            //'/personal/settings',
            //'/personal/settings/account',
        ]
    },

    head: {
        title: process.env.APP_NAME,
        titleTemplate: `%s - ${process.env.APP_NAME}`,
    },
    loading: {
        color: 'blue',
        height: '1px'
    },
    render: {
        bundleRenderer: {
            shouldPrefetch: (file, type) => {
                console.log('shouldPrefetch');
                console.log(type);
                console.log(file);
                if (type === 'script') {
                    if (/admin/.test(file)) {
                        return false;
                    }
                }
                return true;
            }
        }
    },
    css: [
        '@/assets/scss/app/app.scss',
    ],
    plugins: [
        '~plugins/axios.js',
        '~plugins/nuxt-i18n.js',
        '~plugins/vue-font-awesome.js',
        '~plugins/vue-font-regular-awesome.js',
        '~plugins/vue-multiselect.js',
        '~plugins/vue-components.js',
        '~plugins/vue-mixins-ssr.js',
        '~plugins/vue-js-modal.js',
        '~plugins/i18n.js',
        '~plugins/vuebar.js',
        //{ src: '~plugins/vue-js-modal.js', mode: 'client' },
        //{ src: '~plugins/vue-slick-carousel.js', mode: 'client' },
        //{ src: '~plugins/vue-js-modal.js', mode: 'client' },
        //{ src: '~plugins/vue-flickity.js', mode: 'client' },
        //{ src: '~plugins/vue-gallery.js', mode: 'client' },
        //{ src: '~plugins/vue-read-more.js', mode: 'client' },
        { src: '~plugins/vue-editor.js', mode: 'client' },
        { src: '~plugins/vue-tippy.js', mode: 'client' },
        //{ src: '~plugins/vue-snip.js', mode: 'client' },
        { src: '~plugins/vue-js-toggle-button.js', mode: 'client' },
        { src: '~plugins/vue2-datepicker.js', mode: 'client' },
        { src: '~plugins/vue-cleave-directive.js', mode: 'client' },
        { src: '~plugins/vue-scrollto.js', mode: 'client' },
        { src: '~plugins/vue-mixins-client.js', mode: 'client' },
        { src: '~plugins/vue-social-sharing.js', mode: 'client' },
        { src: '~plugins/vue-plyr.js', mode: 'client' },
    ],
    buildModules: [
        //'@nuxtjs/router',
        '@nuxtjs/dotenv',
        //'@nuxtjs/svg-sprite',
        '@nuxt/image',
        '@nuxtjs/device',
        ['@nuxtjs/html-validator', {
            usePrettier: false,
            failOnError: false,
            options: {
                extends: [
                    'html-validate:document',
                    'html-validate:recommended',
                    'html-validate:standard'
                ],
                rules: {
                    'svg-focusable': 'off',
                    'no-unknown-elements': 'off',
                    // Conflicts or not needed as we use prettier formatting
                    'void-style': 'off',
                    'element-case': 'off',
                    'element-name': 'off',
                    'no-trailing-whitespace': 'off',
                    // Conflict with Nuxt defaults
                    'require-sri': 'off',
                    'attribute-boolean-style': 'off',
                    'doctype-style': 'off',
                    // Unreasonable rule
                    'no-inline-style': 'off'
                }
            }
        }],
        //'nuxt-compress',
        ['@nuxtjs/google-analytics', {
            id: process.env.GOOGLE_ANALYTICS_ENABLED === 'true' ? process.env.GOOGLE_ANALYTICS_ID : '',
            debug: {
                enabled: process.env.GOOGLE_ANALYTICS_DEBUG === 'true',
            },
            dev: process.env.GOOGLE_ANALYTICS_DEV === 'true',
            checkDuplicatedScript: false,
            disableScriptLoader: false,
        }],
        ['nuxt-compress', {
            gzip: {
                threshold: 8192,
            },
            brotli: {
                threshold: 8192,
            },
        }],
    ],
    modules: [
        ['bootstrap-vue/nuxt', {
            bootstrapVue: {
                bootstrapCSS: false, // Or `css: false`
                bootstrapVueCSS: false // Or `bvCSS: false`
            },
            componentPlugins: [
                'ButtonPlugin',
                'CollapsePlugin',
                'SkeletonPlugin',
                'PopoverPlugin',
                'SidebarPlugin',
                'FormSelectPlugin',
                'FormTextareaPlugin',
                'FormFilePlugin',
                'FormGroupPlugin',
                'FormCheckboxPlugin',
                'FormRadioPlugin',
                'CardPlugin',
                'TabsPlugin',
                'PaginationNavPlugin',
                'DropdownPlugin',
                'ToastPlugin',
                //'BBreadcrumb',
            ],
            directivePlugins: [
                //'VBPopoverPlugin', 'VBTooltipPlugin', 'VBScrollspyPlugin',
            ]
        }],
        // Doc: https://github.com/nuxt-community/style-resources-module
        '@nuxtjs/style-resources',
        //'@nuxtjs/moment',
        '@nuxtjs/pwa',
        '@nuxtjs/sentry',

        '@nuxtjs/axios',
        '@nuxtjs/auth-next',
        ['nuxt-i18n', i18n],
        '@nuxtjs/toast',
        '@nuxtjs/svg',
        ['@nuxtjs/markdownit', {
            html: true,
            linkify: true,
            runtime: true // Support `$md()`
        }],
        ['@nuxtjs/laravel-echo', {
            plugins: ['~/plugins/echo.js'],
        }],
        // With options
        ['cookie-universal-nuxt', {
            //
        }],
        ['@nuxtjs/yandex-metrika', {
            id: process.env.YANDEX_METRIKA_ENABLED === 'true' ? process.env.YANDEX_METRIKA_ID : '',
            dev: process.env.YANDEX_METRIKA_ENABLED !== 'true',
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true,
            webvisor: true,
            // useCDN:false,
        }],
        ['@nuxtjs/recaptcha', {
            hideBadge: true,
            siteKey: process.env.GOOGLE_RECAPTCHA_ENABLED === 'true' ? process.env.GOOGLE_RECAPTCHA_KEY : '',
            version: 3,
        }],
        '@nuxtjs/webpack-profile',
        ['vue2-editor/nuxt'],
    ],
    firebase: {
        // options
    },
    toast: {
        position: 'top-right',
        keepOnHover: true,
        duration: 2000,
        className: 'toasted-styled',
        containerClass: 'container-toasted-styled',
    },
    axios: {
        credentials: false,
        baseURL: process.env.API_URL,
        browserBaseURL: process.env.API_URL,
        debug: process.env.APP_ENV !== 'production',
    },
    styleResources: {
        scss: './scss/*.scss'
    },
    auth: {
        redirect: {
            home: false,
        },
        resetOnError: true,
        cookie: {
            prefix: 'auth.',
            options: {
                path: '/',
                maxAge: 60 * 60 * 24 * 30,
            },
        },
        strategies: {
            local: {
                endpoints: {
                    login: { url: '/auth/login', method: 'post' },
                    user: { url: '/user', method: 'get', propertyName: false },
                    logout: { url: '/auth/logout', method: 'post' },
                },
                refreshToken: {
                    property: 'refresh_token',
                    data: 'refresh_token',
                    maxAge: 60 * 60 * 24 * 32,
                },
                token: {
                    property: 'data.token',
                    maxAge: 60 * 60 * 24 * 30,
                },
                user: {
                    property: 'data',
                    autoFetch: true,
                },
            },
        },
        plugins: [
            //'~/plugins/auth/user.js'
        ]
    },
    pwa: {
        workbox: {
            cacheOptions: {
                revision: process.env.APP_REVISION,
            },
            cleanupOutdatedCaches: true,
        },
        manifest: {
            name: process.env.APP_FULL_NAME,
            short_name: process.env.APP_NAME,
            description: process.env.APP_DESCRIPTION,
            background_color: '#006fca',
            lang: 'ru',
            crossorigin: 'use-credentials'
        },
        icon: false,
    },
    sentry: {
        dsn: process.env.SENTRY_DSN,
        config: {
            environment: process.env.APP_ENV,
        },
        disabled: process.env.SENTRY_ENABLED === 'false',
    }
};
