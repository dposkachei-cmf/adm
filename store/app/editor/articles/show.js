import api from './../../../../api';
import beforeFetch from "../../../../plugins/common/beforeFetch";
import axiosStore from "../../../../plugins/common/axiosStore";

const state = () => ({
    pending: false,
    loaded: false,
    data: null,
    model: null,
    locale: 'ru',
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    id(state) {
        return state.id;
    },
    model(state) {
        return state.model;
    },
    pending(state) {
        return state.pending;
    },
    loaded(state) {
        return state.loaded;
    },
    locale(state) {
        return state.locale;
    },
};

// actions
const actions = {
    async fetchData({ commit }, data) {
        const self = this;
        commit('pending', true);
        commit('data', null);
        commit('model', data.model);
        //commit('modelId', data.id);
        commit('id', data.id);
        const url = beforeFetch.getUrlModelId(api.editor.article.show, data.model, data.id);
        const query = {};
        if (data.locale !== undefined) {
            query.locale = data.locale;
            commit('locale', data.locale);
        }
        await axiosStore.init(self.$axios).get(url, query, function (result) {
            commit('pending', false);
            commit('data', result.data);
            self.dispatch('app/editor/articles/store/setFormDefault', {
                id: result.data.id,
                image: result.data.image,
                type: result.data.type,
                title: result.data.title,
                preview_description: result.data.preview_description,
                //rubric: result.data.rubric,
                //access_by: result.data.access_by,
                //udk: result.data.udk,
                //doi: result.data.doi,
                //keywords: result.data.keywords,
                status: result.data.status,
                //pages: result.data.pages,
            });
            // if (result.data.source.type === 'pdf') {
            //     self.dispatch('app/editor/pdf/setUrl', result.data.source.url);
            //     if (result.data.source.pages !== null) {
            //         self.dispatch('app/editor/pdf/setShowPages', result.data.source.pages);
            //         self.dispatch('app/editor/pdf/setPage', result.data.source.pages[0]);
            //     } else {
            //         self.dispatch('app/editor/pdf/setShowPages', null);
            //     }
            //     //self.dispatch('app/editor/base/setModePdf');
            // }
            // if (result.data.source.type === 'web') {
            //     self.dispatch('app/editor/iframe/setWebUrl', result.data.source.url);
            //     //self.dispatch('app/editor/base/setModeIframe');
            // }
            self.dispatch('app/editor/base/setBlocks', result.data.blocks);
            //self.dispatch('app/editor/articles/store/setFields', result.fields);
            commit('loaded', true);
        }, function () {
            commit('pending', false);
        });
    },
    async setData({ commit }, data) {
        commit('data', data);
    },
    async setLoaded({ commit }, val) {
        commit('loaded', val);
    },
    async reloadItem({ commit, state }) {
        const self = this;
        const url = beforeFetch.getUrlModelId(api.editor.articles.show, state.model, state.id);
        const query = {};
        query.locale = state.locale;
        await axiosStore.init(self.$axios).get(url, query, function (result) {
            commit('data', result.data);
        });
    }
};

// mutations
const mutations = {
    loaded: (state, val) => {
        state.loaded = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
    model: (state, val) => {
        state.model = val;
    },
    id: (state, val) => {
        state.id = val;
    },
    locale: (state, val) => {
        state.locale = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
