export default {
    feed: {
        setting: {
            title: 'Настроить ленту',
        }
    },
    editor: {
        settings: {
            title: 'Настройки статьи',
        }
    },
    group: {
        invite: {
            title: 'Приглашение в сообщество',
            success: 'Вы успешно добавлены в сообщество',
            error: 'Ошибка приглашения в сообщество',
            'link-text': 'Подробности на странице',
        }
    },
    user: {
        report: {
            title: 'Опишите замечание',
        }
    }
};
