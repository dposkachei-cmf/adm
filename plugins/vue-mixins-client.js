import Vue from 'vue';
import cleave from './mixins/client/cleave';
import modal from './mixins/client/modal';
import sidebar from './mixins/client/sidebar';
import auth from './mixins/client/auth';
import message from './mixins/client/message';
import echo from './mixins/client/echo';
import cropperActions from './mixins/client/cropperActions';
import editor from './mixins/client/editor';
import scroll from './mixins/client/scroll';

Vue.mixin({
    methods: cleave,
});

Vue.mixin({
    methods: modal,
});
Vue.mixin({
    methods: sidebar,
});

Vue.mixin({
    methods: auth,
});

Vue.mixin({
    methods: message,
});

Vue.mixin({
    methods: echo,
});

Vue.mixin({
    methods: cropperActions,
});

Vue.mixin({
    methods: editor,
});

Vue.mixin({
    methods: scroll,
});
