const state = () => ({
    current: null,
    active: null,
    disabled: false,
    names: {
        article_settings: 'sidebar-article-settings',
    },
    item: {},
    left: true,
    right: false,
    feed: {
        events: true,
        theme: 'white',
    },
});

const getters = {
    current(state) {
        return state.current;
    },
    active(state) {
        return state.active;
    },
    names(state) {
        return state.names;
    },
    disabled(state) {
        return state.disabled;
    },
    item(state) {
        return state.item;
    },
    feed(state) {
        return state.feed;
    },
};

const actions = {
    setActive({ commit }, name) {
        commit('active', name);
    },
    setCurrent({ commit }, name) {
        commit('current', name);
    },
    setDisabled({ commit }) {
        commit('disabled', true);
    },
    setItem({ commit }, data) {
        commit('item', data);
    },
    setLeft({ commit }, data) {
        commit('left', data);
    },
    setRight({ commit }, data) {
        commit('right', data);
    },
    setFeedEvents({ commit }, value) {
        commit('feedEvents', value);
    },
    setFeedTheme({ commit }, value) {
        commit('feedTheme', value);
    }
};

const mutations = {
    current: (state, val) => {
        state.current = val;
    },
    active: (state, val) => {
        state.active = val;
    },
    disabled: (state, val) => {
        state.disabled = val;
    },
    item: (state, val) => {
        state.item = val;
    },
    left: (state, val) => {
        state.left = val;
    },
    right: (state, val) => {
        state.right = val;
    },
    feedEvents: (state, val) => {
        state.feed.events = val;
    },
    feedTheme: (state, val) => {
        state.feed.theme = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
