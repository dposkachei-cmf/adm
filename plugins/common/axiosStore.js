//import axios from "./../../plugins/axios";
import serialize from "./serialize";
import response from "./response";

export default {

    /**
     *
     */
    axios: null,

    /**
     *
     * @param axios
     */
    init(axios) {
        this.axios = axios;
        return this;
    },

    /**
     *
     * @param {string} url
     * @param {object} query
     * @param {function} successCallback
     * @param {function} errorCallback
     * @returns {Promise<void>}
     */
    async get(url, query, successCallback, errorCallback) {
        await this.axios.get(url + '?' + serialize.query(query))
            .then((res) => {
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                successCallback(result);
            })
            .catch((error) => {
                if (error) {
                    const result = response.error(error);
                    errorCallback(result);
                }
            });
    },

    /**
     *
     * @param {string} url
     * @param {object} query
     * @param {function} successCallback
     * @param {function} errorCallback
     * @returns {Promise<void>}
     */
    async post(url, query, successCallback, errorCallback) {
        await this.axios.post(url, query)
            .then((res) => {
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                successCallback(result);
            })
            .catch((error) => {
                if (error) {
                    const result = response.error(error);
                    errorCallback(result);
                }
            });
    },

    /**
     *
     * @param {string} url
     * @param {object} query
     * @param {function} successCallback
     * @param {function} errorCallback
     * @returns {Promise<void>}
     */
    async delete(url, query, successCallback, errorCallback) {
        await this.axios.delete(url + '?' + serialize.query(query))
            .then((res) => {
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                successCallback(result);
            })
            .catch((error) => {
                if (error) {
                    const result = response.error(error);
                    errorCallback(result);
                }
            });
    },
};
