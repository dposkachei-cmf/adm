# SMS Frontend

Перейти в директорию нодедока:
```shell
cd nodedock
```

Скопировать конфиг
```shell
cp .env.example .env
```

Запустить билд контейнеров, которые в docker-compose.yml
```shell
docker-compose build
```

Запуск этих контейнеров
```shell
docker-compose up -d
```

Если был первый запуск, то сначала установить записимости и потом выполнить
```shell
docker-compose restart node
```

Этого достаточно чтобы заработал базовый функционал, билд автоматически начнется

Дальше настройка проекта, так же в директории nodedock выполнить

```shell
docker-compose exec --user=nodedock workspace bash
cd /var/www - здесь находится сам проект в контейнере
```

Установить зависимости
```shell
yarn install
```

Скопировать конфиг
```shell
cp .env.example .env
```
