import Vue from 'vue';

import { config, library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
    faSearch,
    faHome,
    faCog,
    faUsers,
    faUser,
    faBell,
    faChevronLeft,
    faChevronRight,
    faPlus,
    faTimes,
    faPencilAlt,
    faHighlighter,
    faTrash,
    faBan,
    faEllipsisH,
    faCheck,
    faEye,
    faEyeSlash,
    faSave,
    faFileUpload,
    faLock,
    faThLarge,
    faChevronUp,
    faChevronDown,
    faUserSecret,
    faUserCog,
    faBook,
    faSort,
    faCaretUp,
    faCaretDown,
    faSyncAlt,
    faFile,
    faTools,
    faQuestionCircle,
    faUnlink,
    faCheckDouble,
    faImage,
    faSearchMinus,
    faSearchPlus,
    faFileImport,
    faFilePdf,
    faFileWord,
    faGlobeAmericas,
    faCogs,
    faAnchor,
    faNewspaper,
    faColumns,
    faSquare,
    faComments,
    faCircle,
    faVideo,
    faHammer,
    faUndo,
    faBorderAll,
    faTags,
    faUserNinja,
    faHistory,
} from '@fortawesome/free-solid-svg-icons';

import {
    faBold,
    faItalic,
    faQuoteRight,
    faCode,
    faLink,
    faListOl,
    faListUl,
    faTable,
    faTasks,
    faFileImage,
} from '@fortawesome/free-solid-svg-icons';
library.add(faBold);
library.add(faItalic);
library.add(faQuoteRight);
library.add(faCode);
library.add(faLink);
library.add(faListOl);
library.add(faListUl);
library.add(faTable);
library.add(faTasks);
library.add(faFileImage);

import {
    faCommentDots,
} from '@fortawesome/free-regular-svg-icons';
import {
    faFacebookF,
    //faTwitter,
    //faOrcid,
    faVk,
    faAutoprefixer,
    //faWhatsapp,
    //faTelegramPlane,
    //faOdnoklassniki,
    //faInstagram,
    faGoogle,
} from '@fortawesome/free-brands-svg-icons';

config.autoAddCss = false;

library.add(faHome);
library.add(faBell);
library.add(faSearch);
library.add(faCog);
library.add(faChevronLeft);
library.add(faChevronRight);
library.add(faUsers);
library.add(faPlus);
library.add(faTimes);
library.add(faPencilAlt);
library.add(faHighlighter);
library.add(faTrash);
library.add(faEllipsisH);
library.add(faUser);
library.add(faBan);
library.add(faCheck);
library.add(faLock);
library.add(faCircle);

library.add(faEyeSlash);
library.add(faSave);
library.add(faFileUpload);
library.add(faAutoprefixer);
library.add(faUserSecret);
library.add(faUserCog);
library.add(faSort);
library.add(faCaretUp);
library.add(faCaretDown);
library.add(faSyncAlt);
library.add(faFile);
library.add(faTools);
library.add(faImage);
library.add(faVideo);
library.add(faHammer);

library.add(faFacebookF);
library.add(faVk);
library.add(faGoogle);
library.add(faEye);
library.add(faThLarge);
library.add(faChevronUp);
library.add(faChevronDown);
library.add(faBook);
library.add(faCommentDots);
library.add(faQuestionCircle);
library.add(faUnlink);
library.add(faCheckDouble);
library.add(faSearchMinus);
library.add(faSearchPlus);
library.add(faFileImport);
library.add(faFilePdf);
library.add(faFileWord);
library.add(faGlobeAmericas);
library.add(faCogs);
library.add(faAnchor);
library.add(faNewspaper);
library.add(faColumns);
library.add(faSquare);
library.add(faComments);
library.add(faUndo);
library.add(faBorderAll);
library.add(faTags);
library.add(faUserNinja);
library.add(faHistory);

Vue.component('FontAwesomeIcon', FontAwesomeIcon);
