import response from '../../../../plugins/common/response';
import api from '../../../../api';

const state = () => ({
    withPending: true,
    pending: false,
    pendingChildren: false,
    loaded: false,
    data: [],
    pagination: null,
    parentId: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    withPending(state) {
        return state.withPending;
    },
    loaded(state) {
        return state.loaded;
    },
    pending(state) {
        return state.pending;
    },
    pendingChildren(state) {
        return state.pendingChildren;
    },
    pagination(state) {
        return state.pagination;
    },
    parentId(state) {
        return state.parentId;
    },
};

// actions
const actions = {
    async fetchComments({ commit, state }, data) {
        const self = this;
        if (state.withPending) {
            commit('pending', true);
        }
        let url = api.articles.comments.replace(':id', data.id);
        if (data.page !== undefined) {
            url += '?page=' + data.page;
        }
        await self.$axios.get(url)
            .then((res) => {
                if (state.withPending) {
                    commit('pending', false);
                }
                if (res.status !== 200) {
                    return;
                }
                if (!state.loaded) {
                    commit('loaded', true);
                }
                const result = response.success(res);
                if (state.withPending) {
                    commit('pending', false);
                    commit('data', result.data);
                } else {
                    commit('dataReplace', result.data);
                    commit('withPending', true);
                }
                commit('pagination', result.pagination);
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    if (state.withPending) {
                        commit('pending', false);
                    }
                }
            });
    },
    async fetchChildrenComments({ commit, state }, data) {
        const self = this;
        commit('pendingChildren', true);
        commit('parentId', data.parent_id);
        let url = api.articles.comments.replace(':id', data.id);
        url += '?parent_id=' + data.parent_id;
        if (data.page !== undefined) {
            url += '&page=' + data.page;
        }
        await self.$axios.get(url)
            .then((res) => {
                commit('pendingChildren', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('dataChildren', result.data);
                commit('paginationChildren', result.pagination);
                commit('parentId', null);
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pendingChildren', false);
                }
            });
    },
    async fetchCommentsInitial({ commit, state }, data) {
        const self = this;
        commit('data', []);
        commit('loaded', false);
        await self.dispatch('article/comments/fetchComments', data);
    },
    async fetchCommentsInitialWithoutPending({ commit, state }, data) {
        const self = this;
        commit('withPending', false);
        await self.dispatch('article/comments/fetchComments', data);
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    pendingChildren: (state, val) => {
        state.pendingChildren = val;
    },
    data: (state, data) => {
        if (data.length === 0) {
            state.data = [];
        } else {
            data.forEach(function (value) {
                state.data.push(value);
            });
        }
    },
    dataReplace: (state, data) => {
        state.data = data;
    },
    dataChildren: (state, data) => {
        const parent = state.data.find(x => x.id === state.parentId);
        console.log(parent);
        if (data.length === 0) {
            //state.data = [];
        } else {
            data.forEach(function (value) {
                parent.children.data.push(value);
            });
        }
    },
    loaded: (state, val) => {
        state.loaded = val;
    },
    parentId: (state, val) => {
        state.parentId = val;
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
    withPending: (state, val) => {
        state.withPending = val;
    },
    paginationChildren: (state, data) => {
        const parent = state.data.find(x => x.id === state.parentId);
        console.log(parent);
        parent.children.pagination = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
