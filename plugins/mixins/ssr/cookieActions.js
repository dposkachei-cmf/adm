export default {

    cookieBase() {
        return {
            sidebar: {
                left: true,
                right: false,
            },
            editor: {
                preview: false,
                mode: 'preview',
            },
            mode: {
                developer: false,
            }
        };
    },
    /**
     *
     * @param app
     * @returns {{set: (function(*=): void), initial: ((function(): (*))|*), get: (function(): any), hasInitial: (function(): boolean)}}
     */
    cookieMiddleware(app) {
        const baseName = 'app.base';
        const set = (value) => app.$cookies.set(`${baseName}`, value, {
            path: '/',
            maxAge: 60 * 60 * 24 * 7
        });
        const get = () => {
            return app.$cookies.get(`${baseName}`, {
                parseJSON: true,
            });
        };
        const hasInitial = () => {
            return get(baseName) !== undefined;
        };
        const initial = () => {
            const object1 = get(baseName);
            if (object1 !== undefined && checkKeys(object1)) {
                return object1;
            } else {
                set(this.cookieBase());
                return this.cookieBase();
            }
        };
        const checkKeys = (object) => {
            if (object === null) {
                return false;
            }
            if (object.sidebar === undefined) {
                return false;
            }
            if (object.editor === undefined) {
                return false;
            }
            if (object.editor.mode === undefined) {
                return false;
            }
            if (object.mode === undefined) {
                return false;
            }
            if (object.mode.developer === undefined) {
                return false;
            }
            return true;
        };
        return {
            get,
            set,
            initial,
            hasInitial,
        };
    },

    /**
     *
     * @returns {{token: (function(): any)}}
     */
    cookieAuth() {
        const self = this;
        const token = () => self.$cookies.get(`auth._token.local`);
        const hasToken = () => token() !== null;
        return {
            token,
            hasToken,
        };
    },

    /**
     *
     */
    cookieActions() {
        const self = this;
        const baseName = 'app.base';
        const set = (value) => self.$cookies.set(`${baseName}`, value, {
            path: '/',
            maxAge: 60 * 60 * 24 * 7
        });
        const get = () => {
            return Object.assign({}, this.cookieBase(), self.$cookies.get(`${baseName}`, {
                parseJSON: true,
            }));
        };
        const initial = () => {
            set(this.cookieBase());
        };
        const setAppLeftBar = (value) => {
            const object = get(baseName);
            object.sidebar.left = value;
            set(object);
        };
        const getAppLeftBar = () => {
            return get(baseName).sidebar.left;
        };
        const setAppRightBar = (value) => {
            const object = get(baseName);
            object.sidebar.right = value;
            set(object);
        };
        const getAppRightBar = () => {
            return get(baseName).sidebar.right;
        };
        const setAppModeDeveloper = (value) => {
            const object = get(baseName);
            object.mode.developer = value;
            set(object);
        };
        const getAppModeDeveloper = () => {
            return get(baseName).mode.developer;
        };
        const setAppEditorPreview = (value) => {
            const object = get(baseName);
            object.editor.preview = value;
            set(object);
        };
        const getAppEditorPreview = () => {
            return get(baseName).editor.preview;
        };
        const setAppEditorMode = (value) => {
            const object = get(baseName);
            object.editor.mode = value;
            set(object);
        };
        const getAppEditorMode = () => {
            return get(baseName).editor.mode;
        };
        const commitInitial = (store, cookieApp) => {
            store.commit('app/sidebar/left', cookieApp.sidebar.left);
            store.commit('app/sidebar/right', cookieApp.sidebar.right);
        };
        return {
            set,
            setAppLeftBar,
            getAppLeftBar,
            setAppRightBar,
            getAppRightBar,
            initial,
            commitInitial,
            setAppEditorPreview,
            getAppEditorPreview,
            setAppEditorMode,
            getAppEditorMode,
            setAppModeDeveloper,
            getAppModeDeveloper,
        };
    }
};
