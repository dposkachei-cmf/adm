import response from '../../../../plugins/common/response';
import api from '../../../../api';
import serialize from '../../../../plugins/common/serialize';

const state = () => ({
    pending: false,
    errors: {},
});

const getters = {
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
};

const actions = {
    async fetchLoginMock({ commit }, data) {
        const self = this;
        commit('pending', true);
        commit('errors', {});
        let url = api.auth.socialite.mock;
        url += '?' + serialize.query(data);
        await self.$axios.get(url)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                if (result.success && result.message !== undefined) {
                    self.$toast.success(result.message);
                }
                if (result.data.token !== undefined) {
                    self.$auth.setUserToken(result.data.token).then(() => {
                        window.location.href = self.$router.history.current.path;
                    });
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                    if (result.message !== undefined) {
                        self.$toast.error(result.message, {
                            duration: 5000,
                        });
                    }
                }
            });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
