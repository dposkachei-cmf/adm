import response from '../../../plugins/common/response';
import api from '../../../api';

const state = () => ({
    pending: false,
    data: {},
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
};

// actions
const actions = {
    async fetchCounters({ commit }, data) {
        const self = this;
        commit('pending', true);
        const url = api.categories.counters.replace(':id', data.id);
        await self.$axios.get(url)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                self.dispatch('categories/show/setCounters', result.data);
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
