import moment from 'moment';

export default {
    query(params, prefix) {
        const self = this;
        const query = Object.keys(params).map((key, number) => {
            let value = params[key];
            if (params.constructor === Array) {
                key = `${prefix}[${number}]`;
            } else if (params.constructor === Object) {
                key = (prefix ? `${prefix}[${key}]` : key);
            } else if (params.constructor === Boolean) {
                value = value ? 1 : 0;
            }
            if (prefix !== undefined && prefix.endsWith('_rg')) {
                value = moment(value).format('YYYY-MM-DD');
            }
            if (value === null) {
                return '';
            }
            //console.log(value);
            if (typeof value === 'object') {
                return self.query(value, key);
            } else {
                let encodeValue = encodeURIComponent(value);
                //console.log(encodeValue);
                // if (encodeValue === 'false' || encodeValue === 'true') {
                //     encodeValue = encodeValue === 'true' ? 1 : 0;
                // }
                return `${key}=${encodeValue}`;
            }
        });

        return [].concat.apply([], query).join('&');
    }
};
