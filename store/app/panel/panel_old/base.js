import api from "../../../../api";
import axios from "./../../../../plugins/axios";
import response from "../../../../plugins/common/response";
import serialize from "../../../../plugins/common/serialize";

const state = () => ({
    opened: false,
    model: null, // users, articles
    item: null, // {}
    role: null, // edit, create, show
    tabActive: {
        key: 0,
    },
    form: {},
    errors: {},
    fields: null, // []
    fieldsLoading: false, // []
    pending: false,
    loading: false,
    withoutLoading: false,
    locale: 'ru',
});

const getters = {
    opened(state) {
        return state.opened;
    },
    role(state) {
        return state.role;
    },
    model(state) {
        return state.model;
    },
    item(state) {
        return state.item;
    },
    form(state) {
        return state.form;
    },
    errors(state) {
        return state.errors;
    },
    fields(state) {
        return state.fields;
    },
    fieldsLoading(state) {
        return state.fieldsLoading;
    },
    pending(state) {
        return state.pending;
    },
    loading(state) {
        return state.loading;
    },
    withoutLoading(state) {
        return state.withoutLoading;
    },
    tabActive(state) {
        return state.tabActive;
    },
    locale(state) {
        return state.locale;
    },
};

const actions = {
    async fetchUpdate({ commit, state }, callback) {
        const self = this;
        const item = state.item;
        const form = state.form;
        commit('pending', true);
        let url = api.panel.update;
        url = url.replace(':id', item.id);
        url = url.replace(':model', state.model);
        const save = serialize.panelForm(serialize.panelFormByFields(form, state.fields.tabs));
        save.locale = state.locale;
        await axios.post(url, save)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                if (result.success && result.message !== undefined) {
                    self.$toast.success(result.message);
                }
                if (result.success && result.message !== undefined) {
                    //self.dispatch('app/message/setSuccess', result.message);
                }
                callback();
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                }
            });
    },
    async fetchStore({ commit, state }, callback) {
        const self = this;
        const form = state.form;
        commit('pending', true);
        commit('errors', {});
        let url = api.panel.store;
        url = url.replace(':model', state.model);
        const save = serialize.panelForm(serialize.panelFormByFields(form, state.fields.tabs));
        save.locale = state.locale;
        await axios.post(url, save)
            .then((res) => {
                commit('pending', false);
                console.log(res);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                if (result.success && result.message !== undefined) {
                    self.$toast.success(result.message);
                }
                callback();
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                }
            });
    },
    async fetchShow({ commit, state }, data) {
        const self = this;
        commit('clear');
        commit('loading', true);
        commit('model', data.model);
        commit('form', {});
        let url = api.panel.show;
        url = url.replace(':id', data.id);
        url = url.replace(':model', data.model);
        if (state.locale !== 'ru') {
            url += '?locale=' + state.locale;
        }
        await axios.get(url)
            .then((res) => {
                commit('loading', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('item', result.data);
                commit('formMerge', {
                    id: result.data.id,
                });
                commit('fields', result.fields);
                self.dispatch('app/panel/panel/base/setTabActiveIndex', 0);
            })
            .catch((error) => {
                if (error) {
                    commit('loading', false);
                    const result = response.error(error);
                }
            });
    },
    async fetchEdit({ commit, state }, data) {
        const self = this;
        commit('clear');
        commit('loading', true);
        commit('model', data.model);
        commit('form', {});
        let url = api.panel.edit;
        url = url.replace(':id', data.id);
        url = url.replace(':model', data.model);
        let query = {};
        if (data.tab !== undefined) {
            query.tab = data.tab;
        }
        if (data.locale !== undefined) {
            commit('locale', data.locale);
            query.locale = data.locale;
        }
        await axios.get(url + '?' + serialize.query(query))
            .then((res) => {
                commit('loading', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('item', result.data);
                commit('formMerge', {
                    id: result.data.id,
                });
                commit('fields', result.fields);
                self.dispatch('app/panel/panel/base/setTabActiveIndex', 0);
            })
            .catch((error) => {
                if (error) {
                    commit('loading', false);
                    const result = response.error(error);
                }
            });
    },
    async fetchReloadItem({ commit, state }) {
        const self = this;
        let url = api.panel.show;
        url = url.replace(':id', state.item.id);
        url = url.replace(':model', state.model);
        if (state.locale !== 'ru') {
            url += '?locale=' + state.locale;
        }
        await axios.get(url)
            .then((res) => {
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('item', result.data);
            })
            .catch((error) => {
                if (error) {
                    const result = response.error(error);
                }
            });
    },
    async fetchReloadFields({ commit, state }) {
        const self = this;
        commit('fieldsLoading', true);
        let url = api.panel.fields;
        url = url.replace(':model', state.model);
        const request = serialize.query(serialize.requestNormalize(state.form));
        await axios.get(url + '?' + request)
            .then((res) => {
                commit('fieldsLoading', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('fields', result.fields);
                result.fields.tabs.forEach((tab) => {
                    tab.data.forEach((item) => {
                        if (item.value !== undefined) {
                            state.form[item.key] = item.value;
                        }
                    })
                });
            })
            .catch((error) => {
                commit('fieldsLoading', false);
                if (error) {
                    const result = response.error(error);
                }
            });
    },
    async fetchReloadEditFields({ commit, state }, data) {
        const self = this;
        commit('fieldsLoading', true);
        let url = api.panel.fields;
        url = url.replace(':model', state.model);
        let save = serialize.requestNormalize(state.form);
        save.item_id = data.id;
        if (state.locale !== 'ru') {
            save.locale = state.locale;
        }
        const request = serialize.query(save);
        await axios.get(url + '?' + request)
            .then((res) => {
                commit('fieldsLoading', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('fields', result.fields);
                result.fields.tabs.forEach((tab) => {
                    tab.data.forEach((item) => {
                        if (item.value !== undefined) {
                            state.form[item.key] = item.value;
                        }
                    })
                });
            })
            .catch((error) => {
                commit('fieldsLoading', false);
                if (error) {
                    const result = response.error(error);
                }
            });
    },
    async fetchFields({ commit, state }, data) {
        const self = this;
        commit('clear');
        commit('loading', true);
        commit('model', data.model);
        commit('form', {});
        let url = api.panel.fields;
        url = url.replace(':model', data.model);
        if (state.locale !== 'ru') {
            url += '?locale=' + state.locale;
        }
        await axios.get(url)
            .then((res) => {
                commit('loading', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                //commit('item', result.data);
                commit('fields', result.fields);
                self.dispatch('app/panel/panel/base/setTabActiveIndex', 0);
            })
            .catch((error) => {
                if (error) {
                    commit('loading', false);
                    const result = response.error(error);
                }
            });
    },
    async clearForm({ commit, state }, data) {
        const self = this;
        let value = {};
        Object.keys(state.form).map((key) => {
            value[key] = null;
        });
        await commit('form', value);
        self.dispatch('app/table/base/setRequest', value);
        self.dispatch('app/table/base/fetchList', {
            model: 'users',
        });
    },
    setOpened({ commit }, value) {
        commit('opened', value);
    },
    setRole({ commit }, value) {
        commit('role', value);
    },
    setSort({ commit }, value) {
        commit('sort', value);
    },
    setTabActive({ commit }, value) {
        commit('tabActive', value);
    },
    setLocale({ commit }, locale) {
        commit('locale', locale);
    },
    setTabActiveIndex({ commit, state }, index) {
        const tab = Object.assign({}, state.fields.tabs[index], {
            key: index,
            data: [],
        })
        commit('tabActive', tab);
    }
};

const mutations = {
    opened: (state, val) => {
        state.opened = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    loading: (state, val) => {
        if (state.withoutLoading && !val) {
            state.withoutLoading = false;
        }
        if (!state.withoutLoading) {
            state.loading = val;
        }
    },
    withoutLoading: (state, val) => {
        state.withoutLoading = val;
    },
    model: (state, val) => {
        state.model = val;
    },
    item: (state, data) => {
        state.item = data;
    },
    form: (state, data) => {
        state.form = data;
    },
    formMerge: (state, data) => {
        state.form = Object.assign({}, state.form, data);
    },
    errors: (state, data) => {
        state.errors = data;
    },
    fields: (state, data) => {
        state.fields = data;
    },
    fieldsLoading: (state, data) => {
        state.fieldsLoading = data;
    },
    locale: (state, data) => {
        state.locale = data;
    },
    role: (state, value) => {
        state.role = value;
    },
    tabActive: (state, data) => {
        state.tabActive = data;
    },
    clear: (state, data) => {
        state.item = null;
        state.errors = {};
        state.fields = null;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
