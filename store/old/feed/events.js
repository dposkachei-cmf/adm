import response from '../../../plugins/common/response';
import serialize from '../../../plugins/common/serialize';
import api from '../../../api';

// initial state
const state = () => ({
    pending: true,
    loaded: false,
    data: [],
    pagination: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    loaded(state) {
        return state.loaded;
    },
    pending(state) {
        return state.pending;
    },
    pagination(state) {
        return state.pagination;
    },
};

// actions
const actions = {
    async fetchEvents({ commit, state }, data) {
        const self = this;
        if (state.loaded) {
            return;
        }
        commit('pending', true);
        const string = serialize.query(data);
        await self.$axios.get(api.feed.events + '?' + string)
            .then((res) => {
                const resp = response.success(res);
                commit('pending', false);
                if (!state.loaded) {
                    commit('loaded', true);
                }
                if (resp.success) {
                    commit('data', resp.data);
                }
                if (resp.pagination !== undefined) {
                    commit('pagination', resp.pagination);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
    async fetchEventsInitial({ commit, state }, data) {
        const self = this;
        commit('data', []);
        commit('loaded', false);
        await self.dispatch('feed/events/fetchEvents', data);
    },
    async fetchEventsPage({ commit, state }, data) {
        const self = this;
        commit('loaded', false);
        await self.dispatch('feed/events/fetchEvents', data);
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    loaded: (state, val) => {
        state.loaded = val;
    },
    data: (state, data) => {
        if (data.length === 0) {
            state.data = [];
        } else {
            data.forEach(function (value) {
                state.data.push(value);
            });
        }
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
