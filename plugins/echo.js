export default function ({ $echo }) {
    // Echo is available here
    $echo.options.client = require('socket.io-client');
    $echo.options.broadcaster = 'socket.io';
    const port = process.env.ECHO_PORT;
    $echo.options.host = process.env.ECHO_URL;
    if (port !== '') {
        $echo.options.host = `${$echo.options.host}:${port}`;
    }
    $echo.options.reconnectionAttempts = 5;
    $echo.options.authModule = false;
    $echo.options.connectOnLogin = false;
    $echo.options.disconnectOnLogout = false;
}
