import basic from './en/basic';
import editor from './en/editor';
import article from './en/article';
import route from './en/route';
import button from './en/button';
import tooltip from './en/tooltip';
import fields from './en/fields';

export default {
    basic,
    editor,
    article,
    route,
    button,
    tooltip,
    fields,
};
