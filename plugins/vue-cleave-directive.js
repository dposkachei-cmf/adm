import Vue from 'vue';
import Cleave from 'cleave.js';

Vue.directive('cleave', {
    inserted: (el, binding) => {
        el.cleave = new Cleave(el, binding.value || {});
    },
    update: (el) => {
        const event = new Event('input', { bubbles: true });
        setTimeout(function () {
            console.log(el.cleave.properties.result);
            if (el.cleave.properties.result === '' && el.value !== '') {
                //
            } else {
                el.value = el.cleave.properties.result;
            }
            el.dispatchEvent(event);
            console.log(el.value);
        }, 100);
    }
});
