import response from '../../../../plugins/common/response';
import api from '../../../../api';

// initial state
const state = () => ({
    pending: false,
    loaded: false,
    data: {
        groups: [],
        categories: [],
        users: [],
    },
    pagination: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    loaded(state) {
        return state.loaded;
    },
    pagination(state) {
        return state.pagination;
    },
};

// actions
const actions = {
    async fetchData({ commit, state }, data) {
        const self = this;
        if (state.loaded) {
            return;
        }
        commit('pending', true);
        await self.$axios.get(api.user.subscriptions.index + '?extended=1')
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                commit('pagination', null);
                commit('loaded', true);
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
    async fetchDataWithoutPending({ commit, state }, data) {
        const self = this;
        if (state.loaded) {
            return;
        }
        await self.$axios.get(api.user.subscriptions.index + '?extended=1')
            .then((res) => {
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                commit('pagination', null);
                commit('loaded', true);
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
    setLoaded({ commit, state }, value) {
        commit('loaded', value);
    }
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    loaded: (state, val) => {
        state.loaded = val;
    },
    data: (state, data) => {
        state.data = data;
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
