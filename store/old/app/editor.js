const state = () => ({
    data: {
        time: 1626251992170,
        blocks: [],
        version: '2.22.1',
    },
    dataEmptyBlocks: [
        {
            id: 'ajbUIGrr7r',
            type: 'list',
            data: {
                style: 'ordered',
                items: [
                    {
                        content: '12312<b>312 3</b>',
                        items: []
                    },
                    {
                        content: '1231312312',
                        items: [
                            {
                                content: 'eerewrw',
                                items: [
                                    {
                                        content: 'ertert',
                                        items: [
                                            {
                                                content: 'tertertert',
                                                items: []
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                content: 'dgdfgdfgd',
                                items: []
                            }
                        ]
                    }
                ]
            }
        }
    ],
    dataExample: {
        time: 1626251992170,
        blocks: [
            // {
            //     id: 'TCBFvasMGK',
            //     type: 'header',
            //     data: {
            //         text: 'Заголовок статьи',
            //         level: 1
            //     },
            //     tunes: {
            //         anyTuneName: {
            //             alignment: 'left'
            //         }
            //     }
            // },
            {
                id: '4khXZZcb09',
                type: 'paragraph',
                data: {
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    alignment: 'left'
                }
            },
            {
                id: 'Y7LsIbrGl6',
                type: 'header',
                data: {
                    text: 'Duis aute',
                    level: 3
                },
                tunes: {
                    anyTuneName: {
                        alignment: 'center'
                    }
                }
            },
            {
                id: 'xJisyt-wgz',
                type: 'paragraph',
                data: {
                    text: 'Lorem ipsum dolor sit amet, <a href="http://localhost/user/articles/create">consectetur</a> adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    alignment: 'left'
                }
            },
            {
                id: 'tiJBlmBVkA',
                type: 'table',
                data: {
                    content: [
                        [
                            '<b>Lorem</b>',
                            '<b>Lorem</b>',
                            '<b>Lorem</b>',
                            '<b>Lorem</b>',
                            '<b>Lorem</b>'
                        ],
                        [
                            'culpa',
                            'culpa',
                            'culpa',
                            'culpa',
                            'culpa'
                        ]
                    ]
                }
            },
            {
                id: 'tX89or9FIn',
                type: 'warning',
                data: {
                    title: 'Note',
                    message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
                }
            },
            {
                id: 'ZR6up9hLok',
                type: 'paragraph',
                data: {
                    text: '<i>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</i> ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <code class="inline-code">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</code> Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. <mark class="cdx-marker">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&nbsp;</mark>',
                    alignment: 'left'
                }
            },
            {
                id: 'BMCWu7XVU3',
                type: 'person',
                data: {
                    file: {
                        url: 'http://www.landscapingbydesign.com.au/wp-content/uploads/2018/11/img-person-placeholder.jpg',
                        size: 91,
                        name: 'Bg9H8RIbLSui.jpg',
                        extension: 'jpg'
                    },
                    captionHead: 'Сергей Никитин',
                    caption: 'Операционный директор штаб-квартиры Group-IB в Сингапуре, эксперт по компьютерной криминалистике'
                }
            },
            {
                id: 'y50BzVluZF',
                type: 'paragraph',
                data: {
                    text: '<u class="cdx-underline">Lorem</u> ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    alignment: 'center'
                }
            },
            {
                id: 'SEJ53TTbGs',
                type: 'paragraph',
                data: {
                    text: '<mark class="cdx-marker">Lorem</mark> ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    alignment: 'right'
                }
            },
            {
                id: 'PTXbgtKwzY',
                type: 'header',
                data: {
                    text: 'Ut enim ad minim veniam',
                    level: 2
                },
                tunes: {
                    anyTuneName: {
                        alignment: 'center'
                    }
                }
            },
            {
                id: '_3jDV5z_Zp',
                type: 'paragraph',
                data: {
                    text: '<b>Lorem</b> ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    alignment: 'justify'
                }
            },
            {
                id: '6pChyfgEF-',
                type: 'quote',
                data: {
                    text: 'Lorem&nbsp;ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. <b>Excepteur</b> sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&nbsp;&nbsp;',
                    caption: 'Lorem&nbsp;ipsum&nbsp;&nbsp;',
                    alignment: 'left'
                }
            },
            {
                id: 'X7vxvNpVrf',
                type: 'paragraph',
                data: {
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&nbsp;&nbsp;',
                    alignment: 'left'
                }
            },
            {
                id: 'kma5vh4HaU',
                type: 'image',
                data: {
                    file: {
                        url: 'https://image.religionandlaw.ru/storage/images/banner/4/banner_footer/Dqdzyu6l6P9p.jpg',
                        size: 91,
                        name: 'Bg9H8RIbLSui.jpg',
                        extension: 'jpg'
                    },
                    caption: 'Lorem <b>ipsum</b> dolor sit amet, consectetur adipiscing elit',
                    withBorder: false,
                    stretched: true,
                    withBackground: false
                }
            },
            {
                id: '6yrS1ENoiN',
                type: 'paragraph',
                data: {
                    text: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&nbsp;Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.   &nbsp;',
                    alignment: 'left'
                }
            },
            {
                id: 'wYVWHWmOBa',
                type: 'image',
                data: {
                    file: {
                        url: 'https://image.religionandlaw.ru/storage/images/journal/2/rectangle_height/Bg9H8RIbLSui.jpg',
                        size: 91,
                        name: 'Bg9H8RIbLSui.jpg',
                        extension: 'jpg'
                    },
                    caption: 'Религия и Право №1 2018',
                    withBorder: false,
                    stretched: false,
                    withBackground: true
                }
            },
            {
                id: 'JJcwaE3TBZ',
                type: 'paragraph',
                data: {
                    text: '<b>Lorem</b>&nbsp;ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    alignment: 'left'
                }
            },
            {
                id: 'usJdusyR4n',
                type: 'attaches',
                data: {
                    file: {
                        url: 'https://image.religionandlaw.ru/storage/images/journal/2/rectangle_height/Bg9H8RIbLSui.jpg',
                        name: 'Bg9H8RIbLSui.jpg',
                        extension: 'jpg',
                        size: 91
                    },
                    title: 'Bg9H8RIbLSui.jpg или Другое название файла'
                }
            },
            {
                id: 'gY0iWLv0lX',
                type: 'paragraph',
                data: {
                    text: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.&nbsp;Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
                    alignment: 'left'
                }
            },
            {
                id: 'ZBxhepiQZH',
                type: 'AnyButton',
                data: {
                    link: 'http://localhost/user/articles/create',
                    text: 'Купить'
                }
            },
            {
                id: 'W-XQoH3IJS',
                type: 'incut',
                data: {
                    text: 'Новый текст'
                }
            },
            {
                id: 'fJpw6foMw9',
                type: 'paragraph',
                data: {
                    text: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                    alignment: 'left'
                }
            },
            {
                id: 'I63-tngTUF',
                type: 'list',
                data: {
                    style: 'ordered',
                    items: [
                        {
                            content: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. <mark class="cdx-marker">Excepteur sint</mark> occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                            items: [
                                {
                                    content: 'Excepteur sint',
                                    items: []
                                },
                                {
                                    content: 'Occaecat cupidatat',
                                    items: [
                                        {
                                            content: 'Minim',
                                            items: []
                                        },
                                        {
                                            content: 'Veniam',
                                            items: []
                                        }
                                    ]
                                },
                                {
                                    content: 'Sunt in culpa',
                                    items: []
                                }
                            ]
                        },
                        {
                            content: 'Culpa qui officia',
                            items: []
                        },
                        {
                            content: 'Deserunt mollit',
                            items: []
                        }
                    ]
                }
            },
            {
                id: 'VvnDmBxgeW',
                type: 'list',
                data: {
                    style: 'unordered',
                    items: [
                        {
                            content: 'Sint occaecat',
                            items: []
                        },
                        {
                            content: 'Non proident',
                            items: [
                                {
                                    content: 'Mollit anim',
                                    items: []
                                }
                            ]
                        },
                        {
                            content: 'Qui officia',
                            items: []
                        }
                    ]
                }
            },
            {
                id: '6iSVAxKpug',
                type: 'delimiter',
                data: {}
            },
            {
                id: 'KrSrsopbi0',
                type: 'paragraph',
                data: {
                    text: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    alignment: 'left'
                }
            },
            {
                id: 'Y08veWI7Wg',
                type: 'carousel',
                data: [
                    {
                        url: 'https://image.religionandlaw.ru/storage/images/journal/2/rectangle_height/Bg9H8RIbLSui.jpg',
                        caption: 'Excepteur sint'
                    },
                    {
                        url: 'https://image.religionandlaw.ru/storage/images/journal/2/rectangle_height/Bg9H8RIbLSui.jpg',
                        caption: 'non proident'
                    },
                    {
                        url: 'https://image.religionandlaw.ru/storage/images/journal/2/rectangle_height/Bg9H8RIbLSui.jpg',
                        caption: 'non proident'
                    },
                    {
                        url: 'https://image.religionandlaw.ru/storage/images/journal/2/rectangle_height/Bg9H8RIbLSui.jpg',
                        caption: 'non proident'
                    }
                ]
            },
            {
                id: 'bU_UFiiGRz',
                type: 'paragraph',
                data: {
                    text: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&nbsp;',
                    alignment: 'left'
                }
            },
            {
                id: 'DQYIyS2g8N',
                type: 'paragraph',
                data: {
                    text: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    alignment: 'left'
                }
            },
            {
                id: 'V1vYoPNAiK',
                type: 'embed',
                data: {
                    service: 'youtube',
                    source: 'https://www.youtube.com/watch?v=slxPFAJN9UM',
                    embed: 'https://www.youtube.com/embed/slxPFAJN9UM',
                    width: 580,
                    height: 320,
                    caption: '<b>Описание </b>для видео'
                }
            },
            {
                id: '3FBefqv1Ik',
                type: 'paragraph',
                data: {
                    text: 'Aliquet risus feugiat in ante metus dictum. Malesuada pellentesque elit eget gravida cum sociis natoque. Massa ultricies mi quis hendrerit. Ut tellus elementum sagittis vitae et leo duis. Eget sit amet tellus cras adipiscing enim eu turpis egestas. Velit ut tortor pretium viverra suspendisse potenti nullam. Pellentesque pulvinar pellentesque habitant morbi tristique senectus. Scelerisque fermentum dui faucibus in ornare quam. Blandit libero volutpat sed cras ornare arcu dui vivamus. Aliquam etiam erat velit scelerisque. Dui faucibus in ornare quam. Dolor magna eget est lorem ipsum dolor. Eu consequat ac felis donec et. Vulputate mi sit amet mauris. Odio euismod lacinia at quis. Ut sem viverra aliquet eget sit amet tellus. Rhoncus dolor purus non enim. Non arcu risus quis varius quam quisque id diam vel. Nisl vel pretium lectus quam id leo in vitae. Nunc faucibus a pellentesque sit amet.',
                    alignment: 'left'
                }
            },
            {
                id: '3mWS4S48bd',
                type: 'linkTool',
                data: {
                    link: 'http://localhost/user/articles/create',
                    meta: {
                        title: 'CodeX Team',
                        description: 'Club of web-development, design and marketing. We build team learning how to build full-valued projects on the world market.',
                        image: {
                            url: 'https://codex.so/public/app/img/meta_img.png'
                        }
                    }
                }
            },
            {
                id: 'umFdcNXdeh',
                type: 'paragraph',
                data: {
                    text: 'Non arcu risus quis varius quam quisque id diam vel. Nisl vel pretium lectus quam id leo in vitae. Nunc faucibus a pellentesque sit amet',
                    alignment: 'left'
                }
            }
        ],
        version: '2.22.1'
    },
    preview: false,
    //split: true,
    focusedIndex: null,
    focusedId: null,
    focusedMainHeader: false,
});

const getters = {
    blocks(state) {
        return state.data.blocks;
    },
    version(state) {
        return state.data.version;
    },
    preview(state) {
        return state.preview;
    },
    focusedId(state) {
        return state.focusedId;
    },
    focusedIndex(state) {
        return state.focusedIndex;
    },
    focusedMainHeader(state) {
        return state.focusedMainHeader;
    },
};

const actions = {
    setBlocks({ commit }, blocks) {
        commit('blocks', blocks);
    },
    setBlocksDefault({ commit }, blocks) {
        commit('blocksDefault', blocks);
    },
    setFocused({ commit }, data) {
        commit('focused', data);
    },
    setPreview({ commit }, data) {
        commit('preview', data);
    },
    clearFocused({ commit }, data) {
        commit('focused', {
            id: null,
            index: null,
            isHeader: false,
        });
    }
};

const mutations = {
    blocks: (state, val) => {
        state.data.blocks = val;
    },
    blocksDefault: (state, val) => {
        state.dataDefault.blocks = val;
    },
    version: (state, val) => {
        state.data.version = val;
    },
    focused: (state, data) => {
        state.focusedIndex = data.index;
        state.focusedId = data.id;
        state.focusedMainHeader = data.isHeader;
    },
    focusedIndex: (state, val) => {
        state.focusedIndex = val;
    },
    preview: (state, val) => {
        state.preview = val;
    },
    focusedId: (state, val) => {
        state.focusedId = val;
    },
    focusedMainHeader: (state, val) => {
        state.focusedMainHeader = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
