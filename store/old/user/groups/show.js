import response from '../../../../plugins/common/response';
import api from '../../../../api';

const state = () => ({
    pending: false,
    loaded: false,
    data: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    loaded(state) {
        return state.loaded;
    },
};

// actions
const actions = {
    async fetchData({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        if (state.loaded) {
            return;
        }
        const url = api.user.groups.show.replace(':id', data.id);
        await self.$axios.get(url)
            .then((res) => {
                commit('pending', false);
                commit('loaded', true);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                }
            });
    },
    async fetchDataForce({ commit, state }, data) {
        const self = this;
        commit('loaded', false);
        await self.dispatch('user/groups/show/fetchData', data);
    },
};

// mutations
const mutations = {
    loaded: (state, val) => {
        state.loaded = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
