const state = () => ({
    item: {},
    model: null,
    current: null,
    active: null,
    disabled: false,
    names: {
        login: 'login',
    },
    deleted: false,
    deletedFull: false,
});

const getters = {
    model(state) {
        return state.model;
    },
    current(state) {
        return state.current;
    },
    active(state) {
        return state.active;
    },
    names(state) {
        return state.names;
    },
    disabled(state) {
        return state.disabled;
    },
    item(state) {
        return state.item;
    },
    deleted(state) {
        return state.deleted;
    },
    deletedFull(state) {
        return state.deletedFull;
    },
};

const actions = {
    setActive({ commit }, name) {
        commit('active', name);
    },
    setCurrent({ commit }, name) {
        commit('current', name);
    },
    setDisabled({ commit }) {
        commit('disabled', true);
    },
    setItem({ commit }, data) {
        commit('model', data.model);
        commit('item', data.item);
    },
    setDeleted({ commit }) {
        commit('deleted', true);
    },
    setDeletedFull({ commit }) {
        commit('deletedFull', true);
    },
};

const mutations = {
    model: (state, val) => {
        state.model = val;
    },
    current: (state, val) => {
        state.current = val;
    },
    active: (state, val) => {
        state.active = val;
    },
    disabled: (state, val) => {
        state.disabled = val;
    },
    item: (state, val) => {
        state.item = val;
    },
    deleted: (state, val) => {
        state.deleted = val;
        setTimeout(function () {
            state.deleted = false;
        }, 500);
    },
    deletedFull: (state, val) => {
        state.deletedFull = val;
        setTimeout(function () {
            state.deletedFull = false;
        }, 500);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
