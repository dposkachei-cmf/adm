export default {

    /**
     * @param item
     * @returns {[{sizes: string, rel: string, href: string}, {sizes: string, rel: string, href: string, type: string}, {sizes: string, rel: string, href: string, type: string}, {rel: string, href: string}, {color: string, rel: string, href: string}, null]}
     */
    seoBaseLink(item) {
        return [
            { rel: 'apple-touch-icon', sizes: '180x180', href: `${process.env.APP_URL}/img/apple-touch-icon.png` },
            { rel: 'icon', sizes: '32x32', type: 'image/png', href: `${process.env.APP_URL}/img/favicon-32x32.png` },
            { rel: 'icon', sizes: '16x16', type: 'image/png', href: `${process.env.APP_URL}/img/favicon-16x16.png` },
            { rel: 'icon', sizes: '16x16', type: 'image/x-icon', href: `${process.env.APP_URL}/img/favicon.ico` },
            { rel: 'manifest', href: `${process.env.APP_URL}/site.webmanifest` },
            { rel: 'mask-icon', href: `${process.env.APP_URL}/img/favicon-32x32.png`, color: '#5bbad5' },
            { rel: 'canonical', href: item.url },
        ];
    },
    /**
     *
     * @param item
     * @returns {[{hid: string, name: string, content}, {hid: string, name: string, content}, {hid: string, name: string, content: string}, {hid: string, name: string, content: string}, {hid: string, name: string, content}, null, null, null, null, null, null, null, null, null, null]}
     */
    seoBase(item) {
        return [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            {
                hid: 'description',
                name: 'description',
                content: item.description,
            },
            {
                hid: 'keywords',
                name: 'keywords',
                content: item.keywords || '',
            },
            {
                hid: 'image',
                name: 'image',
                content: item.image || '',
            }, {
                hid: 'vk:image',
                name: 'vk:image',
                content: item.image || '',
            }, {
                hid: 'twitter:title',
                name: 'twitter:title',
                content: `${item.title}`,
            }, {
                hid: 'twitter:description',
                name: 'twitter:description',
                content: `${item.description}`,
            }, {
                hid: 'twitter:image',
                name: 'twitter:image',
                content: item.image || '',
            }, {
                hid: 'twitter:url',
                name: 'twitter:url',
                content: item.url,
            }, {
                hid: 'twitter:image:alt',
                name: 'twitter:image:alt',
                content: `${item.title}`,
            }, {
                hid: 'og:title',
                property: 'og:title',
                content: `${item.title}`,
            }, {
                hid: 'og:description',
                property: 'og:description',
                content: `${item.description}`,
            }, {
                hid: 'og:type',
                property: 'og:type',
                content: 'article',
            }, {
                hid: 'og:url',
                property: 'og:url',
                content: item.url,
            },
            {
                hid: 'og:locale',
                property: 'og:locale',
                content: 'ru-RU',
            },
            {
                hid: 'og:site_name',
                property: 'og:site_name',
                content: process.env.APP_NAME,
            }, {
                hid: 'og:image',
                property: 'og:image',
                content: item.image || '',
            }, {
                hid: 'og:image:secure_url',
                property: 'og:image:secure_url',
                content: item.image,
            }, {
                hid: 'og:image:alt',
                property: 'og:image:alt',
                content: `${item.title}`,
            }
        ];
    },
    seoScriptsAuth() {
        const scripts = [];
        scripts.push({
            src: 'https://yastatic.net/s3/passport-sdk/autofill/v1/sdk-suggest-latest.js',
            async: true
        });
        return scripts;
    },

    /**
     *
     * @param article
     * @returns {[{hid: string, name: string, content}, {hid: string, name: string, content: string}, {hid: string, name: string, content: *}, {hid: string, name: string, content}, {hid: string, property: string, content}, null, null, null, null]}
     */
    seoArticle(article) {
        const item = {
            title: article.meta.title,
            image: article.image,
            description: article.meta.description,
            keywords: article.meta.keywords,
        };
        item.url = `${process.env.APP_URL}/articles/${article.id}/${article.slug}`;
        return {
            title: item.title,
            meta: this.seoBase(item),
            link: this.seoBaseLink(item),
        };
    },
    seoGroup(group) {
        let item;
        if (group.meta !== null) {
            item = {
                title: `${group.meta.title}`,
                image: group.image,
                description: group.meta.description,
                keywords: group.meta.keywords,
            };
        } else {
            item = {
                title: `${group.title}`,
                image: group.image,
                description: group.description,
                keywords: '',
            };
        }
        item.url = `${process.env.APP_URL}/${group.slug}`;
        return {
            title: item.title,
            meta: this.seoBase(item),
            link: this.seoBaseLink(item),
        };
    },
    seoCategory(category) {
        let item;
        if (category.meta !== null) {
            item = {
                title: `${category.meta.title}`,
                image: category.image,
                description: category.meta.description,
                keywords: category.meta.keywords,
            };
        } else {
            item = {
                title: `${category.title}`,
                image: category.image,
                description: category.description,
                keywords: '',
            };
        }
        item.url = `${process.env.APP_URL}/${category.slug}`;
        return {
            title: item.title,
            meta: this.seoBase(item),
            link: this.seoBaseLink(item),
        };
    },
    seoPage(page) {
        let item;
        if (page.meta !== null) {
            item = {
                title: `${page.meta.title}`,
                description: page.meta.description,
                keywords: page.meta.keywords,
            };
        } else {
            item = {
                title: `${page.title}`,
                description: page.description,
                keywords: '',
            };
        }
        item.image = `${process.env.APP_URL}/img/favicon-32x32.png`;
        item.url = `${process.env.APP_URL}/${page.slug}`;
        return {
            title: item.title,
            meta: this.seoBase(item),
            link: this.seoBaseLink(item),
        };
    },
    seoOther(data) {
        const item = {
            title: `${data.title}`,
            description: data.description || process.env.APP_DESCRIPTION,
            keywords: '',
            image: `${process.env.APP_URL}/img/favicon-32x32.png`,
        };
        if (data.url !== null) {
            item.url = `${process.env.APP_URL}${data.url}`;
        } else {
            item.url = `${process.env.APP_URL}`;
        }
        return {
            title: item.title,
            meta: this.seoBase(item),
            link: this.seoBaseLink(item),
        };
    },
    seoOtherAuth(data) {
        const seo = this.seoOther(data);
        seo.script = this.seoScriptsAuth();
        return seo;
    },
    seoUser(user) {
        let item;
        if (user.meta !== null) {
            item = {
                title: `${user.meta.title}`,
                image: user.image,
                description: user.meta.description,
                keywords: user.meta.keywords,
            };
        } else {
            item = {
                title: `${user.last_name} ${user.first_name}`,
                image: user.image,
                description: user.details.description,
                keywords: '',
            };
        }
        item.url = `${process.env.APP_URL}/u/${user.id}-${user.slug}`;
        return {
            title: item.title,
            meta: this.seoBase(item),
            link: this.seoBaseLink(item),
        };
    },
};
