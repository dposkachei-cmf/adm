export default {
    article: {
        keywords: 'Keywords',
        library: 'Library',
        udk: 'UDK',
        doi: 'DOI',
    },
    tool: {
        paragraph: 'Параграф',
        heading: 'Заголовок',
        button: 'Кнопка',
        alert: 'Предупреждение',
        attaches: 'Файл',
        audio: 'Аудио-файл',
        video: 'Видео',
        carousel: 'Галерея',
        image: 'Изображение',
        person: 'Персона',
        incut: 'Врезка',
        list: 'Список',
        warning: 'Примечание',
        checklist: 'Чек-лист',
        quote: 'Цитата',
        code: 'Код',
        delimiter: 'Разделитель',
        asterisks: 'Разделитель из звёзд',
        html: 'HTML-фрагмент',
        table: 'Таблица',
        link: 'Ссылка',
        marker: 'Маркер',
        bold: 'Полужирный',
        italic: 'Курсив',
        'inline-code': 'Код',
        underline: 'Подчеркнуть',
        supscript: 'Надстрочный индекс',
    },
    basic: {
        'click-tune': 'Нажмите, чтобы настроить',
        'convert-to': 'Конвертировать в',
        'button-default': 'Кнопка по умолчанию',
        'or-move': 'или перетащите',
        delete: 'Удалить',
        text: 'Текст',
        image: 'Изображение',
        add: 'Добавить',
        link: 'Ссылка',
        set: 'Применить',
        caption: 'Описание',
        title: 'Заголовок',
        message: 'Сообщение',
        'image-error': 'Ошибка загрузки файла',
        'your-text': 'Ваш текст',
        'move-up': 'Переместить вверх',
        'move-down': 'Переместить вниз',
        'upload-image': 'Загрузить изображение',
        'select-image': 'Выбрать изображение',
        'select-image-file': 'Выберите файл',
        'image-border': 'Обвести',
        'image-stretch': 'Растянуть',
        'image-background': 'Добавить фон',
        'enter-code': 'Ваш код',
        author: 'Автор',
        'quote-text': 'Цитата',
        list: {
            unordered: 'Маркированный список',
            ordered: 'Нумерованный список',
        },
        quote: {
            'enter-quote': 'Введите цитату',
            'enter-caption': 'Введите источник',
            'enter-alignment': 'Введите источник',
        },
        'insert-column-before': 'Вставить колонку до',
        'insert-column-after': 'Вставить колонку после',
        'insert-row-before': 'Вставить строку до',
        'insert-row-after': 'Вставить строку после',
        'delete-row': 'Удалить нижнюю строку',
        'delete-column': 'Удалить последнюю колонку',
    },
    ui: {
        blockTunes: {
            toggler: {
                'Click to tune': 'Нажмите, чтобы настроить',
                'or drag to move': 'или перетащите'
            },
        },
        inlineToolbar: {
            converter: {
                'Convert to': 'Конвертировать в'
            }
        },
        toolbar: {
            toolbox: {
                Add: 'Добавить'
            }
        }
    },
};
