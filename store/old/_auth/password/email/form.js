import response from '../../../../../plugins/common/response';
import api from '../../../../../api';

const state = () => ({
    pending: false,
    form: {},
    errors: {},
});

const getters = {
    pending(state) {
        return state.pending;
    },
    form(state) {
        return state.form;
    },
    errors(state) {
        return state.errors;
    },
};

const actions = {
    async fetchEmail({ commit }, data) {
        const self = this;
        commit('pending', true);
        commit('errors', {});
        await self.$axios.post(api.auth.passwordEmail, data)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                commit('form', {});
                const result = response.success(res);
                if (result.success && result.message !== undefined) {
                    self.$toast.success(result.message);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                    if (result.message !== undefined && result.status_code !== 422) {
                        self.$toast.error(result.message);
                    }
                }
            });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    form: (state, val) => {
        state.form = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
