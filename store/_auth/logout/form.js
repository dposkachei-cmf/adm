'use strict';

import api from "../../../api";
import response from "../../../plugins/common/response";
import axiosStore from "../../../plugins/common/axiosStore";

const state = () => ({
    pending: false,
    reloaded: false,
    errors: {},
});
const getters = {
    pending(state) {
        return state.pending;
    },
    reloaded(state) {
        return state.reloaded;
    },
    errors(state) {
        return state.errors;
    },
};
const actions = {
    async fetchLogout({ commit }) {
        const self = this;
        commit('pending', true);
        commit('errors', {});
        await axiosStore.init(self.$axios).post(api.auth.logout, {}, function (result) {
            commit('pending', false);
            if (result.data.redirect !== undefined) {
                window.location.href = result.data.redirect;
            }
        }, function (result) {
            commit('pending', false);
            self.$cookies.remove(`auth._token.local`);
            window.location.href = '/';
            if (result.errors !== undefined) {
                commit('errors', result.errors);
            }
        });
    },
    async fetchLogoutSimple({ commit }) {
        const self = this;
        self.$cookies.remove(`auth._token.local`);
        localStorage.removeItem('auth._token.local');
        window.location.href = '/';
    }
};
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    reloaded: (state, val) => {
        state.reloaded = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
};
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
