import response from '../../plugins/common/response';
import api from './../../api';
import axios from "./../../plugins/axios";
import axiosStore from "../../plugins/common/axiosStore";

const state = () => ({
    loaded: false,
    pending: false,
    data: {},
});

const getters = {
    loaded(state) {
        return state.loaded;
    },
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
};

const actions = {
    async fetchUser({ commit, state }) {
        const self = this;
        commit('pending', true);
        await axiosStore.init(self.$axios).get(api.auth.user, {}, function (result) {
            commit('pending', false);
            commit('data', result.data);
            if (!state.loaded) {
                commit('loaded', true);
            }
        }, function () {
            commit('pending', false);
        });
    },
    async setData({ commit }, data) {
        commit('data', data);
    },
};

const mutations = {
    loaded: (state, val) => {
        state.loaded = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, val) => {
        state.data = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
