import response from '../../../../plugins/common/response';
import api from "../../../../api";
import beforeFetch from "../../../../plugins/common/beforeFetch";
import axiosStore from "../../../../plugins/common/axiosStore";

const state = () => ({
    pendingArray: [],
});

const getters = {
    pendingArray(state) {
        return state.pendingArray;
    },
};

const actions = {
    async fetchDestroy({ commit, state }, data) {
        const self = this;
        commit('pendingArray', response.pendingAdd(state.pendingArray, data.id));
        const url = beforeFetch.getUrlModelIdImageId(api.panel.image.destroy, data.model, data.item.id, data.id);
        await axiosStore.init(self.$axios).delete(url, {}, function () {
            commit('pendingArray', response.pendingRemove(state.pendingArray, data.id));
            self.dispatch('app/panel/base/fetchReloadItem');
            // @todo UserChangedEvent
            // if (data.model === 'users') {
            //     self.dispatch('user/base/fetchUser');
            // }
        }, function () {
            commit('pendingArray', response.pendingRemove(state.pendingArray, data.id));
        });
    },
};

const mutations = {
    pendingArray: (state, val) => {
        state.pendingArray = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
