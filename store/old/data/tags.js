import response from '../../../plugins/common/response';
import api from '../../../api';

const state = () => ({
    loaded: false,
    pending: true,
    data: [],
});

const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    loaded(state) {
        return state.loaded;
    },
};

const actions = {
    async fetchTags({ commit, state }, data) {
        const self = this;
        if (state.loaded) {
            return;
        }
        commit('pending', true);
        await self.$axios.get(api.index.tags)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                commit('loaded', true);
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                }
            });
    },
};

const mutations = {
    loaded: (state, val) => {
        state.loaded = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, val) => {
        state.data = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
