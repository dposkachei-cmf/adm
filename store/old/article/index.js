import response from '../../../plugins/common/response';
import api from '../../../api';

const state = () => ({
    pending: false,
    isPrivate: false,
    data: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    isPrivate(state) {
        return state.isPrivate;
    },
};

// actions
const actions = {
    async fetchData({ commit }, data) {
        const self = this;
        commit('data', null);
        commit('pending', true);
        commit('isPrivate', false);
        let url = api.articles.show.replace(':id', data.id);
        if (data.token !== undefined) {
            url += '?token=' + data.token;
        }
        await self.$axios.get(url)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                if (data.token !== undefined) {
                    commit('isPrivate', true);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.status_code === 404) {
                        self.$router.replace('/');
                    }
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    isPrivate: (state, val) => {
        state.isPrivate = val;
    },
    data: (state, data) => {
        state.data = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
