import api from '../api';
import response from '../plugins/common/response';
import cookieBase from '../plugins/mixins/ssr/cookieActions';

/**
 * Инициализация данных из /data
 *
 * @param app
 * @param redirect
 * @param store
 */
export default ({ app, redirect, store }) => {
    const loaded = store.getters['app/data/loaded'];
    if (loaded) {
        return;
    }
    const cookieApp = cookieBase.cookieMiddleware(app).initial();
    //console.log(cookieBase.cookiesMiddleware(app).hasInitial());
    // if (!cookieBase.cookiesMiddleware(app).hasInitial()) {
    //     store.commit('app/cookie/data', cookieApp);
    //     store.commit('app/cookie/needSave', true);
    // }
    store.commit('app/sidebar/left', cookieApp.sidebar.left);
    store.commit('app/sidebar/right', cookieApp.sidebar.right);
    //store.commit('app/editor/preview', cookieApp.editor.preview);
    //store.commit('app/sidebar/feedEvents', cookieApp.feed.events.active);
    //store.commit('app/sidebar/feedTheme', cookieApp.feed.theme);
    //store.commit('app/fontSize', cookieApp.feed.fontSize);
    //store.commit('feed/articles/form', cookieApp.feed.form);
    // if (cookieApp1 !== undefined) {
    //     cookieBase.cookieActions().commitInitial(store, cookieApp1);
    // } else {
    //     cookieBase.cookieActions().initial();
    //     const cookieApp2 = cookieBase.cookieActions().get();
    //     cookieBase.cookieActions().commitInitial(store, cookieApp2);
    // }
    store.commit('app/data/pending', true);
    return app.$axios.get(api.index.data)
        .then((res) => {
            store.commit('app/data/pending', false);
            if (res.status !== 200) {
                return;
            }
            const result = response.success(res);
            store.commit('app/data/data', result.data);
            if (!loaded) {
                store.commit('app/data/loaded', true);
            }
        })
        .catch((error) => {
            if (error) {
                store.commit('app/data/pending', false);
            }
        });
};
