const state = () => ({
    success: null,
    error: null,
});

const getters = {
    success(state) {
        return state.success;
    },
    error(state) {
        return state.error;
    },
};

const actions = {
    setSuccess({ commit }, name) {
        commit('success', name);
    },
};

const mutations = {
    success: (state, val) => {
        state.success = val;
    },
    error: (state, val) => {
        state.error = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
