import response from '../../../../plugins/common/response';
import api from '../../../../api';

const state = () => ({
    pending: false,
    pendingArray: [],
});

const getters = {
    pending(state) {
        return state.pending;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
};

const actions = {
    async fetchDestroy({ state, commit }, data) {
        const self = this;
        commit('pending', true);
        commit('pendingArray', response.pendingAdd(state.pendingArray, data.provider));
        const url = api.user.social.destroy.replace(':provider', data.provider);
        await self.$axios.delete(url)
            .then((res) => {
                commit('pending', false);
                commit('pendingArray', response.pendingRemove(state.pendingArray, data.provider));
                self.$auth.fetchUser().then(function () {});
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                    commit('pendingArray', response.pendingRemove(state.pendingArray, data.provider));
                }
            });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    pendingArray: (state, val) => {
        state.pendingArray = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
