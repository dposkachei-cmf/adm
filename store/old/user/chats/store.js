import response from '../../../../plugins/common/response';
import api from '../../../../api';

const state = () => ({
    pending: false,
    data: null,
    errors: {},
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
};

// actions
const actions = {
    async fetchStore({ commit }, data) {
        const self = this;
        commit('pending', true);
        const url = api.user.chats.store;
        await self.$axios.post(url, {
            user_id: data.user_id,
        })
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                self.dispatch('user/chats/setLoaded', false);
                self.$router.push(`/m/${result.data.id}`);
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
    errors: (state, val) => {
        state.errors = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
