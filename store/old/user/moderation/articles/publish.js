import response from '../../../../../plugins/common/response';
import api from '../../../../../api';

const state = () => ({
    published: false,
    pending: false,
    pendingArray: [],
});

const getters = {
    published(state) {
        return state.published;
    },
    pending(state) {
        return state.pending;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
};

// actions
const actions = {
    async fetchPublish({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        commit('pendingArray', response.pendingAdd(state.pendingArray, data.id, 'article'));
        const url = api.user.moderation.articles.publish.replace(':id', data.id);
        await self.$axios.post(url)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                commit('published', true);
                commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, 'article'));
                const result = response.success(res);
                //commit('data', result.data);
                if (result.success && result.message !== undefined) {
                    self.$toast.success(result.message);
                }
                self.$auth.fetchUser().then(function () {});
                //self.$router.push('/personal/moderation/publishes');
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, 'article'));
                    const result = response.error(error);
                    if (result.message !== undefined && result.status_code !== 422) {
                        self.$toast.error(result.message);
                    }
                }
            });
    },
};

// mutations
const mutations = {
    published: (state, val) => {
        state.published = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    pendingArray: (state, val) => {
        state.pendingArray = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
