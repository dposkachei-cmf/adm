import moment from 'moment';

export default {
    echo() {
        const self = this;
        const isEnabled = () => process.env.ECHO_ENABLED === 'true';
        const setHeaders = () => {
            self.$echo.options.auth.headers.Authorization = self.$auth.strategy.token.get();
        };
        const listen = () => {
            setHeaders();
            console.log('------------- ECHO -------------');
            console.log(process.env.ECHO_URL);
            self.$echo.private(`admin.${self.$auth.user.id}`)
                .listen('DataChangedEvent', (e) => {
                    console.log('DataChangedEvent');
                    console.log(e);
                    self.$nuxt.$store.dispatch('app/data/fetchIndexDataForce', {});
                }).listen('\\App\\Domain\\Cmf\\Events\\ChangedEvent', (e) => {
                    console.log('ChangedEvent');
                    console.log(e);
                    if (e.event.table !== undefined) {
                        self.$nuxt.$store.dispatch('app/table/changed/setModel', e.event.table.class);
                    }
                    if (e.event.user !== undefined) {
                        self.$auth.fetchUser().then(function () {});
                    }
                }).listen('User\\ChangeDeviceEvent', (e) => {
                    //console.log(e);
                }).listen('User\\UserReloadCommentsEvent', (e) => {
                    console.log(e);
                    self.$nuxt.$store.dispatch('data/comments/fetchCommentsRefreshWithoutPending', {});
                }).listen('User\\UserNewCommentEvent', (e) => {
                    console.log(e);
                    //self.$nuxt.$store.dispatch('data/comments/fetchCommentsRefresh', {});
                    self.$nuxt.$store.dispatch('data/comments/pushComment', e.comment);
                }).listen('User\\UserNewChatEvent', (e) => {
                    self.$nuxt.$store.dispatch('user/notifications/counters/incrementChats', {});
                    self.$nuxt.$store.dispatch('user/chats/fetchChats', {});
                    console.log(e);
                    listenChat({ id: e.chat_id });
                }).notification((notification) => {
                    console.log(notification);
                    console.log(notification.type);
                    sendNotification(notification);
                });
            // self.$echo.private(`admin.${self.$auth.user.id}.${self.$auth.user.session_hash}`).listen('User\\AdminNotificationEvent', (e) => {
            //     self.$toast.success(e.message, {
            //         duration: 300000,
            //     });
            // });
            // self.$echo.private(`user.${self.$auth.user.id}`).notification((notification) => {
            //     console.log(notification);
            //     console.log(notification.type);
            //     sendNotification(notification);
            // });
            self.$nuxt.$store.dispatch('user/echo/setConnected', true);
            self.$nuxt.$store.dispatch('user/echo/setNeedConnection', false);
        };
        const listenChat = (chat) => {
            console.log(`------------- ECHO LISTEN CHAT ${chat.id} -------------`);
            self.$echo.join(`chat.${chat.id}`)
                .here((users) => {
                    console.log(users);
                }).joining((user) => {
                    console.log(user.name);
                }).leaving((user) => {
                    console.log(user.name);
                }).error((error) => {
                    console.error(error);
                }).listen('Chat\\ChatNewMessageEvent', (e) => {
                    const message = e;
                    if (message.user_id === self.$auth.user.id) {
                        message.read_at = moment().toISOString();
                    } else {
                        self.$nuxt.$store.dispatch('user/notifications/counters/incrementChats', {});
                    }
                    self.$nuxt.$store.dispatch('user/chats/messages/appendMessage', message);
                    self.$nuxt.$store.dispatch('user/chats/setLastMessage', message);
                    //self.$nuxt.$store.dispatch('user/setNewChatMessage', message);
                    console.log(e);
                });
        };
        const leaveChat = (chat) => {
            self.$echo.leave(`chat.${chat.id}`);
        };
        const connect = () => {
            if (!self.$auth.loggedIn) {
                return;
            }
            if (!isEnabled()) {
                return;
            }
            self.$echo.connect();
            listen();
        };
        const disconnect = () => {
            if (!isEnabled()) {
                return;
            }
            self.$echo.disconnect();
        };
        const sendNotification = (notification) => {
            const options = {
                title: notification.extend.title,
                variant: notification.variant,
                solid: true,
                autoHideDelay: 3000
            };
            if (['user.article', 'user.article.status'].includes(notification.type)) {
                self.$auth.fetchUser().then(function () {});
            }
            self.$bvToast.toast(notification.message, options);
            self.$nuxt.$store.dispatch('user/notifications/counters/incrementNotifications', {});
            // self.$toast.info(notification.message, {
            //     duration: 300000,
            // });
        };
        return {
            connect,
            listen,
            isEnabled,
            disconnect,
            listenChat,
            leaveChat,
        };
    },
};
