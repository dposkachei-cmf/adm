import response from '../../../../plugins/common/response';
import api from '../../../../api';

// initial state
const state = () => ({
    withPending: true,
    pending: true,
    data: {},
    pagination: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    withPending(state) {
        return state.withPending;
    },
    pending(state) {
        return state.pending;
    },
    pagination(state) {
        return state.pagination;
    },
};

// actions
const actions = {
    async fetchComments({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        await self.$axios.get(api.user.comments.index)
            .then((res) => {
                commit('pending', false);
                if (!state.withPending) {
                    commit('withPending', true);
                }
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                commit('pagination', null);
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
    async fetchCommentsWithoutPending({ commit }, data) {
        const self = this;
        commit('withPending', false);
        await self.dispatch('user/comments/fetchComments', data);
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        if (state.withPending) {
            state.pending = val;
        }
    },
    data: (state, data) => {
        state.data = data;
    },
    withPending: (state, data) => {
        state.withPending = data;
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
