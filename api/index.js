export default {
    index: {
        data: '/data',
    },
    auth: {
        token: ``, //`${env.API_URL}/sanctum/csrf-cookie`,
        login: `/auth/login`, //`${env.APP_URL}/auth/login`,
        logout: `/auth/logout`, //`${env.APP_URL}/auth/logout`,
        user: `/user`, //'/user',
    },
    users: {
        list: '/users',
        edit: '/users/:id',
        update: '/users/:id',
    },
    dev: {
        action: '/dev/action',
        system: '/dev/system',
    },
    statistic: {
        index: '/statistic',
    },
    user: {
        notifications: {
            index: '/user/notifications',
            destroy: '/user/notifications/:id',
            clear: '/user/notifications/clear',
        },
        votes: {
            index: '/user/votes',
            array: '/user/votes',
            toggle: '/user/votes/toggle',
        },
        comments: {
            index: '/user/comments',
            destroy: '/user/comments/:id',
        },
    },
    panel: {
        comments: '/:model/:id/comments',
        history: '/:model/:id/history',
        show: '/:model/:id',
        action: '/:model/:id/action',
        edit: '/:model/:id/edit',
        //edit: '/:model/:id',
        update: '/:model/:id',
        store: '/:model',
        fields: '/:model/fields',
        values: '/:model/fields/values',
        upload: {
            image: '/:model/:id/upload/image'
        },
        image: {
            destroy: '/:model/:id/image/:image_id/destroy',
            main: '/:model/:id/image/:image_id/main',
        },
    },
    table: {
        list: '/:model',
        destroy: '/:model/:id',
        banned: '/:model/:id/banned',
        release: '/:model/:id/release',
    },
    articles: {
        list: '/articles',
    },
    editor: {
        fields: '/:model/editor/fields',
        uid: '/:model/editor/uid',
        upload: '/:model/editor/upload/:uid',
        url: '/:model/editor/url/:uid',
        search: '/:model/editor/search',
        article: {
            show: '/:model/:id/editor',
            update: '/:model/:id/editor',
            image: '/:model/:id/image',
            video: '/:model/:id/video',
            destroy: '/:model/:id',
            store: '/:model/editor',
            //priority: '/:model/priority',
        },
        // articles: {
        //     list: '/:model/:id/articles',
        //     show: '/:model/:id/articles/:article_id',
        //     update: '/:model/:id/articles/:article_id',
        //     image: '/:model/:id/articles/:article_id/image',
        //     destroy: '/:model/:id/articles/:article_id',
        //     store: '/:model/:id/articles',
        //     priority: '/:model/:id/articles/priority',
        // },
    },
};
