import response from '../../../../plugins/common/response';
import api from '../../../../api';

// initial state
const state = () => ({
    pending: false,
    data: [],
    pagination: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    pagination(state) {
        return state.pagination;
    },
};

// actions
const actions = {
    async fetchNotifications({ commit }, data) {
        const self = this;
        commit('pending', true);
        await self.$axios.get(api.user.notifications.index)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                if (result.pagination !== undefined) {
                    commit('pagination', result.pagination);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
    async fetchNotificationsWithoutPending({ commit, state }, data) {
        const self = this;
        await self.$axios.get(api.user.notifications.index)
            .then((res) => {
                const result = response.success(res);
                commit('data', result.data);
                if (result.pagination !== undefined) {
                    commit('pagination', result.pagination);
                }
            })
            .catch((error) => {
                if (error) {
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
