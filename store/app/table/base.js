import api from "../../../api";
import axios from "./../../../plugins/axios";
import response from "../../../plugins/common/response";
import serialize from "../../../plugins/common/serialize";
import beforeFetch from "../../../plugins/common/beforeFetch";
import axiosStore from "../../../plugins/common/axiosStore";

const state = () => ({
    locale: 'ru',
    pending: false,
    loaded: false,
    search: false, // default collapse for search form
    withPending: true,
    model: null, // string
    data: null, // array of objects table items
    form: {}, // search form
    formRoute: {}, // search url form
    request: {}, // search request object
    errors: {},
    fields: null, // object
    pagination: null, // object
});

const getters = {
    model(state) {
        return state.model;
    },
    search(state) {
        return state.search;
    },
    data(state) {
        return state.data;
    },
    form(state) {
        return state.form;
    },
    formRoute(state) {
        return state.formRoute;
    },
    request(state) {
        return state.request;
    },
    loaded(state) {
        return state.loaded;
    },
    fields(state) {
        return state.fields;
    },
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
    pagination(state) {
        return state.pagination;
    },
    locale(state) {
        return state.locale;
    },
};

const actions = {
    /**
     *
     * @param commit
     * @param state
     * @param {string} value
     * @returns {Promise<void>}
     */
    async setLocale({ commit, state }, value) {
        const self = this;
        await commit('locale', value);
        self.dispatch('app/table/base/fetchList', {
            model: state.model,
        });
    },
    /**
     *
     * @param commit
     * @param state
     * @param {{model: string, locale: string|undefined, page: int, request: object}} data
     * @returns {Promise<void>}
     */
    async fetchList({ commit, state }, data) {
        const self = this;
        if (data.model !== state.model) {
            commit('loaded', false);
            commit('model', data.model);
            commit('fields', null);
            commit('pagination', null);
        }
        if (data.locale !== undefined) {
            commit('locale', data.locale);
        }
        commit('pending', true);
        const request = beforeFetch.getTableListRequest(state.request, data, state.locale);
        console.log(request);
        commit('request', request);
        commit('formRoute', request);
        const url = beforeFetch.getTableListUrl(api.table.list, state.model);
        const query = state.request;
        await axiosStore.init(self.$axios).get( url, query, function (result) {
            commit('pending', false);
            if (!state.loaded) {
                commit('loaded', true);
            }
            commit('data', result.data);
            commit('fields', result.fields);
            commit('pagination', result.pagination);
        }, function () {
            commit('pending', false);
            // if (error.response.status === 403) {
            //     self.$toast.error('Доступ запрещен');
            // }
        });
    },
    fetchListInitial({ commit, state }, data) {
        const self = this;
        commit('loaded', false);
        self.dispatch('app/table/base/fetchList', data)
    },
    fetchListWithoutPending({ commit, state }, data) {
        const self = this;
        commit('withPending', false);
        self.dispatch('app/table/base/fetchList', data)
    },
    async fetchDelete({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        const url = beforeFetch.getTableDeleteUrl(api.table.destroy, state.model, data.id);
        await axiosStore.init(self.$axios).delete(url, {}, function (result) {
            commit('pending', false);
        }, function () {
            commit('pending', false);
        });
    },
    async setSearch({ commit }, value) {
        await commit('search', value);
    },
    async setForm({ commit }, data) {
        await commit('form', data);
    },
    async setRequest({ commit }, data) {
        await commit('request', data);
    },
    async fetchReload({ commit, state }, data) {
        const self = this;
        await self.dispatch('app/table/base/fetchListWithoutPending', {
            model: state.model,
            request: state.request,
        });
    },
    async clearForm({ commit, state }, data) {
        const self = this;
        let value = {};
        Object.keys(state.form).map((key) => {
            value[key] = null;
        });
        await commit('form', value);
        self.dispatch('app/table/base/setRequest', value);
        self.dispatch('app/table/base/fetchList', {
            model: state.model,
        });
    },
};

const mutations = {
    locale: (state, val) => {
        state.locale = val;
    },
    pending: (state, val) => {
        if (state.withPending) {
            state.pending = val;
        }
        if (!val) {
            state.pending = val;
            state.withPending = true;
        }
    },
    data: (state, data) => {
        state.data = data;
    },
    form: (state, data) => {
        state.form = data;
    },
    formRoute: (state, data) => {
        state.formRoute = serialize.urlToForm(data);
    },
    request: (state, data) => {
        state.request = serialize.requestNormalize(data);
    },
    fields: (state, data) => {
        state.fields = data;
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
    loaded: (state, data) => {
        state.loaded = data;
    },
    errors: (state, data) => {
        state.errors = data;
    },
    model: (state, val) => {
        state.model = val;
    },
    search: (state, val) => {
        state.search = val;
    },
    withPending: (state, val) => {
        state.withPending = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
