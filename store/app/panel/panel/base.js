import api from "../../../../api";
import serialize from "../../../../plugins/common/serialize";
import beforeFetch from "../../../../plugins/common/beforeFetch";
import axiosStore from "../../../../plugins/common/axiosStore";

const state = () => ({
    locale: 'ru',
    placement: 'right',
    opened: false,
    model: null, // users, articles
    item: null, // {} active object from table
    tab: null, // {} active tab
    panel: null, // {} panel
    role: null, // edit, create, show
    tabActive: {
        key: 0,
    },
    form: {},
    errors: {},
    pending: false,
    loading: false,
    withoutLoading: false,
    fields: null, // {}
    fieldsLoading: false,
    created: false,
    updated: false,
});

const getters = {
    locale(state) {
        return state.locale;
    },
    placement(state) {
        return state.placement;
    },
    opened(state) {
        return state.opened;
    },
    role(state) {
        return state.role;
    },
    model(state) {
        return state.model;
    },
    item(state) {
        return state.item;
    },
    form(state) {
        return state.form;
    },
    errors(state) {
        return state.errors;
    },
    fields(state) {
        return state.fields;
    },
    fieldsLoading(state) {
        return state.fieldsLoading;
    },
    pending(state) {
        return state.pending;
    },
    loading(state) {
        return state.loading;
    },
    withoutLoading(state) {
        return state.withoutLoading;
    },
    tabActive(state) {
        return state.tabActive;
    },
    tab(state) {
        return state.tab;
    },
    panel(state) {
        return state.panel;
    },
    created(state) {
        return state.created;
    },
    updated(state) {
        return state.updated;
    },
};

const actions = {

    /**
     *
     * @param commit
     * @param {string} locale
     */
    setLocale({ commit }, locale) {
        commit('locale', locale);
    },

    /**
     *
     * @param commit
     * @param state
     * @param {{model: string, locale: ?string, release_id: int, panel: string}} data
     * @returns {Promise<void>}
     */
    async fetchFields({ commit, state }, data) {
        const self = this;
        commit('item', null);
        commit('errors', {});
        commit('fields', null);
        commit('loading', true);
        commit('model', data.model);
        commit('form', {});
        const url = beforeFetch.getPanelFieldsUrl(api.panel.fields, data.model);
        const query = {};
        // if (data.locale !== undefined) {
        //     commit('locale', data.locale);
        // }
        // if (state.locale !== 'ru') {
        //     query.locale = state.locale;
        // }
        query.locale = 'ru';
        if (data.release_id !== undefined) {
            query.release_id = data.release_id;
        }
        if (data.panel !== undefined) {
            query.panel = data.panel;
        }
        await axiosStore.init(self.$axios).get(url, query, function (result) {
            commit('loading', false);
            commit('fields', result.fields);
            self.dispatch('app/panel/panel/base/setTabActiveIndex', 0);
        }, function () {
            commit('loading', false);
        });
    },

    /**
     *
     * @param commit
     * @param state
     * @returns {Promise<void>}
     */
    async fetchReloadFields({ commit, state }) {
        const self = this;
        commit('fieldsLoading', true);
        const url = beforeFetch.getPanelFieldsUrl(api.panel.fields, state.model);
        const query = serialize.requestNormalize(state.form);
        await axiosStore.init(self.$axios).get(url, query, function (result) {
            commit('fieldsLoading', false);
            commit('fields', result.fields);
            result.fields.tabs.forEach((tab) => {
                tab.data.forEach((item) => {
                    if (item.value !== undefined) {
                        state.form[item.key] = item.value;
                    }
                });
            });
        }, function (result) {
            commit('fieldsLoading', false);
        });
    },

    /**
     *
     * @param commit
     * @param state
     * @param {{id: int}} data
     * @returns {Promise<void>}
     */
    async fetchReloadEditFields({ commit, state }, data) {
        const self = this;
        commit('fieldsLoading', true);
        const url = beforeFetch.getPanelFieldsUrl(api.panel.fields, state.model);
        let query = serialize.requestNormalize(state.form);
        query.item_id = data.id;
        if (state.locale !== 'ru') {
            query.locale = state.locale;
        }
        await axiosStore.init(self.$axios).get(url, query, function (result) {
            commit('fieldsLoading', false);
            commit('fields', result.fields);
            result.fields.tabs.forEach((tab) => {
                tab.data.forEach((item) => {
                    if (item.value !== undefined) {
                        state.form[item.key] = item.value;
                    }
                });
            });
        }, function (result) {
            commit('fieldsLoading', false);
        });
    },

    /**
     *
     * @param commit
     * @param state
     * @returns {Promise<void>}
     */
    async fetchUpdate({ commit, state }) {
        const self = this;
        const item = state.item;
        const form = state.form;
        commit('pending', true);
        commit('updated', false);
        commit('errors', {});
        const url = beforeFetch.getPanelUpdateUrl(api.panel.update, state.model, item.id);
        const save = serialize.panelForm(serialize.panelFormByFields(form, state.fields.tabs));
        save.locale = state.locale;
        await axiosStore.init(self.$axios).post(url, save, function (result) {
            commit('pending', false);
            commit('updated', true);
            if (result.success && result.message !== undefined) {
                self.$toast.success(result.message);
            }
            if (state.model === 'users') {
                self.dispatch('user/base/fetchUser');
            }
        }, function (result) {
            commit('pending', false);
            if (result.errors !== undefined) {
                commit('errors', result.errors);
            }
        });
    },

    /**
     *
     * @param commit
     * @param state
     * @param {object} form
     * @returns {Promise<void>}
     */
    async fetchUpdateSubmit({ commit, state }, form) {
        const self = this;
        const item = state.item;
        commit('pending', true);
        commit('updated', false);
        commit('errors', {});
        const url = beforeFetch.getPanelUpdateUrl(api.panel.update, state.model, item.id);
        const save = serialize.panelForm(serialize.panelFormByFields(form, state.fields.tabs));
        save.locale = state.locale;
        await axiosStore.init(self.$axios).post(url, save, function (result) {
            commit('pending', false);
            commit('updated', true);
            if (result.success && result.message !== undefined) {
                self.$toast.success(result.message);
            }
            self.dispatch('app/panel/panel/base/fetchReloadEditFields', {
                id: item.id,
            });
        }, function (result) {
            commit('pending', false);
            if (result.errors !== undefined) {
                commit('errors', result.errors);
            }
        });
    },

    /**
     *
     * @param commit
     * @param state
     * @param data
     * @returns {Promise<void>}
     */
    async fetchStore({ commit, state }, data) {
        const self = this;
        const form = Object.assign({}, state.form, data);
        commit('pending', true);
        commit('created', false);
        commit('errors', {});
        const url = beforeFetch.getPanelStoreUrl(api.panel.store, state.model);
        const save = serialize.panelForm(serialize.panelFormByFields(form, state.fields.tabs));
        save.locale = 'ru';
        await axiosStore.init(self.$axios).post(url, save, function (result) {
            commit('pending', false);
            commit('created', true);
            if (result.success && result.message !== undefined) {
                self.$toast.success(result.message);
            }
        }, function (result) {
            commit('pending', false);
            if (result.errors !== undefined) {
                commit('errors', result.errors);
            }
        });
    },

    /**
     *
     * @param commit
     * @param state
     * @param data
     * @returns {Promise<void>}
     */
    async fetchShow({ commit, state }, data) {
        const self = this;
        commit('item', null);
        commit('errors', {});
        commit('fields', null);
        commit('loading', true);
        commit('model', data.model);
        commit('form', {});
        const url = beforeFetch.getPanelShowUrl(api.panel.show, data.model, data.id);
        let query = {};
        if (data.locale !== undefined) {
            commit('locale', data.locale);
        }
        if (state.locale !== 'ru') {
            query.locale = state.locale;
        }
        await axiosStore.init(self.$axios).get(url, query, function (result) {
            commit('loading', false);
            commit('item', result.data);
            commit('formMerge', {
                id: result.data.id,
            });
            commit('fields', result.fields);
            self.dispatch('app/panel/panel/base/setTabActiveIndex', 0);
        }, function (result) {
            commit('loading', false);
        });
    },

    /**
     *
     * @param commit
     * @param state
     * @param {{model: string, id: int, tab: ?string, panel: ?string, locale: ?string}} data
     * @returns {Promise<void>}
     */
    async fetchEdit({ commit, state }, data) {
        const self = this;
        commit('item', null);
        commit('errors', {});
        commit('fields', null);
        commit('loading', true);
        commit('model', data.model);
        commit('form', {});
        const url = beforeFetch.getPanelEditUrl(api.panel.edit, data.model, data.id);
        let query = {};
        if (data.tab !== undefined) {
            commit('tab', data.tab);
            query.tab = data.tab;
        }
        if (data.panel !== undefined) {
            commit('panel', data.panel);
            query.panel = data.panel;
        }
        if (data.locale !== undefined) {
            commit('locale', data.locale);
        }
        if (state.locale !== 'ru') {
            query.locale = state.locale;
        }
        await axiosStore.init(self.$axios).get(url, query, function (result) {
            commit('loading', false);
            commit('item', result.data);
            commit('formMerge', {
                id: result.data.id,
            });
            commit('fields', result.fields);
            self.dispatch('app/panel/panel/base/setTabActiveIndex', 0);
        }, function (result) {
            commit('loading', false);
        });
    },

    /**
     *
     * @param commit
     * @param state
     * @returns {Promise<void>}
     */
    async fetchReloadItem({ commit, state }) {
        const self = this;
        const url = beforeFetch.getPanelEditUrl(api.panel.edit, state.model, state.item.id);
        const query = {};
        if (state.locale !== 'ru') {
            query.locale = state.locale;
        }
        if (state.tab !== null) {
            query.tab = state.tab;
        }
        await axiosStore.init(self.$axios).get(url, query, function (result) {
            commit('item', result.data);
        });
    },
    setOpened({ commit }, value) {
        commit('opened', value);
    },
    setRole({ commit }, value) {
        commit('role', value);
    },
    setItem({ commit }, value) {
        commit('item', value);
    },
    setSort({ commit }, value) {
        commit('sort', value);
    },
    setModel({ commit }, value) {
        commit('model', value);
    },
    setPlacement({ commit }, value) {
        commit('placement', value);
    },
    setTabActive({ commit }, value) {
        commit('tabActive', value);
    },
    setTabActiveIndex({ commit, state }, index) {
        const tab = Object.assign({}, state.fields.tabs[index], {
            key: index,
            data: [],
        });
        commit('tabActive', tab);
    }
};

const mutations = {
    locale: (state, data) => {
        state.locale = data;
    },
    placement: (state, data) => {
        state.placement = data;
    },
    opened: (state, val) => {
        state.opened = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    loading: (state, val) => {
        if (state.withoutLoading && !val) {
            state.withoutLoading = false;
        }
        if (!state.withoutLoading) {
            state.loading = val;
        }
    },
    withoutLoading: (state, val) => {
        state.withoutLoading = val;
    },
    model: (state, val) => {
        state.model = val;
    },
    item: (state, data) => {
        state.item = data;
    },
    form: (state, data) => {
        state.form = data;
    },
    formMerge: (state, data) => {
        state.form = Object.assign({}, state.form, data);
    },
    errors: (state, data) => {
        state.errors = data;
    },
    fields: (state, data) => {
        state.fields = data;
    },
    fieldsLoading: (state, data) => {
        state.fieldsLoading = data;
    },
    role: (state, value) => {
        state.role = value;
    },
    tab: (state, value) => {
        state.tab = value;
    },
    panel: (state, value) => {
        state.panel = value;
    },
    tabActive: (state, data) => {
        state.tabActive = data;
    },
    created: (state, val) => {
        state.created = val;
    },
    updated: (state, val) => {
        state.updated = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
