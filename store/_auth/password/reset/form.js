import response from '../../../../plugins/common/response';
import api from '../../../../api';

const state = () => ({
    pending: false,
    form: {},
    errors: {},
});

const getters = {
    pending(state) {
        return state.pending;
    },
    form(state) {
        return state.form;
    },
    errors(state) {
        return state.errors;
    },
};

const actions = {
    async fetchReset({ commit }, data) {
        const self = this;
        commit('pending', true);
        commit('errors', {});
        await self.$axios.post(api.auth.passwordReset, data)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                if (result.data.token !== undefined) {
                    self.$auth.setUserToken(result.data.token).then(() => {
                        if (self.$router.history.current.path !== '/auth/password/reset') {
                            window.location.href = self.$router.history.current.path;
                        } else {
                            window.location.href = self.$router.history.current.path;
                            //self.$router.push('/');
                            //self.dispatch('user/echo/setNeedConnection', true);
                        }
                    });
                }
                if (result.success && result.message !== undefined) {
                    self.$toast.success(result.message);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                    if (result.message !== undefined && result.status_code !== 422) {
                        self.$toast.error(result.message);
                    }
                }
            });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    form: (state, val) => {
        state.form = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
