import seo from '../../common/seo';
import crpyto from '../../common/crpyto';

export default {

    /**
     * i18nRoute.index()
     */
    i18nRoute() {
        const self = this;
        const index = () => self.localePath('/');
        const my = () => self.localePath('/my');
        const about = () => self.localePath('/about');
        const terms = () => self.localePath('/terms');
        const rules = () => self.localePath('/rules');
        const page = item => self.localePath('/' + item.slug);
        const policy = () => self.localePath('/privacy-policy');
        const authRegister = () => self.localePath('/auth/register');
        const authLogin = () => self.localePath('/auth/login');
        const authLogout = () => self.localePath('/auth/logout');
        const authPasswordEmail = () => self.localePath('/auth/password/email');
        const personalNewArticle = () => self.localePath('/personal/articles/create?type=article');
        const personalNewEvent = () => self.localePath('/personal/articles/create?type=event');
        const personalSettingsAccount = () => self.localePath('/personal/settings/account');
        const personalSettingsAccounts = () => self.localePath('/personal/settings/accounts');
        const personalSettingsPassword = () => self.localePath('/personal/settings/password');
        const personalSettingsNotifications = () => self.localePath('/personal/settings/notifications');
        const personalSettingsCareer = () => self.localePath('/personal/settings/career');
        const usersArticles = user => self.localePath(`/u/${user.id}-${user.slug}`);
        const usersComments = user => self.localePath(`/u/${user.id}-${user.slug}/comments`);
        const usersSubscriptions = user => self.localePath(`/u/${user.id}-${user.slug}/subscriptions`);
        const usersSubscribers = user => self.localePath(`/u/${user.id}-${user.slug}/subscribers`);
        const usersDetails = user => self.localePath(`/u/${user.id}-${user.slug}/details`);
        const userArticles = () => self.localePath('/u/123123-user-rage');
        const userDrafts = () => self.localePath('/u/123123-user-rage/drafts');
        const userComments = () => self.localePath('/u/123123-user-rage/comments');
        const userRead = () => self.localePath('/u/123123-user-rage/read');
        const userFavorites = () => self.localePath('/u/123123-user-rage/favorites');
        const personalArticles = () => self.localePath('/personal');
        const personalDrafts = () => self.localePath('/personal/drafts');
        const personalComments = () => self.localePath('/personal/comments');
        const personalRead = () => self.localePath('/personal/read');
        const personalWatches = () => self.localePath('/personal/watches');
        const personalFavorites = () => self.localePath('/personal/favorites');
        const personalGroups = () => self.localePath('/personal/groups');
        const personalGroup = (group) => self.localePath(`/${group.slug}`);
        const personalTags = () => self.localePath('/personal/tags');
        const personalDetails = () => self.localePath('/personal/details');
        const personalModerationWaiting = () => self.localePath('/personal/moderation/waiting');
        const personalModerationPublishes = () => self.localePath('/personal/moderation/publishes');
        const messages = () => self.localePath('/m');
        const personalGroupArticles = (group) => {
            return self.localePath(`/personal/groups/${group.id}/articles`);
        };
        const article = (item) => {
            return self.localePath(`/articles/${item.id}/${item.slug}`);
        };
        const articlePrivate = (item, email) => {
            const token = crpyto.encrypt(email);
            return self.localePath(`/articles/${item.id}/${item.slug}?token=${token}`);
        };
        const articleUrl = (item) => {
            return process.env.APP_URL + self.localePath(`/articles/${item.id}/${item.slug}`);
        };
        const category = (item) => {
            return self.localePath(`/${item.slug}`);
        };
        const group = (item) => {
            return self.localePath(`/${item.slug}`);
        };
        const userArticlePreview = (item) => {
            return self.localePath(`/personal/articles/${item.id}/${item.slug}`);
        };
        const articleComments = (item) => {
            return self.localePath(`/articles/${item.id}/${item.slug}#comments`);
        };
        const userArticlesCreate = (type) => {
            return self.localePath(`/personal/articles/create?type=${type}`);
        };
        const userChats = () => {
            return self.localePath('/m');
        };
        const userChat = (chat) => {
            return self.localePath(`/m/${chat.id}`);
        };
        const userArticlesCreateArticle = () => {
            return userArticlesCreate('article');
        };
        const userArticlesCreateEvent = () => {
            return userArticlesCreate('event');
        };
        const userArticlesEdit = (item) => {
            return self.localePath(`/personal/articles/${item.id}/edit`);
        };
        const userGroupsEdit = (item) => {
            return self.localePath(`/personal/groups/${item.id}/edit`);
        };
        const searchTag = (item) => {
            return self.localePath(`/search?tags=${item.id}`);
        };
        const userGroupTabMain = item => self.localePath(`/personal/groups/${item.id}/main`);
        const userGroupTabMembers = item => self.localePath(`/personal/groups/${item.id}/members`);
        const userGroupTabMembersAdd = item => self.localePath(`/personal/groups/${item.id}/members-add`);

        return {
            index,
            my,
            authLogin,
            authRegister,
            authLogout,
            authPasswordEmail,
            personalSettingsAccount,
            personalGroup,
            personalSettingsAccounts,
            personalSettingsPassword,
            personalSettingsNotifications,
            personalSettingsCareer,
            userArticles,
            userComments,
            userRead,
            userFavorites,
            userDrafts,
            personalArticles,
            personalModerationWaiting,
            personalModerationPublishes,
            personalGroups,
            personalTags,
            personalDrafts,
            personalComments,
            personalRead,
            personalFavorites,
            personalDetails,
            personalWatches,
            personalGroupArticles,
            article,
            articleUrl,
            articleComments,
            articlePrivate,
            userArticlesEdit,
            userArticlesCreateEvent,
            userArticlesCreateArticle,
            usersArticles,
            usersComments,
            usersSubscriptions,
            usersSubscribers,
            usersDetails,
            userArticlePreview,
            group,
            userChats,
            userChat,
            category,
            messages,
            userGroupsEdit,
            userGroupTabMain,
            userGroupTabMembers,
            userGroupTabMembersAdd,
            searchTag,
            about,
            page,
            terms,
            rules,
            policy,
            personalNewArticle,
            personalNewEvent,
        };
    },

    /**
     *
     * @returns {{journal: ((function(*=): (*))|*), release: (function(*): *), article: (function(*): *)}}
     */
    i18nUrl() {
        const self = this;
        const journal = (item) => {
            if (item !== undefined && parseInt(item.id) !== parseInt(process.env.APP_JOURNAL_ID)) {
                return process.env.APP_URL + self.localePath(`/journals/${item.id}/${item.slug}`);
            }
            return process.env.APP_URL + self.localePath('/');
        };
        const release = (item) => {
            return process.env.APP_URL + self.localePath(`/releases/${item.id}/${item.slug}`);
        };
        const article = (item) => {
            return process.env.APP_URL + self.localePath(`/articles/${item.id}/${item.slug}`);
        };
        return {
            journal,
            article,
            release,
        };
    },

    /**
     *
     */
    i18nTitle() {
        const self = this;
        const index = () => self.$t('route.index');
        const my = () => self.$t('route.my');
        const feed = () => self.$t('route.feed');
        const authLogin = () => self.$t('route.auth.login');
        const authRegister = () => self.$t('route.auth.register');
        const personalSettingsAccount = () => self.$t('route.personal.settings.account');
        const personalSettingsPassword = () => self.$t('route.personal.settings.password');
        const personalSettingsNotifications = () => self.$t('route.personal.settings.notifications');
        const personalSettingsAccounts = () => self.$t('route.personal.settings.accounts');
        const about = () => self.$t('route.about');
        const terms = () => self.$t('route.terms');
        const policy = () => self.$t('route.policy');
        const rules = () => self.$t('route.rules');
        const page = item => item.title;

        return {
            index,
            my,
            feed,
            authLogin,
            authRegister,
            personalSettingsAccount,
            personalSettingsPassword,
            personalSettingsNotifications,
            personalSettingsAccounts,
            about,
            page,
            terms,
            policy,
            rules,
        };
    },

    i18nSeo() {
        const self = this;
        const index = () => {
            return seo.seoOther({
                title: self.$t('route.index'),
                url: null,
            });
        };
        const my = () => {
            return seo.seoOther({
                title: self.$t('route.my'),
                url: '/my',
            });
        };
        const search = () => {
            return seo.seoOther({
                title: self.$t('route.search'),
                url: '/search',
            });
        };
        const chats = () => {
            return seo.seoOther({
                title: self.$t('route.chats'),
                url: '/m',
            });
        };
        const chat = (item) => {
            if (item === null) {
                return seo.seoOther({
                    title: self.$t('route.chats'),
                    url: '/m',
                });
            }
            return seo.seoOther({
                title: item.title,
                url: `/m/${item.id}`,
            });
        };
        const personalArticles = () => {
            return seo.seoOther({
                title: self.$t('route.personal.articles'),
                url: '/personal',
            });
        };
        const personalDrafts = () => {
            return seo.seoOther({
                title: self.$t('route.personal.drafts'),
                url: '/personal/drafts',
            });
        };
        const personalModerationWaiting = () => {
            return seo.seoOther({
                title: self.$t('route.personal.moderation.waiting'),
                url: '/personal/moderation/waiting',
            });
        };
        const personalModerationPublishes = () => {
            return seo.seoOther({
                title: self.$t('route.personal.moderation.publishes'),
                url: '/personal/moderation/publishes',
            });
        };
        const personalComments = () => {
            return seo.seoOther({
                title: self.$t('route.personal.comments'),
                url: '/personal/comments',
            });
        };
        const personalFavorites = () => {
            return seo.seoOther({
                title: self.$t('route.personal.favorites'),
                url: '/personal/favorites',
            });
        };
        const personalDetails = () => {
            return seo.seoOther({
                title: self.$t('route.personal.details'),
                url: '/personal/details',
            });
        };
        const personalWatches = () => {
            return seo.seoOther({
                title: self.$t('route.personal.watches'),
                url: '/personal/watches',
            });
        };
        const personalSettingsAccount = () => {
            return seo.seoOther({
                title: self.$t('route.personal.settings.account'),
                url: '/personal/settings/account',
            });
        };
        const personalSettingsPassword = () => {
            return seo.seoOther({
                title: self.$t('route.personal.settings.password'),
                url: '/personal/settings/password',
            });
        };
        const personalSettingsNotifications = () => {
            return seo.seoOther({
                title: self.$t('route.personal.settings.notifications'),
                url: '/personal/settings/notifications',
            });
        };
        const personalSettingsAccounts = () => {
            return seo.seoOther({
                title: self.$t('route.personal.settings.accounts'),
                url: '/personal/settings/accounts',
            });
        };
        const personalGroups = () => {
            return seo.seoOther({
                title: self.$t('route.personal.groups'),
                url: '/personal/groups',
            });
        };
        const personalTags = () => {
            return seo.seoOther({
                title: self.$t('route.personal.tags'),
                url: '/personal/tags',
            });
        };
        const authRegister = () => {
            return seo.seoOther({
                title: self.$t('auth.register.title'),
                url: '/auth/register',
            });
        };
        const authLogin = () => {
            return seo.seoOtherAuth({
                title: self.$t('auth.login.title'),
                url: '/auth/login',
            });
        };
        const authYandexCallback = () => {
            return seo.seoOtherAuth({
                title: self.$t('auth.yandex.callback'),
                url: '/auth/yandex/callback',
            });
        };
        const authVkCallback = () => {
            return seo.seoOtherAuth({
                title: self.$t('auth.vk.callback'),
                url: '/auth/vk/callback',
            });
        };
        const authFacebookCallback = () => {
            return seo.seoOtherAuth({
                title: self.$t('auth.facebook.callback'),
                url: '/auth/facebook/callback',
            });
        };
        const authGoogleCallback = () => {
            return seo.seoOtherAuth({
                title: self.$t('auth.google.callback'),
                url: '/auth/google/callback',
            });
        };
        const authResetPassword = () => {
            return seo.seoOther({
                title: self.$t('auth.password.reset.title'),
                url: '/auth/password/reset',
            });
        };
        const authEmailPassword = () => {
            return seo.seoOther({
                title: self.$t('auth.password.email.title'),
                url: '/auth/password/email',
            });
        };

        return {
            index,
            my,
            search,
            chat,
            chats,
            authRegister,
            authLogin,
            authYandexCallback,
            authVkCallback,
            authFacebookCallback,
            authGoogleCallback,
            authResetPassword,
            authEmailPassword,
            personalArticles,
            personalDrafts,
            personalModerationWaiting,
            personalModerationPublishes,
            personalDetails,
            personalComments,
            personalFavorites,
            personalWatches,
            personalSettingsAccount,
            personalSettingsPassword,
            personalSettingsNotifications,
            personalSettingsAccounts,
            personalGroups,
            personalTags,
        };
    }
};
