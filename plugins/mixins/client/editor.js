export default {

    /**
     * i18nRoute.index()
     */
    editorActions() {
        const self = this;
        const i18n = () => {
            return {
                messages: {
                    ui: {
                        blockTunes: {
                            toggler: {
                                'Click to tune': self.$t('editor.basic.click-tune'),
                                'or drag to move': self.$t('editor.basic.or-move')
                            },
                        },
                        inlineToolbar: {
                            converter: {
                                'Convert to': self.$t('editor.basic.convert-to')
                            }
                        },
                        toolbar: {
                            toolbox: {
                                Add: self.$t('editor.basic.add')
                            }
                        }
                    },
                    tools: {
                        AnyButton: {
                            'Button Text': self.$t('editor.basic.text'),
                            'Link Url': self.$t('editor.basic.link'),
                            Set: self.$t('editor.basic.set'),
                            'Default Button': self.$t('editor.basic.button-default'),
                        },
                        image: {
                            Image: self.$t('editor.basic.image'),
                            'Upload an image': self.$t('editor.basic.upload-image'),
                            'Select an Image': self.$t('editor.basic.select-image'),
                            'With border': self.$t('editor.basic.image-border'),
                            'Stretch image': self.$t('editor.basic.image-stretch'),
                            'With background': self.$t('editor.basic.image-background'),
                        },
                        person: {
                            Image: self.$t('editor.basic.image'),
                            'Upload an image': self.$t('editor.basic.upload-image'),
                            'Select an Image': self.$t('editor.basic.select-image'),
                            'With border': self.$t('editor.basic.image-border'),
                            'Stretch image': self.$t('editor.basic.image-stretch'),
                            'With background': self.$t('editor.basic.image-background'),
                        },
                        quote: {
                            'Enter a quote': self.$t('editor.basic.quote.enter-quote'),
                            'Enter a caption': self.$t('editor.basic.quote.enter-caption'),
                            'Left alignment': self.$t('editor.basic.quote.enter-alignment'),
                        },
                        list: {
                            Unordered: self.$t('editor.basic.list.unordered'),
                            Ordered: self.$t('editor.basic.list.ordered'),
                        },
                        code: {
                            'Enter a code': self.$t('editor.basic.enter-code'),
                        },
                        table: {
                            'Insert column before': self.$t('editor.basic.insert-column-before'),
                            'Insert column after': self.$t('editor.basic.insert-column-after'),
                            'Insert row before': self.$t('editor.basic.insert-row-before'),
                            'Insert row after': self.$t('editor.basic.insert-row-after'),
                            'Delete row': self.$t('editor.basic.delete-row'),
                            'Delete column': self.$t('editor.basic.delete-column'),
                        },
                        linkTool: {
                            Link: self.$t('editor.basic.link'),
                        },
                        carousel: {
                            'Add Image': self.$t('editor.basic.upload-image'),
                            'Caption...': self.$t('editor.basic.caption'),
                        },
                        embed: {
                            'Enter a caption': self.$t('editor.basic.caption'),
                        }
                    },
                    toolNames: {
                        Text: self.$t('editor.tool.paragraph'),
                        Heading: self.$t('editor.tool.heading'),
                        Button: self.$t('editor.tool.button'),
                        Alert: self.$t('editor.tool.alert'),
                        Attaches: self.$t('editor.tool.attaches'),
                        Audio: self.$t('editor.tool.audio'),
                        Video: self.$t('editor.tool.video'),
                        Carousel: self.$t('editor.tool.carousel'),
                        Image: self.$t('editor.tool.image'),
                        Person: self.$t('editor.tool.person'),
                        Incut: self.$t('editor.tool.incut'),
                        List: self.$t('editor.tool.list'),
                        Warning: self.$t('editor.tool.warning'),
                        Checklist: self.$t('editor.tool.checklist'),
                        Quote: self.$t('editor.tool.quote'),
                        Code: self.$t('editor.tool.code'),
                        Delimiter: self.$t('editor.tool.delimiter'),
                        Asterisks: self.$t('editor.tool.asterisks'),
                        'Raw HTML': self.$t('editor.tool.html'),
                        Table: self.$t('editor.tool.table'),
                        Link: self.$t('editor.tool.link'),
                        Marker: self.$t('editor.tool.marker'),
                        Bold: self.$t('editor.tool.bold'),
                        Italic: self.$t('editor.tool.italic'),
                        InlineCode: self.$t('editor.tool.inline-code'),
                        Underline: self.$t('editor.tool.underline'),
                        Supscript: self.$t('editor.tool.supscript'),
                        Tooltip: self.$t('editor.tool.tooltip'),
                    },
                    /**
                     * Section allows to translate Block Tunes
                     */
                    blockTunes: {
                        /**
                         * Each subsection is the i18n dictionary that will be passed to the corresponded Block Tune plugin
                         * The name of a plugin should be equal the name you specify in the 'tunes' section for that plugin
                         *
                         * Also, there are few internal block tunes: "delete", "moveUp" and "moveDown"
                         */
                        delete: {
                            Delete: self.$t('editor.basic.delete')
                        },
                        moveUp: {
                            'Move up': self.$t('editor.basic.move-up')
                        },
                        moveDown: {
                            'Move down': self.$t('editor.basic.move-down')
                        }
                    },
                },
            };
        };
        return {
            i18n,
        };
    }
};
