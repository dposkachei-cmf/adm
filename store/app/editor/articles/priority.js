import response from '../../../../plugins/common/response';
import api from './../../../../api';
import axios from "./../../../../plugins/axios";

const state = () => ({
    published: false,
    pending: false,
});

const getters = {
    published(state) {
        return state.published;
    },
    pending(state) {
        return state.pending;
    },
};

// actions
const actions = {
    async fetchPriority({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        let url = api.editor.articles.priority;
        url = url.replace(':id', data.id);
        url = url.replace(':model', data.model);
        await axios.post(url, {
            article_id: data.article_id,
            value: data.value,
        })
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                self.dispatch('app/panel/base/fetchReloadItem');
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                }
            });
    },
};

// mutations
const mutations = {
    published: (state, val) => {
        state.published = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    pendingArray: (state, val) => {
        state.pendingArray = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
