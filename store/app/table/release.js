import api from "../../../api";
import beforeFetch from "../../../plugins/common/beforeFetch";
import axiosStore from "../../../plugins/common/axiosStore";

const state = () => ({
    pending: false,
    success: false,
});

const getters = {
    pending(state) {
        return state.pending;
    },
    success(state) {
        return state.success;
    },
};

const actions = {
    async fetchRelease({ commit }, data) {
        const self = this;
        commit('pending', true);
        commit('success', false);
        const url = beforeFetch.getUrlModelId(api.table.release, data.model, data.id);
        await axiosStore.init(self.$axios).post(url, {
            value: data.value === true ? 1 : 0,
        }, function (result) {
            commit('pending', false);
            commit('success', true);
            if (result.success && result.message !== undefined) {
                self.$toast.success(result.message);
            }
        }, function () {
            commit('pending', false);
        });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    success: (state, val) => {
        state.success = val;
        setTimeout(function () {
            state.success = false;
        }, 500);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
