const state = () => ({
    connected: false,
    needConnection: true,
    //
    chat: {
        needDisconnect: false,
        needConnection: false,
        items: [],
    },
    releaseLocked: [],
});

// getters
const getters = {
    connected(state) {
        return state.connected;
    },
    needConnection(state) {
        return state.needConnection;
    },
    chat(state) {
        return state.chat;
    },
    releaseLocked(state) {
        return state.releaseLocked;
    },
};

// actions
const actions = {
    async setConnected({ commit }, value) {
        await commit('connected', value);
    },
    async setNeedConnection({ commit }, value) {
        await commit('needConnection', value);
    },
    async setChatNeedConnection({ commit, state }, chats) {
        await commit('chatItems', chats);
        await commit('chatNeedConnection', true);
    },
    async setChatNeedDisconnect({ commit, state }) {
        //await commit('chatItems', []);
        await commit('chatNeedDisconnect', true);
    },
    setReleaseLocked({ commit }, data) {
        commit('releaseLocked', data);
    },
    setReleaseLockedAppend({ commit }, value) {
        commit('releaseLockedAppend', value);
    }
};

// mutations
const mutations = {
    connected: (state, val) => {
        state.connected = val;
    },
    needConnection: (state, val) => {
        state.needConnection = val;
    },
    chat: (state, val) => {
        state.chat = val;
    },
    chatNeedConnection: (state, val) => {
        state.chat.needConnection = val;
    },
    chatNeedDisconnect: (state, val) => {
        state.chat.needDisconnect = val;
    },
    chatItems: (state, data) => {
        state.chat.items = data;
    },
    releaseLocked: (state, data) => {
        state.releaseLocked = data;
    },
    releaseLockedAppend: (state, value) => {
        state.releaseLocked.push(value);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
