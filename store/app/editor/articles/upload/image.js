import api from "../../../../../api";
import axiosStore from "../../../../../plugins/common/axiosStore";

const state = () => ({
    success: false,
    pending: false,
    errors: {},
    closeModal: false,
});

const getters = {
    success(state) {
        return state.success;
    },
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
    closeModal(state) {
        return state.closeModal;
    },
};

const actions = {
    async fetchUploadTemplate({ commit, state }, data) {
        const self = this;
        commit('success', false);
        commit('pending', true);
        commit('errors', {});
        commit('closeModal', false);
        let url = api.editor.upload;
        url = url.replace(':model', data.get('model'));
        url = url.replace(':uid', data.get('uid'));
        await axiosStore.init(self.$axios).post(url, data, function (result) {
            commit('pending', false);
            commit('success', true);
            console.log(result.file.url);
            self.dispatch('app/editor/articles/store/setImage', result.file.url);
            if (result.success && result.message !== undefined) {
                console.log(result.message);
            }
            setTimeout(function () {
                commit('closeModal', true);
            }, 1000);
        }, function (result) {
            commit('pending', false);
            if (result.errors !== undefined) {
                commit('errors', result.errors);
            }
        });
    },
    async fetchUploadAndUpdate({ commit, state }, data) {
        const self = this;
        commit('success', false);
        commit('pending', true);
        commit('errors', {});
        commit('closeModal', false);
        let url = api.editor.article.image;
        url = url.replace(':model', data.model);
        url = url.replace(':id', data.id);
        await axiosStore.init(self.$axios).post(url, data, function (result) {
            commit('pending', false);
            commit('success', true);
            if (result.success && result.message !== undefined) {
                console.log(self.$toast.success(result.message));
            }
            setTimeout(function () {
                commit('closeModal', true);
            }, 1000);
        }, function (result) {
            commit('pending', false);
            if (result.errors !== undefined) {
                commit('errors', result.errors);
            }
        });
    },
};

const mutations = {
    success: (state, val) => {
        state.success = val;
        setTimeout(function () {
            state.success = false;
        }, 1500);
    },
    pending: (state, val) => {
        state.pending = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
    closeModal: (state, val) => {
        state.closeModal = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
