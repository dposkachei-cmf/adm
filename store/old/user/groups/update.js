import response from '../../../../plugins/common/response';
import api from '../../../../api';

const state = () => ({
    failed: false,
    success: false,
    pending: false,
    errors: {},
    form: {
        title: 'Заголовок',
        preview_description: 'Краткое описание',
        image: null,
        category_id: null,
    },
    formDefault: {
        id: null,
        title: 'Заголовок',
        preview_description: 'Краткое описание',
        category_id: null,
    }
});

const getters = {
    form(state) {
        return state.form;
    },
    formDefault(state) {
        return state.formDefault;
    },
    success(state) {
        return state.success;
    },
    failed(state) {
        return state.failed;
    },
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
    uid(state) {
        return state.uid;
    },
};

const actions = {
    setFormDefault({ commit, state }, data = {}) {
        commit('form', Object.assign({}, state.formDefault, data));
    },
    async fetchUpdate({ commit, state }, data) {
        const self = this;
        commit('success', false);
        commit('pending', true);
        commit('errors', {});
        const id = data.id;
        delete data.id;
        const save = data;
        const url = api.user.groups.update.replace(':id', id);
        await self.$axios.post(url, save)
            .then((res) => {
                const resp = response.success(res);
                commit('pending', false);
                commit('success', true);
                if (resp.success && resp.message !== undefined) {
                    self.$toast.success(resp.message);
                }
                self.dispatch('user/groups/show/fetchDataForce', {
                    id,
                });
            })
            .catch((error) => {
                if (error) {
                    commit('failed', true);
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                    if (result.message !== undefined && result.status_code !== 422) {
                        self.$toast.error(result.message);
                    }
                }
            });
    },
    setImage({ commit, state }, value) {
        state.form.image = value;
    }
};

const mutations = {
    success: (state, val) => {
        state.success = val;
        setTimeout(function () {
            state.success = false;
        }, 1500);
    },
    failed: (state, val) => {
        state.failed = val;
        setTimeout(function () {
            state.failed = false;
        }, 1500);
    },
    form: (state, val) => {
        state.form = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
    uid: (state, val) => {
        state.uid = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
