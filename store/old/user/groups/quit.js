import response from '../../../../plugins/common/response';
import api from '../../../../api';

const state = () => ({
    pending: false,
    pendingArray: [],
});

const getters = {
    pending(state) {
        return state.pending;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
};

const actions = {
    async fetchQuit({ state, commit }, data) {
        const self = this;
        commit('pending', true);
        commit('pendingArray', response.pendingAdd(state.pendingArray, data.id, 'group'));
        const url = api.user.groups.quit.replace(':id', data.id);
        await self.$axios.post(url)
            .then((res) => {
                commit('pending', false);
                commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, 'group'));
                self.$auth.fetchUser().then(function () {});
                self.dispatch('user/groups/list/fetchGroupsWithoutPending', data);
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                    commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, 'group'));
                }
            });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    pendingArray: (state, val) => {
        state.pendingArray = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
