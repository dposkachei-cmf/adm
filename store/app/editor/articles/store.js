import api from './../../../../api';
import serialize from "../../../../plugins/common/serialize";
import beforeFetch from "../../../../plugins/common/beforeFetch";
import axiosStore from "../../../../plugins/common/axiosStore";

const state = () => ({
    failed: false,
    success: false,
    pending: false,
    updated: false,
    updatedForce: false,
    uid: null,
    fields: null,
    model: null,
    locale: 'ru',
    changed: {},
    errors: {},
    form: {
        id: null,
        title: '',
        annotation: '',
        rubric: '',
        image: null,
        video: null,
        category: null,
        creator: null,
        //group_id: null,
        type: null,
        access_by: null,
        udk: null,
        doi: null,
        keywords: null,
        status: null,
        authors: null,
        books: null,
        pages: null,
        source_file: null,
        source_type: null,
        //commented: true,
        //access_all: true,
        //tagsArray: [],
        //tags: [],
        //date_rg: [],
        //start_at: null,
        //finish_at: null,
    },
    formDefault: {
        id: null,
        title: '',
        annotation: '',
        rubric: '',
        image: null,
        video: null,
        category: null,
        creator: null,
        //group_id: null,
        type: null,
        access_by: null,
        udk: null,
        doi: null,
        keywords: null,
        status: null,
        authors: null,
        books: null,
        pages: null,
        source_file: null,
        source_type: null,
        //commented: true,
        //access_all: true,
        //tagsArray: [],
        //tags: [],
        //date_rg: [],
        //start_at: null,
        //finish_at: null,
    },
    isSetFormDefault: false,
});

const getters = {
    form(state) {
        return state.form;
    },
    changed(state) {
        return state.changed;
    },
    formDefault(state) {
        return state.formDefault;
    },
    success(state) {
        return state.success;
    },
    failed(state) {
        return state.failed;
    },
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
    uid(state) {
        return state.uid;
    },
    model(state) {
        return state.model;
    },
    locale(state) {
        return state.locale;
    },
    fields(state) {
        return state.fields;
    },
    updated(state) {
        return state.updated;
    },
    updatedForce(state) {
        return state.updatedForce;
    },
    isSetFormDefault(state) {
        return state.isSetFormDefault;
    },
};

const actions = {
    setLocale({ commit }, locale) {
        commit('locale', locale);
    },
    setUpdated({ commit }) {
        commit('updated', true);
    },
    setUpdatedForce({ commit }) {
        commit('updatedForce', true);
    },
    setFields({ commit }, data) {
        commit('fields', data);
    },
    setFormDefault({ commit, state }, data = {}) {
        commit('isSetFormDefault', true);
        commit('form', Object.assign({}, state.formDefault, data));
        // когда после show сохраняются страницы и потом после обновления сбрасывается страница на первую
        setTimeout(function () {
            commit('isSetFormDefault', false);
        }, 300);
        console.log(state.formDefault);
        console.log(data);
        console.log(Object.assign({}, state.formDefault, data));
    },
    async fetchSave({ commit, state }, data) {
        const self = this;
        console.log(state.form);
        if (state.form.id === null) {
            //await self.dispatch('user/articles/store/fetchStore', data);
        } else {
            await self.dispatch('app/editor/articles/store/fetchUpdate', data);
        }
    },
    async fetchStore({ commit, state }, data) {
        const self = this;
        commit('success', false);
        commit('pending', true);
        commit('errors', {});
        const form = state.form;
        const query = Object.assign(form, data);
        const url = beforeFetch.getUrlModel(api.editor.article.store, state.model);
        query.locale = state.locale;
        await axiosStore.init(self.$axios).post(url, query, function (result) {
            commit('pending', false);
            commit('success', true);
            $router.push(`/${state.model}/${result.data.id}/edit`);
            if (result.success && result.message !== undefined) {
                self.$toast.success(result.message);
            }
        }, function (result) {
            commit('failed', true);
            commit('pending', false);
            if (result.errors !== undefined) {
                commit('errors', result.errors);
                if (Object.keys(result.errors).find(x => ['title', 'type', 'creator', 'category'].includes(x)) !== null) {
                    self.dispatch('app/sidebar/setActive', 'sidebar-article-settings');
                }
                if (result.errors.blocks !== undefined) {
                    result.errors.blocks.forEach((mes) => {
                        self.$toast.error(mes, {
                            duration: 5000,
                        });
                    });
                }
            }
            if (result.message !== undefined && result.status_code !== 422) {
                self.$toast.error(result.message);
            }
        });
    },
    async fetchUpdate({ commit, state }, data) {
        const self = this;
        commit('success', false);
        commit('pending', true);
        commit('errors', {});
        const form = Object.assign(state.form, data);
        const url = beforeFetch.getUrlModelId(api.editor.article.update, state.model, state.form.id);
        form.locale = state.locale;
        const query = serialize.editorForm(form);
        await axiosStore.init(self.$axios).post(url, query, function (result) {
            commit('pending', false);
            commit('success', true);
            //self.dispatch('app/editor/base/setBlocksMergeImages', resp.data);
            if (result.success && result.message !== undefined) {
                self.$toast.success(result.message);
            }
            if (result.data !== undefined && result.data.id !== undefined) {
                self.dispatch('app/editor/articles/show/setData', result.data);
            }
            // if (state.changed.authors !== undefined) {
            //     self.dispatch('app/editor/articles/store/reloadFields');
            //     commit('changed', {});
            // }
            // if (state.changed.books !== undefined) {
            //     self.dispatch('app/editor/articles/store/reloadFields');
            //     commit('changed', {});
            // }
            // if (state.changed.pages !== undefined && result.data.source.type === 'pdf') {
            //     if (result.data.source.pages !== null) {
            //         self.dispatch('app/editor/pdf/setShowPages', result.data.source.pages);
            //         self.dispatch('app/editor/pdf/setPage', result.data.source.pages[0]);
            //     } else {
            //         self.dispatch('app/editor/pdf/setShowPages', null);
            //     }
            //     commit('changed', {});
            // }
            // if (result.data.source.type === 'pdf' && result.data.source.url !== state.form.source_file) {
            //     // commit('formMerge', {
            //     //     pages: null,
            //     // });
            //     self.dispatch('app/editor/pdf/setUrl', result.data.source.url);
            //     if (result.data.source.pages !== null) {
            //         self.dispatch('app/editor/pdf/setShowPages', result.data.source.pages);
            //         self.dispatch('app/editor/pdf/setPage', result.data.source.pages[0]);
            //     } else {
            //         self.dispatch('app/editor/pdf/setShowPages', null);
            //     }
            // }
        }, function (result) {
            commit('failed', true);
            commit('pending', false);
            if (result.errors !== undefined) {
                commit('errors', result.errors);
                if (result.errors.title !== undefined || result.errors.type !== undefined || result.errors.annotation !== undefined || result.errors.keywords !== undefined) {
                    self.dispatch('app/sidebar/setActive', 'sidebar-article-settings');
                }
            }
            if (result.message !== undefined && result.status_code !== 422) {
                self.$toast.error(result.message);
            }
        });
    },
    async fetchGetUid({ commit, state }, data) {
        const self = this;
        commit('model', data.model);
        const url = beforeFetch.getUrlModel(api.editor.uid, data.model);
        const query = {};
        if (data.locale !== undefined) {
            query.locale = data.locale;
            commit('locale', data.locale);
        }
        if (data.article_id !== undefined) {
            query.article_id = data.article_id;
        }
        await axiosStore.init(self.$axios).post(url, query, function (result) {
            commit('uid', result.uid);
        });
    },
    async fetchFields({ commit, state }, data) {
        const self = this;
        const url = beforeFetch.getUrlModel(api.editor.fields, data.model);
        const query = {};
        if (data.locale !== undefined) {
            query.locale = data.locale;
        }
        if (data.article_id !== undefined) {
            query.article_id = data.article_id;
        }
        // if (data.release_id !== undefined) {
        //     query.release_id = data.release_id;
        // }
        await axiosStore.init(self.$axios).get(url, query, function (result) {
            commit('fields', result.data.fields);
            // if (result.data.source !== null && result.data.source.type === 'pdf' && result.data.source.url !== null) {
            //     self.dispatch('app/editor/base/setModePdf');
            //     self.dispatch('app/editor/pdf/setUrl', result.data.source.url);
            //     if (result.data.source.pages !== null) {
            //         self.dispatch('app/editor/pdf/setShowPages', result.data.source.pages);
            //         self.dispatch('app/editor/pdf/setPage', result.data.source.pages[0]);
            //     } else {
            //         self.dispatch('app/editor/pdf/setShowPages', null);
            //     }
            // }
        });
    },
    async fetchFieldsInitial({ commit, state }, data) {
        const self = this;
        commit('fields', null);
        await self.dispatch('app/editor/articles/store/fetchFields', data);
    },
    async reloadFields({ commit, state }) {
        const self = this;
        await self.dispatch('app/editor/articles/store/fetchFields', {
            model: state.model,
            article_id: state.form.id,
            locale: state.locale,
        });
    },
    setModel({ commit, state }, data) {
        commit('model', data.model);
    },
    setChanged({ commit }, data) {
        commit('changed', data);
    },
    setFormMerge({ commit, state }, data) {
        commit('form', Object.assign({}, state.form, data));
    },
    setFormSource({ commit, state }, data) {
        commit('form', Object.assign({}, state.form, {
            source_file: data.source,
            source_type: data.type,
        }));
    },
    setImage({ commit, state }, value) {
        state.form.image = value;
        state.form.video = null;
    },
    setVideo({ commit, state }, value) {
        state.form.image = null;
        state.form.video = value;
    }
};

const mutations = {
    success: (state, val) => {
        state.success = val;
        setTimeout(function () {
            state.success = false;
        }, 1500);
    },
    failed: (state, val) => {
        state.failed = val;
        setTimeout(function () {
            state.failed = false;
        }, 1500);
    },
    form: (state, val) => {
        state.form = val;
    },
    formMerge: (state, data) => {
        state.form = Object.assign({}, state.form, data);
    },
    pending: (state, val) => {
        state.pending = val;
    },
    fields: (state, data) => {
        state.fields = data;
    },
    errors: (state, val) => {
        state.errors = val;
    },
    uid: (state, val) => {
        state.uid = val;
    },
    model: (state, val) => {
        state.model = val;
    },
    locale: (state, val) => {
        state.locale = val;
    },
    changed: (state, val) => {
        state.changed = val;
    },
    isSetFormDefault: (state, val) => {
        state.isSetFormDefault = val;
    },
    updated: (state, val) => {
        state.updated = val;
        setTimeout(function () {
            state.updated = false;
        }, 500);
    },
    updatedForce: (state, val) => {
        state.updatedForce = val;
        setTimeout(function () {
            state.updatedForce = false;
        }, 500);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
