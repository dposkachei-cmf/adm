import moment from 'moment';
import serialize from '../../../plugins/common/serialize';
import response from '../../../plugins/common/response';

const state = () => ({
    loaded: false,
    reloaded: false,
    pending: false,
    data: [],
    pagination: {},
    request: {
        q: '',
        category_id: null,
        group_id: null,
        sort_by: 'date',
        page: 1,
        limit: 10,
        active_from: '',
        active_to: '',
        published: null,
    },
    form: {
        q: '',
        published: null,
        category_id: null,
        categoryObject: null,
        tagsArray: [],
        group_id: null,
        groupObject: null,
        sort_by: 'date',
        page: 1,
        limit: 10,
        published_value: null,
        published_rg: [],
    },
    options: {
        sort_by: {
            date: 'date',
            relevant: 'relevant',
        },
        published: {
            all: 'all',
            year: 'year',
            month: 'month',
            week: 'week',
        },
        groups: [],
        categories: [],
        tags: [],
    },
    errors: {},
});

const getters = {
    data(state) {
        return state.data;
    },
    pagination(state) {
        return state.pagination;
    },
    pending(state) {
        return state.pending;
    },
    form(state) {
        return state.form;
    },
    errors(state) {
        return state.errors;
    },
    request(state) {
        return state.request;
    },
    reloaded(state) {
        return state.reloaded;
    },
    loaded(state) {
        return state.loaded;
    },
};

const actions = {
    fetchSearchByAll({ commit, store, state }, data) {
        const self = this;
        const object = {
            q: data.q,
        };
        self.dispatch('search/fetchSearchRoute', object);
    },
    fetchSearchByJournal({ commit, store, state }, data) {
        const self = this;
        const object = {
            q: data.q,
        };
        self.dispatch('search/fetchSearchRoute', object);
    },
    fetchSearchOpenAccess({ commit, store, state }, data) {
        const self = this;
        const object = {
            //
        };
        self.dispatch('search/fetchSearch', object);
    },
    async fetchSearchBase({ commit, state }, data) {
        const self = this;
        commit('request', data);
        const request = state.request;
        if (data.page !== undefined) {
            request.page = data.page;
        }
        const string = serialize.query(request);
        commit('pending', true);
        await self.$axios.get('/search?' + string)
            .then((res) => {
                const resp = response.success(res);
                commit('pending', false);
                if (resp.success) {
                    commit('data', resp.data);
                }
                if (!state.loaded) {
                    commit('loaded', true);
                }
                if (!state.reloaded) {
                    commit('reloaded', false);
                }
                if (resp.pagination !== undefined) {
                    commit('pagination', resp.pagination);
                }
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                }
            });
    },
    async fetchSearchRoute({ commit, state }, data) {
        const self = this;
        commit('request', data);
        await self.$router.push({
            path: self.localePath('/search'),
            query: state.request,
        });
    },
    async fetchSearch({ commit, state }, data) {
        const self = this;
        await self.dispatch('search/fetchSearchRoute', data);
        await self.dispatch('search/fetchSearchBase', data);
    },
    async fetchSearchInitial({ commit, state }, data) {
        const self = this;
        commit('data', []);
        commit('loaded', false);
        await self.dispatch('search/fetchSearchRoute', data);
        await self.dispatch('search/fetchSearchBase', data);
    },
    async fetchSearchModal({ commit, state }, data) {
        const self = this;
        commit('data', []);
        commit('loaded', false);
        await self.dispatch('search/fetchSearchBase', data);
    },
    setForm({ commit, state }, data) {
        commit('form', data);
    },
    setReloaded({ commit, state }, data) {
        commit('reloaded', true);
    },
    setEmptySearchModal({ commit, state }, data) {
        commit('data', []);
        commit('loaded', false);
        commit('form', {
            q: '',
            sort_by: 'date',
            categoryObject: null,
            groupObject: null,
            tagsArray: [],
            published: 'all',
        });
    }
};

const mutations = {
    form: (state, val) => {
        state.form = val;
    },
    reloaded: (state, val) => {
        state.reloaded = val;
    },
    loaded: (state, val) => {
        state.loaded = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, data) => {
        if (data.length === 0) {
            state.data = [];
        } else {
            data.forEach(function (value) {
                state.data.push(value);
            });
        }
    },
    pagination: (state, val) => {
        state.pagination = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
    request: (state, val) => {
        console.log(val);
        const value = {};

        if (val.q !== undefined) {
            value.q = val.q;
        }
        if (value.q === '' || value.q === null || value.q === undefined) {
            value.q = '';
        }
        if (val.page !== undefined) {
            //value.page = val.page;
        }
        // category
        if (val.categoryObject !== undefined && val.categoryObject !== null) {
            if (val.categoryObject.id !== undefined) {
                value.category_id = val.categoryObject.id;
            } else {
                value.category_id = val.categoryObject;
            }
        }
        // group
        if (val.groupObject !== undefined && val.groupObject !== null) {
            if (val.groupObject.id !== undefined) {
                value.group_id = val.groupObject.id;
            } else {
                value.group_id = val.groupObject;
            }
        }
        // tags
        if (val.tagsArray !== undefined && val.tagsArray.length) {
            value.tags = [];
            val.tagsArray.forEach((tag) => {
                if (tag !== undefined) {
                    value.tags.push(tag.id);
                }
            });
        }
        // group
        if (val.sort_by !== undefined && val.sort_by !== null) {
            value.sort_by = val.sort_by;
        }
        // group
        if (val.published !== undefined && val.published !== null && val.published !== 'all') {
            value.published = val.published;
        }
        state.form.q = value.q;
        state.request = value;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
