import response from '../../../../plugins/common/response';
import api from '../../../../api';

// initial state
const state = () => ({
    pending: true,
    data: [],
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
};

// actions
const actions = {
    async fetchTags({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        await self.$axios.get(api.user.tags.index + '?extended=1')
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
