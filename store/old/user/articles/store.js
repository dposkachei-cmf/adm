import response from '../../../../plugins/common/response';
import api from '../../../../api';
import moment from 'moment';

const state = () => ({
    failed: false,
    success: false,
    pending: false,
    uid: null,
    errors: {},
    form: {
        id: null,
        title: '',
        preview_description: '',
        image: null,
        video: null,
        // https://www.youtube.com/embed/HtmLg0TQ2Pg
        // https://www.youtube.com/watch?v=HtmLg0TQ2Pg
        category_id: null,
        categoryObject: null,
        group_id: null,
        groupObject: null,
        type: null,
        commented: true,
        access_all: true,
        tagsArray: [],
        tags: [],
        date_rg: [],
        start_at: null,
        finish_at: null,
    },
    formDefault: {
        id: null,
        title: '',
        preview_description: '',
        image: null,
        video: null,
        category_id: null,
        categoryObject: null,
        group_id: null,
        groupObject: null,
        type: null,
        commented: true,
        access_all: true,
        tagsArray: [],
        tags: [],
        date_rg: [],
        start_at: null,
        finish_at: null,
    }
});

const getters = {
    form(state) {
        return state.form;
    },
    formDefault(state) {
        return state.formDefault;
    },
    success(state) {
        return state.success;
    },
    failed(state) {
        return state.failed;
    },
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
    uid(state) {
        return state.uid;
    },
};

const actions = {
    setFormValues({ commit, state }, data = {}) {
        commit('form', Object.assign({}, state.form, data));
    },
    setFormDefault({ commit, state }, data = {}) {
        commit('form', Object.assign({}, state.formDefault, data));
    },
    async fetchSave({ commit, state }, data) {
        const self = this;
        if (state.form.id === null) {
            //await self.dispatch('user/articles/store/fetchStore', data);
        } else {
            await self.dispatch('user/articles/store/fetchUpdate', data);
        }
    },
    async fetchStore({ commit, state }, data) {
        const self = this;
        commit('success', false);
        commit('pending', true);
        commit('errors', {});
        const form = state.form;
        const save = Object.assign(form, data);
        save.private = save.access_all === true
            ? 0
            : 1;
        save.can_commented = save.commented === true
            ? 1
            : 0;
        save.tags = [];
        save.tagsArray.forEach((tag) => {
            save.tags.push(tag.id);
        });
        await self.$axios.post(api.user.articles.store, save)
            .then((res) => {
                const resp = response.success(res);
                commit('pending', false);
                commit('success', true);
                self.$auth.fetchUser().then(function () {});
                self.$router.push(`/personal/articles/${resp.data.id}/edit`);
                //state.form.id = resp.data.id;
                if (resp.success && resp.message !== undefined) {
                    console.log(self.$toast.success(resp.message));
                }
            })
            .catch((error) => {
                if (error) {
                    commit('failed', true);
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                        if (result.errors.category_id !== undefined || result.errors.type !== undefined || result.errors.group_id !== undefined) {
                            self.dispatch('app/sidebar/setActive', 'sidebar-article-settings');
                        }
                        if (result.errors.blocks !== undefined) {
                            result.errors.blocks.forEach((mes) => {
                                self.$toast.error(mes, {
                                    duration: 5000,
                                });
                            });
                        }
                    }
                    if (result.message !== undefined && result.status_code !== 422) {
                        self.$toast.error(result.message);
                    }
                }
            });
    },
    async fetchUpdate({ commit, state }, data) {
        const self = this;
        commit('success', false);
        commit('pending', true);
        commit('errors', {});
        const save = Object.assign(state.form, data);
        save.private = save.access_all === true
            ? 0
            : 1;
        save.can_commented = save.commented === true
            ? 1
            : 0;
        save.tags = [];
        save.tagsArray.forEach((tag) => {
            save.tags.push(tag.id);
        });
        const url = api.user.articles.update.replace(':id', state.form.id);
        await self.$axios.post(url, save)
            .then((res) => {
                const resp = response.success(res);
                commit('pending', false);
                commit('success', true);
                if (resp.success && resp.message !== undefined) {
                    console.log(self.$toast.success(resp.message));
                }
            })
            .catch((error) => {
                if (error) {
                    commit('failed', true);
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                        if (result.errors.category_id !== undefined || result.errors.type !== undefined || result.errors.group_id !== undefined) {
                            self.dispatch('app/sidebar/setActive', 'sidebar-article-settings');
                        }
                    }
                    if (result.message !== undefined && result.status_code !== 422) {
                        self.$toast.error(result.message);
                    }
                }
            });
    },
    async fetchGetUid({ commit, state }, data) {
        const self = this;
        const url = api.editor.store.draft;
        await self.$axios.post(url, {})
            .then((res) => {
                const resp = response.success(res);
                commit('uid', resp.uid);
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                }
            });
    },
    setImage({ commit, state }, value) {
        state.form.image = value;
        state.form.video = null;
    },
    setVideo({ commit, state }, value) {
        state.form.image = null;
        state.form.video = value;
    }
};

const mutations = {
    success: (state, val) => {
        state.success = val;
        setTimeout(function () {
            state.success = false;
        }, 1500);
    },
    failed: (state, val) => {
        state.failed = val;
        setTimeout(function () {
            state.failed = false;
        }, 1500);
    },
    form: (state, val) => {
        state.form = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
    uid: (state, val) => {
        state.uid = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
