import response from '../../plugins/common/response';
import api from './../../api';
import axios from "./../../plugins/axios";

const state = () => ({
    loaded: false,
    pending: false,
    data: {},
});

const getters = {
    loaded(state) {
        return state.loaded;
    },
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
};

const actions = {
    async setUserData({ commit }, data) {
        commit('data', data);
    },
};

const mutations = {
    loaded: (state, val) => {
        state.loaded = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, val) => {
        state.data = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
