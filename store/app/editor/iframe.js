const state = () => ({
    url: null,
});

const getters = {
    url(state) {
        return state.url;
    },
};

const actions = {
    setWebUrl({ commit }, value) {
        const self = this;
        commit('url', value);
        if (value === null) {
            self.dispatch('app/editor/articles/store/setFormSource', {
                type: 'preview',
                source: null,
            });
        } else {
            self.dispatch('app/editor/articles/store/setFormSource', {
                type: 'web',
                source: value,
            });
        }
    },
};

const mutations = {
    url: (state, val) => {
        state.url = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
