import api from './../../../../api';
import beforeFetch from "../../../../plugins/common/beforeFetch";
import axiosStore from "../../../../plugins/common/axiosStore";

// initial state
const state = () => ({
    locale: 'ru',
    pending: true,
    hasPending: true,
    data: [],
    pagination: null,
});

// getters
const getters = {
    locale(state) {
        return state.locale;
    },
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    hasPending(state) {
        return state.hasPending;
    },
    pagination(state) {
        return state.pagination;
    },
};

// actions
const actions = {
    async fetchDrafts({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        const url = beforeFetch.getUrlModelId(api.editor.articles.list, data.model, data.id);
        const query = {};
        if (data.locale !== undefined) {
            query.locale = data.locale;
            commit('locale', data.locale);
        }
        if (data.article_id !== undefined && data.article_id !== null) {
            query.article_id = data.article_id;
        }
        query.drafts = 1;
        await axiosStore.init(self.$axios).get(url, query, function (result) {
            commit('pending', false);
            if (!state.hasPending) {
                commit('hasPending', true);
            }
            commit('data', result.data);
            if (result.pagination !== undefined) {
                commit('pagination', result.pagination);
            }
        }, function () {
            commit('pending', false);
        });
    },
    // async fetchArticlesWithoutPending({ commit }, data) {
    //     const self = this;
    //     commit('hasPending', false);
    //     await self.dispatch('app/editor/articles/list/fetchArticles', data);
    // },
    async fetchDraftsWithoutPending({ commit }, data) {
        const self = this;
        commit('hasPending', false);
        await self.dispatch('app/editor/articles/base/fetchDrafts', data);
    },
    // async fetchArticlesInitial({ commit }, data) {
    //     const self = this;
    //     commit('data', []);
    //     await self.dispatch('app/editor/articles/list/fetchArticles', data);
    // },
    // async fetchDraftsInitial({ commit }, data) {
    //     const self = this;
    //     commit('data', []);
    //     await self.dispatch('app/editor/articles/list/fetchDrafts', data);
    // },
};

// mutations
const mutations = {
    locale: (state, val) => {
        state.locale = val;
    },
    pending: (state, val) => {
        if (state.hasPending) {
            state.pending = val;
        }
    },
    hasPending: (state, val) => {
        state.hasPending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
