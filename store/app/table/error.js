const state = () => ({
    code: null,
    message: null,
});

const getters = {
    code(state) {
        return state.code;
    },
    message(state) {
        return state.message;
    },
};

const actions = {
    setAccessDenied({ commit }) {
        commit('code', 403);
        commit('message', 'Access Denied');
    },
    setEmpty({ commit }) {
        commit('code', null);
        commit('message', null);
    }
};

const mutations = {
    code: (state, val) => {
        state.code = val;
    },
    message: (state, val) => {
        state.message = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
