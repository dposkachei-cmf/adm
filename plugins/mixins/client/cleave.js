export default {
    cleave() {
        const empty = () => {};
        const phone = () => {
            return {
                prefix: '+',
                noImmediatePrefix: true,
                blocks: [2, 0, 3, 0, 3, 4],
                delimiters: [' ', '(', ')', ' ', '-'],
                numericOnly: true,
                numeralPositiveOnly: true,
            };
        };
        const date = () => {
            return {
                //noImmediatePrefix: true,
                blocks: [4, 2, 2],
                delimiters: ['-', '-'],
                numericOnly: true,
                numeralPositiveOnly: true,
            };
        };
        const percent = () => {
            return {
                prefix: '% ',
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            };
        };
        const price = () => {
            return {
                prefix: '$ ',
                numeral: true,
                numeralPositiveOnly: true,
                noImmediatePrefix: true,
                rawValueTrimPrefix: true,
                numeralIntegerScale: 9,
                numeralDecimalScale: 2
            };
        };
        const number = (max = 8) => {
            return {
                numeral: true,
                numeralThousandsGroupStyle: 'none',
                numeralIntegerScale: max,
                numeralPositiveOnly: true,
                numeralDecimalScale: 0
            };
        };
        const index = () => {
            return {
                numeral: true,
                numeralThousandsGroupStyle: 'none',
                numeralIntegerScale: 6,
                numeralDecimalScale: 0
            };
        };
        const years = () => {
            return {
                numeral: true,
                numeralThousandsGroupStyle: 'none',
                numeralIntegerScale: 2,
                numeralDecimalScale: 0
            };
        };
        const percentOne = () => {
            return {
                numeral: true,
                numeralThousandsGroupStyle: 'none',
                numeralIntegerScale: 2,
                numeralDecimalScale: 1
            };
        };
        const percentTwo = () => {
            return {
                numeral: true,
                numeralThousandsGroupStyle: 'none',
                numeralIntegerScale: 2,
                numeralDecimalScale: 3
            };
        };
        return {
            empty,
            date,
            phone,
            percent,
            price,
            number,
            index,
            years,
            percentOne,
            percentTwo,
        };
    },
};
