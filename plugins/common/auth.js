import CryptoJS from 'crypto-js';

export default {

    /**
     * @returns {[{sizes: string, rel: string, href: string}, {sizes: string, rel: string, href: string, type: string}, {sizes: string, rel: string, href: string, type: string}, {rel: string, href: string}, {color: string, rel: string, href: string}, null]}
     */
    yandexRedirect() {
        window.location.href = `https://oauth.yandex.ru/authorize?response_type=token&client_id=${process.env.YANDEX_AUTH_CLIENT_ID}&redirect_uri=${process.env.YANDEX_AUTH_CALLBACK}&force_confirm=yes`;
        //
        // YaAuthSuggest.init({
        //     client_id: process.env.YANDEX_AUTH_CLIENT_ID,
        //     response_type: 'token',
        //     redirect_uri: process.env.YANDEX_AUTH_CALLBACK,
        // }, process.env.APP_URL)
        //     .then(({ handler }) => handler())
        //     .then(data => console.log('Сообщение с токеном', data))
        //     .catch(error => console.log('Обработка ошибки', error));
    },

    /**
     *
     * @returns {string|null}
     */
    yandexGetAccessToken() {
        //YaSendSuggestToken(process.env.APP_URL);
        const hash = /access_token=([^&]+)/.exec(document.location.hash);
        if (hash === null) {
            return null;
        }
        return hash[1];
    },

    /**
     *
     */
    vkRedirect() {
        window.location.href = `https://oauth.vk.com/authorize?client_id=${process.env.VKONTAKTE_AUTH_CLIENT_ID}&redirect_uri=${process.env.VKONTAKTE_AUTH_CALLBACK}&scope=1&display=page&response_type=code`;
        //window.location.href = `https://oauth.vk.com/authorize?client_id=${process.env.VKONTAKTE_AUTH_CLIENT_ID}&redirect_uri=${process.env.VKONTAKTE_AUTH_CALLBACK}&scope=1&display=page`;
    },
    /**
     *
     * @returns {string|null}
     */
    vkGetAccessToken() {
        //YaSendSuggestToken(process.env.APP_URL);
        const hash = /access_token=([^&]+)/.exec(document.location.hash);
        if (hash === null) {
            return null;
        }
        return hash[1];
    },

    /**
     *
     */
    facebookRedirect() {
        const state = CryptoJS.lib.WordArray.random(16);
        window.location.href = `https://www.facebook.com/v12.0/dialog/oauth?client_id=${process.env.FACEBOOK_AUTH_CLIENT_ID}&redirect_uri=${process.env.FACEBOOK_AUTH_CALLBACK}&display=popup&scope=email&state=${state}&response_type=code`;
    },

    /**
     *
     * @returns {string|null}
     */
    facebookGetAccessToken() {
        const hash = /access_token=([^&]+)/.exec(document.location.hash);
        if (hash === null) {
            return null;
        }
        return hash[1];
    },


    /**
     *
     */
    googleRedirect() {
        //const state = CryptoJS.lib.WordArray.random(16);
        window.location.href = `https://accounts.google.com/o/oauth2/v2/auth?client_id=${process.env.GOOGLE_AUTH_CLIENT_ID}&redirect_uri=${process.env.GOOGLE_AUTH_CALLBACK}&scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile&response_type=token`;
    },

    /**
     *
     */
    googleGetAccessToken() {
        //YaSendSuggestToken(process.env.APP_URL);
        const hash = /access_token=([^&]+)/.exec(document.location.hash);
        if (hash === null) {
            return null;
        }
        return hash[1];
    },
};
