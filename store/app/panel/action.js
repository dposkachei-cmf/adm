import api from '../../../api';
import response from '../../../plugins/common/response';
import serialize from '../../../plugins/common/serialize';
import axiosStore from '../../../plugins/common/axiosStore';
import beforeFetch from '../../../plugins/common/beforeFetch';
import axios from './../../../plugins/axios';

const state = () => ({
    item: null,
    success: false,
    model: null,
    name: null,
    errors: null,
    locale: 'ru',
    pendingArray: [],
    data: null,
});

const getters = {
    name(state) {
        return state.opened;
    },
    item(state) {
        return state.item;
    },
    model(state) {
        return state.model;
    },
    errors(state) {
        return state.errors;
    },
    locale(state) {
        return state.locale;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
    data(state) {
        return state.data;
    },
};

const actions = {
    async fetchAction({ commit, state }, data) {
        const self = this;
        commit('model', data.model);
        commit('name', data.action);
        commit('success', false);
        commit('data', null);
        const url = beforeFetch.getUrlModelId(api.panel.action, data.model, data.id);
        if (data.pending !== undefined) {
            commit('pendingArray', response.pendingAdd(state.pendingArray, data.pending));
        } else {
            commit('pendingArray', response.pendingAdd(state.pendingArray, data.id, data.action));
        }
        let query = {};
        query.action = data.action;
        if (data.locale !== undefined) {
            query.locale = data.locale;
        }
        if (data.request !== undefined) {
            query = Object.assign({}, query, data.request);
        }
        if (data.item !== undefined) {
            commit('item', data.item);
        }
        await axiosStore.init(self.$axios).post(url, query, function (result) {
            if (data.pending !== undefined) {
                commit('pendingArray', response.pendingRemove(state.pendingArray, data.pending));
            } else {
                commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, data.action));
            }
            commit('success', true);
            if (result.data !== undefined) {
                commit('data', result.data);
            }
        }, function () {
            if (data.pending !== undefined) {
                commit('pendingArray', response.pendingRemove(state.pendingArray, data.pending));
            } else {
                commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, data.action));
            }
        });
    },
};

const mutations = {
    name: (state, val) => {
        state.name = val;
    },
    model: (state, val) => {
        state.model = val;
    },
    errors: (state, data) => {
        state.errors = data;
    },
    pendingArray: (state, data) => {
        state.pendingArray = data;
    },
    success: (state, val) => {
        state.success = val;
    },
    locale: (state, val) => {
        state.locale = val;
    },
    item: (state, data) => {
        state.item = data;
    },
    data: (state, data) => {
        state.data = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
