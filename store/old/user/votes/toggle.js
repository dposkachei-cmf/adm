import response from '../../../../plugins/common/response';
import api from '../../../../api';

// initial state
const state = () => ({
    pending: false,
    pendingArray: [],
});

// getters
const getters = {
    pending(state) {
        return state.pending;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
};

// actions
const actions = {
    async fetchToggle({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        const action = data.value === 1 ? 'up' : 'down';
        commit('pendingArray', response.pendingAdd(state.pendingArray, data.id, data.type + '_' + action));
        await self.$axios.post(api.user.votes.toggle, {
            item_type: data.type,
            item_id: data.id,
            value: data.value,
        })
            .then((res) => {
                commit('pending', false);
                commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, data.type + '_' + action));
                self.dispatch('user/votes/array/fetchVotes', {});
                if (data.type === 'article') {
                    self.dispatch('article/counters/fetchCounters', {
                        id: data.id,
                    });
                }
                if (data.type === 'comment') {
                    self.dispatch('comment/counters/fetchCounters', {
                        id: data.id,
                    });
                }
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                    commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, data.type + '_' + action));
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    pendingArray: (state, data) => {
        state.pendingArray = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
