const state = () => ({
    pending: false,
});

const getters = {
    pending(state) {
        return state.pending;
    },
};

const actions = {
    async fetchTest({ commit, state }, data) {
        const self = this;
        await self.$axios.get('/tags?token=' + data.token)
            .then((res) => {
                //
            })
            .catch((error) => {
                if (error) {
                    //
                }
            });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
