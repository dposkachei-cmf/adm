import response from '../../../plugins/common/response';
import api from './../../../api';
import axiosStore from "../../../plugins/common/axiosStore";

const state = () => ({
    destroyed: false,
    pending: false,
    pendingArray: [],
});

const getters = {
    destroyed(state) {
        return state.destroyed;
    },
    pending(state) {
        return state.pending;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
};

const actions = {
    async fetchDestroy({ state, commit }, data) {
        const self = this;
        commit('pending', true);
        commit('pendingArray', response.pendingAdd(state.pendingArray, data.id));
        const url = api.user.comments.destroy.replace(':id', data.id);
        await axiosStore.init(self.$axios).delete(url, function () {
            commit('pending', false);
            commit('destroyed', true);
            commit('pendingArray', response.pendingRemove(state.pendingArray, data.id));
            self.$auth.fetchUser().then(function () {});
        }, function () {
            commit('pending', false);
            commit('pendingArray', response.pendingRemove(state.pendingArray, data.id));
        });
    },
};

const mutations = {
    destroyed: (state, val) => {
        state.destroyed = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    pendingArray: (state, val) => {
        state.pendingArray = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
