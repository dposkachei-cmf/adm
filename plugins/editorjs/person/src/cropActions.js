import { make } from './ui';
import bgIcon from './svg/background.svg';
import borderIcon from './svg/border.svg';
import cropIcon from './svg/crop.svg';
import stretchedIcon from './svg/stretched.svg';


/**
 * Working with Block Tunes
 */
export default class CropActions {
    /**
     * @param {object} tune - image tool Tunes managers
     * @param {object} tune.api - Editor API
     * @param {Function} tune.onCropFile - tune toggling callback
     */
    constructor({ api, onCropFile }) {
        this.api = api;
        this.onCropFile = onCropFile;
        this.buttons = [];
    }

    /**
     * Available Image tunes
     *
     * @returns {{name: string, icon: string, title: string}[]}
     */
    static get actions() {
        return [
            {
                name: 'crop',
                icon: cropIcon,
                title: 'Обрезать',
            },
        ];
    }

    /**
     * Styles
     *
     * @returns {{wrapper: string, buttonBase: *, button: string, buttonActive: *}}
     */
    get CSS() {
        return {
            wrapper: '',
            buttonBase: this.api.styles.settingsButton,
            button: 'image-tool__tune',
            buttonActive: this.api.styles.settingsButtonActive,
        };
    }

    /**
     * Makes buttons with tunes: add background, add border, stretch image
     *
     * @param {ImageToolData} toolData - generate Elements of tunes
     * @returns {Element}
     */
    render(toolData) {
        const title = this.api.i18n.t('Обрезать');
        const el = make('div', [this.CSS.buttonBase, this.CSS.button], {
            innerHTML: cropIcon,
            title,
        });

        el.addEventListener('click', () => {
            this.tuneClicked('crop');
        });

        el.dataset.tune = 'crop';
        //el.classList.toggle(this.CSS.buttonActive, toolData[tune.name]);

        this.buttons.push(el);

        this.api.tooltip.onHover(el, title, {
            placement: 'top',
        });
        return el;
        //
        // const wrapper = make('div', this.CSS.wrapper);
        // this.buttons = [];
        // const tunes = CropActions.actions;
        // tunes.forEach(tune => {
        //     const title = this.api.i18n.t(tune.title);
        //     const el = make('div', [this.CSS.buttonBase, this.CSS.button], {
        //         innerHTML: tune.icon,
        //         title,
        //     });
        //     el.addEventListener('click', () => {
        //         this.tuneClicked(tune.name);
        //     });
        //     el.dataset.tune = tune.name;
        //     //el.classList.toggle(this.CSS.buttonActive, toolData[tune.name]);
        //     this.buttons.push(el);
        //     this.api.tooltip.onHover(el, title, {
        //         placement: 'top',
        //     });
        //     wrapper.appendChild(el);
        // });
        // return wrapper;
    }

    /**
     * Clicks to one of the tunes
     *
     * @param {string} tuneName - clicked tune name
     */
    tuneClicked(tuneName) {
        if (tuneName === 'crop') {
            this.onCropFile();
        }
    }
}
