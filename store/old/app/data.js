import response from '../../../plugins/common/response';
import api from '../../../api';

const state = () => ({
    loaded: false,
    pending: false,
    reloaded: false,
    data: {
        config: {
            ENV: 'default',
            DEVELOPMENT_MODE: false,
        },
        categories: [],
        groups: [],
        types: [],
    },
});

const getters = {
    loaded(state) {
        return state.loaded;
    },
    data(state) {
        return state.data;
    },
    config(state) {
        return state.data.config;
    },
    pending(state) {
        return state.pending;
    },
    reloaded(state) {
        return state.reloaded;
    },
};

const actions = {
    async fetchIndexData({ commit, state }, data) {
        const self = this;
        if (state.loaded) {
            return;
        }
        commit('pending', true);
        await self.$axios.get(api.index.data)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                if (!state.loaded) {
                    commit('loaded', true);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                }
            });
    },
    async fetchIndexDataForce({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        await self.$axios.get(api.index.data)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                if (!state.loaded) {
                    commit('loaded', true);
                }
                commit('reloaded', true);
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                }
            });
    },
};

const mutations = {
    loaded: (state, val) => {
        state.loaded = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, val) => {
        state.data = val;
    },
    reloaded: (state, val) => {
        state.reloaded = val;
        if (val) {
            setTimeout(function () {
                state.reloaded = false;
            }, 1000);
        }
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
