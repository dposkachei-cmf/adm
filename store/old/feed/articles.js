import response from '../../../plugins/common/response';
import serialize from '../../../plugins/common/serialize';
import api from '../../../api';

// initial state
const state = () => ({
    pending: true,
    loaded: false,
    initialLoaded: false,
    data: [],
    form: {
        categories: ['all'],
        groups: ['all'],
        types: ['all'],
    },
    formMy: {
        categories: ['all'],
        groups: ['all'],
        types: ['all'],
    },
    pagination: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    loaded(state) {
        return state.loaded;
    },
    initialLoaded(state) {
        return state.initialLoaded;
    },
    pending(state) {
        return state.pending;
    },
    pagination(state) {
        return state.pagination;
    },
    form(state) {
        return state.form;
    },
};

// actions
const actions = {
    async fetchArticles({ commit, state }, data) {
        const self = this;
        if (state.loaded) {
            return;
        }
        commit('pending', true);
        const string = serialize.query(data);
        await self.$axios.get(api.feed.articles + '?' + string)
            .then((res) => {
                const resp = response.success(res);
                commit('pending', false);
                if (resp.success) {
                    commit('data', resp.data);
                }
                if (!state.loaded) {
                    commit('loaded', true);
                }
                if (!state.initialLoaded) {
                    commit('initialLoaded', true);
                }
                if (resp.pagination !== undefined) {
                    commit('pagination', resp.pagination);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
    async fetchArticlesInitial({ commit, state }, data) {
        const self = this;
        commit('data', []);
        commit('loaded', false);
        commit('initialLoaded', false);
        await self.dispatch('feed/articles/fetchArticles', data);
    },
    async fetchArticlesPage({ commit, state }, data) {
        const self = this;
        commit('loaded', false);
        await self.dispatch('feed/articles/fetchArticles', data);
    },
    async clearForm({ commit, state }, data) {
        await commit('form', {
            categories: ['all'],
            groups: ['all'],
            types: ['all'],
        });
    },
    async setForm({ commit, state }, data) {
        await commit('form', data);
    },
    async setFormMy({ commit, state }, data) {
        await commit('form', data);
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    loaded: (state, val) => {
        state.loaded = val;
    },
    initialLoaded: (state, val) => {
        state.initialLoaded = val;
    },
    data: (state, data) => {
        if (data.length === 0) {
            state.data = [];
        } else {
            data.forEach(function (value) {
                state.data.push(value);
            });
        }
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
    form: (state, data) => {
        state.form = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
