import response from '../../../../../plugins/common/response';
import api from '../../../../../api';
import serialize from '../../../../../plugins/common/serialize';

const state = () => ({
    pending: false,
    reloaded: false,
    errors: {},
});

const getters = {
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
    reloaded(state) {
        return state.reloaded;
    },
};

const actions = {
    async fetchLoginVk({ commit }, data) {
        const self = this;
        commit('pending', true);
        commit('errors', {});
        let url = api.auth.socialite.vk;
        url += '?' + serialize.query(data);
        await self.$axios.get(url)
            .then((res) => {
                commit('pending', false);
                commit('reloaded', true);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                if (result.success && result.message !== undefined) {
                    self.$toast.success(result.message);
                }
                if (result.data.token !== undefined) {
                    if (data.user_id !== undefined) {
                        self.$auth.setUserToken(result.data.token).then(() => {
                            self.$router.push('/personal/settings/accounts');
                        });
                    } else {
                        self.$auth.setUserToken(result.data.token).then(() => {
                            window.location.href = '/';
                        });
                    }
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                    if (result.message !== undefined) {
                        self.$toast.error(result.message, {
                            duration: 5000,
                        });
                    }
                }
            });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
    reloaded: (state, val) => {
        state.reloaded = val;
        if (val) {
            setTimeout(function () {
                state.reloaded = false;
            }, 5000);
        }
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
