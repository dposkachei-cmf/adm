import api from '../../../../api';
import response from "../../../../plugins/common/response";

const state = () => ({
    pending: false,
});

const getters = {
    pending(state) {
        return state.pending;
    },
};

const actions = {
    async fetchClear({ state, commit }, data) {
        const self = this;
        commit('pending', true);
        const url = api.user.notifications.clear;
        await self.$axios.post(url)
            .then((res) => {
                commit('pending', false);
                self.dispatch('user/notifications/list/fetchNotificationsWithoutPending');
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                }
            });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
