import response from '../../../../../plugins/common/response';
import api from "../../../../../api";

const state = () => ({
    success: false,
    pending: false,
    errors: {},
    closeModal: false,
});

const getters = {
    success(state) {
        return state.success;
    },
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
    closeModal(state) {
        return state.closeModal;
    },
};

const actions = {
    async fetchUploadTemplate({ commit, state }, data) {
        const self = this;
        commit('success', false);
        commit('pending', true);
        commit('errors', {});
        commit('closeModal', false);
        const url = api.editor.upload.file.replace(':uid', data.get('uid'));
        await self.$axios.post(url, data)
            .then((res) => {
                const resp = response.success(res);
                commit('pending', false);
                commit('success', true);
                console.log(resp.file.url);
                self.dispatch('user/articles/store/setImage', resp.file.url);
                if (resp.success && resp.message !== undefined) {
                    console.log(resp.message);
                }
                setTimeout(function () {
                    commit('closeModal', true);
                }, 1000);
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                }
            });
    },
    async fetchUploadAndUpdate({ commit, state }, data) {
        const self = this;
        commit('success', false);
        commit('pending', true);
        commit('errors', {});
        commit('closeModal', false);
        const url = api.user.articles.image.replace(':id', data.id);
        await self.$axios.post(url, data)
            .then((res) => {
                const resp = response.success(res);
                commit('pending', false);
                commit('success', true);
                if (resp.success && resp.message !== undefined) {
                    console.log(self.$toast.success(resp.message));
                }
                setTimeout(function () {
                    commit('closeModal', true);
                }, 1000);
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                }
            });
    },
};

const mutations = {
    success: (state, val) => {
        state.success = val;
        setTimeout(function () {
            state.success = false;
        }, 1500);
    },
    pending: (state, val) => {
        state.pending = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
    closeModal: (state, val) => {
        state.closeModal = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
