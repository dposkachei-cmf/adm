import api from './../../../api';
import axiosStore from "../../../plugins/common/axiosStore";

const state = () => ({
    pending: false,
});

const getters = {
    pending(state) {
        return state.pending;
    },
};

const actions = {
    async fetchClear({ state, commit }) {
        const self = this;
        commit('pending', true);
        const url = api.user.notifications.clear;
        await axiosStore.init(self.$axios).post(url, {}, function (result) {
            commit('pending', false);
            self.dispatch('user/notifications/list/fetchNotificationsWithoutPending');
        }, function () {
            commit('pending', false);
        });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
