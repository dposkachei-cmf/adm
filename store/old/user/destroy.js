import response from '../../../plugins/common/response';
import api from '../../../api';

const state = () => ({
    destroyed: false,
    pending: false,
});

const getters = {
    destroyed(state) {
        return state.destroyed;
    },
    pending(state) {
        return state.pending;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
};

const actions = {
    async fetchDestroy({ state, commit }, data) {
        const self = this;
        commit('pending', true);
        await self.$axios.delete(api.user.destroy)
            .then((res) => {
                commit('pending', false);
                commit('destroyed', true);
                self.$router.push({
                    path: self.localePath('/auth/logout'),
                });
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                }
            });
    },
};

const mutations = {
    destroyed: (state, val) => {
        state.destroyed = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
