export default {

    /**
     *
     */
    sidebarActions() {
        const self = this;
        const bodyOverflowHidden = (visible) => {
            const body = document.getElementsByTagName('body')[0];
            if (visible) {
                body.classList.add('overflow-hidden');
                setTimeout(function () {
                    //body.classList.add('overflow-hidden');
                }, 200);
            } else {
                body.classList.remove('overflow-hidden');
                setTimeout(function () {
                    //body.classList.remove('overflow-hidden');
                }, 200);
            }
        };
        const close = () => {};
        const closeAll = () => {
            self.$store.dispatch('app/sidebar/setActive', null);
            self.$store.dispatch('app/sidebar/setCurrent', null);
        };
        return {
            open,
            close,
            closeAll,
            bodyOverflowHidden,
        };
    },
};
