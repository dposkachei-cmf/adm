export default {
    /**
     *
     * @param field
     * @param formRouteTable
     * @returns {null}
     */
    selectTable(field, formRouteTable) {
        let value = null;
        if (formRouteTable[field.key] !== undefined) {
            value = formRouteTable[field.key];
        }
        return value;
    },

    /**
     *
     * @param field
     * @param item
     * @returns {null}
     */
    selectField(field, item) {
        let value = null;
        if (item !== null) {
            value = item[field.key];
        }
        if (field.default !== undefined && (value === null || value === undefined)) {
            value = field.default;
        }
        return value;
    },

    /**
     *
     * @param field
     * @param item
     * @returns {null}
     */
    selectShow(field, item) {
        let value = null;
        if (item !== null && field.selected.id !== undefined) {
            value = field.selected;
        }
        if (value === null && item !== null && item[field.key] !== undefined && item[field.key] !== null) {
            value = item[field.key];
        }
        if (field.default !== undefined && (value === null || value === undefined)) {
            value = field.default;
        }
        return value;
    },

    /**
     *
     * @param field
     * @param formRouteTable
     * @returns {null}
     */
    searchSelectTable(field, formRouteTable) {
        let value = null;
        if (field.selected !== undefined) {
            value = field.selected;
        }
        if (value === null && formRouteTable[field.key] !== undefined) {
            value = formRouteTable[field.key];
        }
        return value;
    },

    /**
     *
     * @param field
     * @param item
     * @returns {null}
     */
    searchSelectField(field, item) {
        let value = null;
        if (field.selected !== undefined) {
            value = field.selected;
        }
        if (item !== null && value === null) {
            value = field.values.find((x) => x.id === item.id);
        }
        if (field.default !== undefined && (value === null || value === undefined)) {
            value = field.default;
        }
        return value;
    },

    /**
     *
     * @param field
     * @param item
     * @param role
     * @param form
     * @param formRouteTable
     * @returns {*}
     */
    searchSelect(field, item, role, form, formRouteTable) {
        let value = null;
        if (field.multiple) {
            value = field.selected;
        } else {
            value = item !== null
                ? field.selected
                : null;
        }
        if (role === 'table' && value === null && field.selected !== undefined) {
            value = field.selected;
        } else if (value === null && formRouteTable[field.key] !== undefined) {
            value = formRouteTable[field.key];
        }
        if (role === 'panel' && value === null && field.default !== undefined) {
            value = field.default;
        }
        if (field.default !== undefined && (value === null || value === undefined)) {
            value = field.default;
        }
        return value;
    },

    /**
     *
     * @param field
     * @param item
     * @returns {*}
     */
    stringField(field, item) {
        let value = null;
        if (field.parent === undefined) {
            if (item !== null) {
                value = item[field.key];
            }
            if (value === null && field.value !== undefined) {
                value = field.value;
            }
        } else {
            if (item !== null) {
                value = item[field.parent][field.key_original];
            }
            if (value === null && field.value !== undefined) {
                value = field.value;
            }
        }
        return value;
    },

    /**
     *
     * @param field
     * @param item
     * @returns {*}
     */
    stringShow(field, item) {
        let value = null;
        if (item !== null) {
            value = item[field.key];
        }
        if (value === null && field.value !== undefined) {
            value = field.value;
        }
        return value;
    },

    /**
     *
     * @param field
     * @param formRouteTable
     * @returns {*}
     */
    stringTable(field, formRouteTable) {
        let value = null;
        if (formRouteTable[field.key] !== undefined) {
            value = formRouteTable[field.key];
        }
        if (value === null && field.value !== undefined) {
            value = field.value;
        }
        return value;
    },

    /**
     *
     * @param field
     * @param item
     * @param form
     * @param formRouteTable
     * @returns {*}
     */
    string(field, item, form, formRouteTable) {
        let value = null;
        if (item !== null) {
            value = item[field.key];
        } else {
            value = null;
        }
        if (value === null && formRouteTable[field.key] !== undefined) {
            value = formRouteTable[field.key];
        }
        if (value === null && field.value !== undefined) {
            value = field.value;
        }
        return value;
    },

    /**
     * @param field
     * @param item
     * @returns {*}
     */
    valueShow(field, item) {
        let value = null;
        if (field.parent === undefined) {
            if (item !== null) {
                value = item[field.key];
            }
            if (value === null && field.value !== undefined) {
                value = field.value;
            }
        } else {
            if (item !== null) {
                value = item[field.parent][field.key_original];
            }
            if (value === null && field.value !== undefined) {
                value = field.value;
            }
        }
        return value;
    },

    /**
     * @param field
     * @param item
     * @returns {*}
     */
    valueField(field, item) {
        let value = null;
        if (field.parent === undefined) {
            if (item !== null) {
                value = item[field.key];
            }
            if (value === null && field.value !== undefined) {
                value = field.value;
            }
            if (value === null && field.default !== undefined) {
                value = field.default;
            }
        } else {
            if (item !== null) {
                value = item[field.parent][field.key_original];
            }
            if (value === null && field.value !== undefined) {
                value = field.value;
            }
            if (value === null && field.default !== undefined) {
                value = field.default;
            }
        }
        return value;
    },

    /**
     * @param field
     * @param item
     * @returns {*}
     */
    fileField(field, item) {
        let value = {
            blob: null,
            file: null,
            filename: null,
        };
        if (item !== null) {
            value = {
                blob: item[field.key].file,
                file: item[field.key].file,
                filename: item[field.key].filename,
            };
        }
        return value;
    },

    /**
     * @param field
     * @param item
     * @returns {*}
     */
    imageField(field, item) {
        let value = {
            blob: null,
            file: null,
            filename: null,
        };
        if (item !== null) {
            value = {
                blob: item[field.key].file,
                file: item[field.key].file,
                filename: item[field.key].filename,
            };
        }
        return value;
    },

    /**
     * @param field
     * @param formRouteTable
     * @returns {*}
     */
    valueTable(field, formRouteTable) {
        let value = null;
        if (formRouteTable[field.key] !== undefined) {
            value = formRouteTable[field.key];
        }
        if (value === null && field.value !== undefined) {
            value = field.value;
        }
        return value;
    },

    /**
     * @param field
     * @param formRouteTable
     * @returns {*}
     */
    dateTable(field, formRouteTable) {
        let value = null;
        if (formRouteTable[field.key] !== undefined) {
            value = formRouteTable[field.key];
        }
        if (value === null && field.value !== undefined) {
            value = field.value;
        }
        return value;
    },
    /**
     * @param field
     * @param item
     * @returns {*}
     */
    dateShow(field, item) {
        let value = null;
        if (item !== null) {
            value = item[field.key];
        }
        if (value === null && field.value !== undefined) {
            value = field.value;
        }
        return value;
    },

    /**
     * @param field
     * @param item
     * @returns {*}
     */
    dateField(field, item) {
        let value = null;
        if (item !== null) {
            value = item[field.key];
        }
        if (value === null && field.value !== undefined) {
            value = field.value;
        }
        return value;
    },

    multiselectField(field, item) {
        let values = [];
        if (item !== null) {
            if (item[field.key] !== undefined && item[field.key].length) {
                item[field.key].forEach((value) => {
                    values.push(value);
                })
            }
        }
        return values;
    },

    multiselectShow(field, item) {
        let values = [];
        if (item !== null) {
            if (item[field.key] !== undefined && item[field.key].length) {
                item[field.key].forEach((value) => {
                    values.push(value);
                })
            }
        }
        return values;
    },

    multiselectTable(field, formRouteTable) {
        let values = [];
        if (formRouteTable[field.key] !== undefined) {
            formRouteTable[field.key].forEach((value) =>  {
                values.push(field.values.find((x) => x.id === parseInt(value)));
            });
        }
        return values;
    }

}
