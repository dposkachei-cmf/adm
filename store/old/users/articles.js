import response from '../../../plugins/common/response';
import serialize from '../../../plugins/common/serialize';
import api from '../../../api';

// initial state
const state = () => ({
    pending: true,
    data: {},
    pagination: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    pagination(state) {
        return state.pagination;
    },
};

// actions
const actions = {
    async fetchArticles({ commit }, data) {
        const self = this;
        commit('pending', true);
        const query = serialize.query({
            page: data.page || 1,
        });
        const url = api.users.articles.replace(':id', data.id);
        await self.$axios.get(url + '?' + query)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                commit('pagination', result.pagination);
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
    pagination: (state, data) => {
        state.pagination = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
