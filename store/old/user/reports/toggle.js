import response from '../../../../plugins/common/response';
import api from '../../../../api';

// initial state
const state = () => ({
    pending: false,
    pendingArray: [],
    errors: {},
    closeModal: false,
});

// getters
const getters = {
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
    pendingArray(state) {
        return state.pendingArray;
    },
    closeModal(state) {
        return state.closeModal;
    },
};

// actions
const actions = {
    async fetchReportToggle({ commit, state }, data) {
        const self = this;
        commit('closeModal', false);
        commit('pending', true);
        commit('pendingArray', response.pendingAdd(state.pendingArray, data.id, data.type));
        await self.$axios.post(api.user.reports.toggle, {
            item_type: data.type,
            item_id: data.id,
            text: data.text,
        })
            .then((res) => {
                commit('pending', false);
                commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, data.type));
                self.dispatch('user/reports/array/fetchReports', {});
                const result = response.success(res);
                if (result.success && result.message !== undefined) {
                    self.$toast.success(result.message);
                }
                setTimeout(function () {
                    commit('closeModal', true);
                }, 1000);
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    commit('pendingArray', response.pendingRemove(state.pendingArray, data.id, data.type));
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
    closeModal: (state, val) => {
        state.closeModal = val;
    },
    pendingArray: (state, data) => {
        state.pendingArray = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
