const state = () => ({
    current: null,
    active: null,
    disabled: false,
    names: {
        login: 'login',
        search: 'search',
        settings: {
            main_articles: 'settings-main-articles',
        },
        article: {
            confirm_delete: 'article_confirm_delete',
            upload_image: 'article_upload_image',
            upload_video: 'article_upload_video',
        },
        comment: {
            confirm_delete: 'comment_confirm_delete',
        },
        user: {
            upload_avatar: 'user_upload_avatar',
            group_upload_image: 'user_group_upload_image',
            confirm_delete: 'user_confirm_delete',
            report: 'user_report',
            tag: {
                create: 'user_tag_create',
            }
        },
        group: {
            invite: 'group-invite',
        },
    },
    item: {},
});

const getters = {
    current(state) {
        return state.current;
    },
    active(state) {
        return state.active;
    },
    names(state) {
        return state.names;
    },
    disabled(state) {
        return state.disabled;
    },
    item(state) {
        return state.item;
    },
};

const actions = {
    setActive({ commit }, name) {
        commit('active', name);
    },
    setCurrent({ commit }, name) {
        commit('current', name);
    },
    setDisabled({ commit }) {
        commit('disabled', true);
    },
    setItem({ commit }, data) {
        commit('item', data);
    }
};

const mutations = {
    current: (state, val) => {
        state.current = val;
    },
    active: (state, val) => {
        state.active = val;
    },
    disabled: (state, val) => {
        state.disabled = val;
    },
    item: (state, val) => {
        state.item = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
