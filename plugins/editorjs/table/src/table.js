import { create } from './documentUtils';
import './styles/table.pcss';

const CSS = {
    table: 'tc-table',
    inputField: 'tc-table__inp',
    cell: 'tc-table__cell',
    wrapper: 'tc-table__wrap',
    area: 'tc-table__area',
    cellMenu: 'tc-table__cell--menu',
    cellMenuButton: 'tc-table__cell--menu__button',
    cellMenuContent: 'tc-table__cell--menu__content',
    highlight: 'tc-table__highlight'
};

/**
 * Generates and manages _table contents.
 */
export class Table {
    /**
     * Creates
     */
    constructor(api) {
        this.api = api;
        this._numberOfColumns = 0;
        this._numberOfRows = 0;
        this._element = this._createTableWrapper();
        this._table = this._element.querySelector('table');
        this._selectedCell = null;
        this._attachEvents();
    }

    /**
     * returns selected/editable cell or null if row is not selected
     * @return {HTMLElement|null}
     */
    get selectedCell() {
        return this._selectedCell;
    }

    /**
     * sets a selected cell and highlights it
     * @param cell - new current cell
     */
    set selectedCell(cell) {
        if (this._selectedCell) {
            this._selectedCell.classList.remove(CSS.highlight);
            this.removeCellMenu(this._selectedCell);
        }

        this._selectedCell = cell;

        if (this._selectedCell) {
            this._selectedCell.classList.add(CSS.highlight);
            this.insertCellMenu(this._selectedCell);
        }
    }

    /**
     * returns current a row that contains current cell
     * or null if no cell selected
     * @returns {HTMLElement|null}
     */
    get selectedRow() {
        if (!this.selectedCell) {
            return null;
        }

        return this.selectedCell.closest('tr');
    }

    insertCellMenu(cell) {
        const self = this;
        const menu = create('div', [CSS.cellMenu], null);
        const menuButton = create('div', [CSS.cellMenuButton], null);
        menuButton.innerHTML = '<svg class="icon icon--dots" width="8px" height="8px"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#dots"></use></svg>';

        self.api.tooltip.onHover(menuButton, 'Настройки ячейки', {
            placement: 'top',
        });

        const menuContent = create('div', [CSS.cellMenuContent], null);
        self.insertCellMenuContent(menuContent, cell);

        menu.appendChild(menuButton);
        menu.appendChild(menuContent);

        cell.appendChild(menu);

        menuButton.addEventListener('click', function () {
            self.openCellMenu(this);
        });
    }

    insertCellMenuContent(menuContent, cell) {
        const container = create('div', ['ce-settings'], null);
        const zone = create('div', ['ce-settings__plugin-zone'], null);
        const div = create('div', [], null);

        const alignCenter = this.menuCellAlignButtonCenter(cell);
        const alignLeft = this.menuCellAlignButtonLeft(cell);
        const alignRight = this.menuCellAlignButtonRight(cell);

        //const rightArrowButton = this.menuCellMergeButtonRight(cell);
        //const downArrowButton = this.menuCellMergeButtonDown(cell);

        div.appendChild(alignLeft);
        div.appendChild(alignCenter);
        div.appendChild(alignRight);

        //div.appendChild(rightArrowButton);
        //div.appendChild(downArrowButton);

        zone.appendChild(div);
        container.appendChild(zone);

        menuContent.appendChild(container);
    }

    /**
     *
     * @param cell
     * @returns {HTMLElement}
     */
    menuCellMergeButtonRight(cell) {
        const self = this;
        const button = create('div', ['cdx-settings-button'], null);
        button.innerHTML = '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="long-arrow-alt-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-long-arrow-alt-right fa-w-14 fa-3x"><path fill="currentColor" d="M313.941 216H12c-6.627 0-12 5.373-12 12v56c0 6.627 5.373 12 12 12h301.941v46.059c0 21.382 25.851 32.09 40.971 16.971l86.059-86.059c9.373-9.373 9.373-24.569 0-33.941l-86.059-86.059c-15.119-15.119-40.971-4.411-40.971 16.971V216z" class=""></path></svg>';
        button.addEventListener('click', function () {
            console.log('current', cell);
            console.log('cell.colSpan', cell.colSpan);
            const cellIndex = cell.cellIndex + 1;
            const next = cell.closest('tr').querySelector('td:nth-child(' + (cellIndex + 1) + ')');
            if (next !== null) {
                next.remove();
                cell.colSpan = cell.colSpan + 1;
            }
        });
        self.api.tooltip.onHover(button, 'Слить с правой ячейкой', {
            placement: 'top',
        });
        return button;
    }

    /**
     *
     * @param cell
     * @returns {HTMLElement}
     */
    menuCellMergeButtonDown(cell) {
        const self = this;
        const button = create('div', ['cdx-settings-button'], null);
        button.innerHTML = '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="long-arrow-alt-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="-100 0 448 512" class="svg-inline--fa fa-long-arrow-alt-down fa-w-8 fa-3x"><path fill="currentColor" d="M168 345.941V44c0-6.627-5.373-12-12-12h-56c-6.627 0-12 5.373-12 12v301.941H41.941c-21.382 0-32.09 25.851-16.971 40.971l86.059 86.059c9.373 9.373 24.569 9.373 33.941 0l86.059-86.059c15.119-15.119 4.411-40.971-16.971-40.971H168z" class=""></path></svg>';
        button.addEventListener('click', function () {
            console.log('current', cell);
            console.log('cell.rowSpan', cell.rowSpan);
            const cellIndex = cell.cellIndex + 1;
            const rowIndex = cell.closest('tr').rowIndex + 1;
            const next = cell.closest('tbody').querySelector('tr:nth-child(' + (rowIndex + 1) + ')').querySelector('td:nth-child(' + cellIndex + ')');
            if (next !== null) {
                next.remove();
                cell.rowSpan = cell.rowSpan + 1;
            }
        });
        self.api.tooltip.onHover(button, 'Слить с нижней ячейкой', {
            placement: 'top',
        });
        return button;
    }

    /**
     *
     * @param cell
     * @returns {HTMLElement}
     */
    menuCellAlignButtonCenter(cell) {
        const self = this;
        const button = create('div', ['cdx-settings-button'], null);
        button.innerHTML = '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="align-center" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-align-center fa-w-14 fa-3x"><path fill="currentColor" d="M432 160H16a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zm0 256H16a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zM108.1 96h231.81A12.09 12.09 0 0 0 352 83.9V44.09A12.09 12.09 0 0 0 339.91 32H108.1A12.09 12.09 0 0 0 96 44.09V83.9A12.1 12.1 0 0 0 108.1 96zm231.81 256A12.09 12.09 0 0 0 352 339.9v-39.81A12.09 12.09 0 0 0 339.91 288H108.1A12.09 12.09 0 0 0 96 300.09v39.81a12.1 12.1 0 0 0 12.1 12.1z" class=""></path></svg>';
        button.addEventListener('click', function () {
            self.menuCellAlignToggle(cell, 'tc-table__center');
        });
        self.api.tooltip.onHover(button, 'Выровнять по центру', {
            placement: 'top',
        });
        return button;
    }

    /**
     *
     * @param cell
     * @returns {HTMLElement}
     */
    menuCellAlignButtonLeft(cell) {
        const self = this;
        const button = create('div', ['cdx-settings-button'], null);
        button.innerHTML = '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="align-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-align-left fa-w-14 fa-3x"><path fill="currentColor" d="M12.83 352h262.34A12.82 12.82 0 0 0 288 339.17v-38.34A12.82 12.82 0 0 0 275.17 288H12.83A12.82 12.82 0 0 0 0 300.83v38.34A12.82 12.82 0 0 0 12.83 352zm0-256h262.34A12.82 12.82 0 0 0 288 83.17V44.83A12.82 12.82 0 0 0 275.17 32H12.83A12.82 12.82 0 0 0 0 44.83v38.34A12.82 12.82 0 0 0 12.83 96zM432 160H16a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zm0 256H16a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16z" class=""></path></svg>';
        button.addEventListener('click', function () {
            self.menuCellAlignToggle(cell, 'tc-table__left');
        });
        self.api.tooltip.onHover(button, 'Выровнять по левому краю', {
            placement: 'top',
        });
        return button;
    }

    /**
     *
     * @param cell
     * @returns {HTMLElement}
     */
    menuCellAlignButtonRight(cell) {
        const self = this;
        const button = create('div', ['cdx-settings-button'], null);
        button.innerHTML = '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="align-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-align-right fa-w-14 fa-3x"><path fill="currentColor" d="M16 224h416a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16H16a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16zm416 192H16a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zm3.17-384H172.83A12.82 12.82 0 0 0 160 44.83v38.34A12.82 12.82 0 0 0 172.83 96h262.34A12.82 12.82 0 0 0 448 83.17V44.83A12.82 12.82 0 0 0 435.17 32zm0 256H172.83A12.82 12.82 0 0 0 160 300.83v38.34A12.82 12.82 0 0 0 172.83 352h262.34A12.82 12.82 0 0 0 448 339.17v-38.34A12.82 12.82 0 0 0 435.17 288z" class=""></path></svg>';
        button.addEventListener('click', function () {
            self.menuCellAlignToggle(cell, 'tc-table__right');
        });
        self.api.tooltip.onHover(button, 'Выровнять по правому краю', {
            placement: 'top',
        });
        return button;
    }

    /**
     *
     * @param cell
     * @param name
     */
    menuCellAlignToggle(cell, name) {
        const classes = [
            'tc-table__center', 'tc-table__left', 'tc-table__right',
        ];
        classes.forEach(function (value) {
            if (cell.classList.contains(value) && value !== name) {
                cell.classList.remove(value);
            }
        });
        if (!cell.classList.contains(name)) {
            cell.classList.add(name);
        } else {
            cell.classList.remove(name);
        }
    }

    /**
     *
     * @param button
     */
    openCellMenu(button) {
        const menu = button.closest('.' + CSS.cellMenu).querySelector('.' + CSS.cellMenuContent).querySelector('.ce-settings');
        menu.classList.add('ce-settings--opened');
    }

    /**
     *
     * @param cell
     */
    removeCellMenu(cell) {
        const menu = cell.querySelector('.' + CSS.cellMenu);
        if (menu) {
            menu.remove();
        }
    }

    /**
     * Inserts column to the right from currently selected cell
     */
    insertColumnAfter() {
        this.insertColumn(1);
        this.focusCellOnSelectedCell();
    }

    /**
     * Inserts column to the left from currently selected cell
     */
    insertColumnBefore() {
        this.insertColumn();
        this.focusCellOnSelectedCell();
    }

    /**
     * Inserts new row below a current row
     */
    insertRowBefore() {
        this.insertRow();
        this.focusCellOnSelectedCell();
    }

    /**
     * Inserts row above a current row
     */
    insertRowAfter() {
        this.insertRow(1);
        this.focusCellOnSelectedCell();
    }

    /**
     * Insert a column into table relatively to a current cell
     * @param {number} direction - direction of insertion. 0 is insertion before, 1 is insertion after
     */
    insertColumn(direction = 0) {
        direction = Math.min(Math.max(direction, 0), 1);

        const insertionIndex = this.selectedCell
            ? this.selectedCell.cellIndex + direction
            : 0;

        this._numberOfColumns++;
        /** Add cell in each row */
        const rows = this._table.rows;

        for (let i = 0; i < rows.length; i++) {
            const cell = rows[i].insertCell(insertionIndex);

            this._fillCell(cell);
        }
    }

    /**
     * Remove column that includes currently selected cell
     * Do nothing if there's no current cell
     */
    deleteColumn() {
        if (!this.selectedCell) {
            return;
        }

        const removalIndex = this.selectedCell.cellIndex;

        this._numberOfColumns--;
        /** Delete cell in each row */
        const rows = this._table.rows;

        for (let i = 0; i < rows.length; i++) {
            rows[i].deleteCell(removalIndex);
        }
    }

    /**
     * Insert a row into table relatively to a current cell
     * @param {number} direction - direction of insertion. 0 is insertion before, 1 is insertion after
     * @return {HTMLElement} row
     */
    insertRow(direction = 0) {
        direction = Math.min(Math.max(direction, 0), 1);

        const insertionIndex = this.selectedRow
            ? this.selectedRow.rowIndex + direction
            : 0;

        const row = this._table.insertRow(insertionIndex);

        this._numberOfRows++;

        this._fillRow(row);
        return row;
    }

    insertRowWithRowSpan(direction = 0, rowSpan = 1) {
        direction = Math.min(Math.max(direction, 0), 1);

        const insertionIndex = this.selectedRow
            ? this.selectedRow.rowIndex + direction
            : 0;

        const row = this._table.insertRow(insertionIndex);
        row.rowSpan = rowSpan;

        this._numberOfRows++;

        this._fillRow(row);
        return row;
    }

    /**
     * Remove row in table on index place
     * @param {number} index - number in the array of columns, where new column to insert,-1 if insert at the end
     */
    deleteRow(index = -1) {
        if (!this.selectedRow) {
            return;
        }

        const removalIndex = this.selectedRow.rowIndex;

        this._table.deleteRow(removalIndex);
        this._numberOfRows--;
    }

    /**
     * get html table wrapper
     * @return {HTMLElement}
     */
    get htmlElement() {
        return this._element;
    }

    /**
     * get real table tag
     * @return {HTMLElement}
     */
    get body() {
        return this._table;
    }

    /**
     * @private
     *
     * Creates table structure
     * @return {HTMLElement} tbody - where rows will be
     */
    _createTableWrapper() {
        return create('div', [CSS.wrapper], null, [
            create('table', [CSS.table])
            // This function can be updated so that it will render the table with the give config instead of 3x3
        ]);
    }

    /**
     * @private
     *
     * Create editable area of cell
     * @return {HTMLElement} - the area
     */
    _createContenteditableArea() {
        return create('div', [CSS.inputField], { contenteditable: 'true' });
    }

    /**
     * @private
     *
     * Fills the empty cell of the editable area
     * @param {HTMLElement} cell - empty cell
     */
    _fillCell(cell) {
        cell.classList.add(CSS.cell);
        const content = this._createContenteditableArea();
        cell.appendChild(create('div', [CSS.area], null, [content]));
    }

    /**
     * @private
     *
     * Fills the empty row with cells  in the size of numberOfColumns
     * @param row = the empty row
     */
    _fillRow(row) {
        for (let i = 0; i < this._numberOfColumns; i++) {
            const cell = row.insertCell();

            this._fillCell(cell);
        }
    }

    /**
     * @private
     *
     * hang necessary events
     */
    _attachEvents() {
        this._table.addEventListener('focus', (event) => {
            this._focusEditField(event);
        }, true);

        this._table.addEventListener('keydown', (event) => {
            this._pressedEnterInEditField(event);
        });

        this._table.addEventListener('click', (event) => {
            this._clickedOnCell(event);
        });

        this.htmlElement.addEventListener('keydown', (event) => {
            this._containerKeydown(event);
        });
    }

    /**
     * @private
     *
     * When you focus on an editable field, remembers the cell
     * @param {FocusEvent} event
     */
    _focusEditField(event) {
        this.selectedCell = event.target.tagName === 'TD'
            ? event.target
            : event.target.closest('td');
    }

    focusCellOnSelectedCell() {
        this.selectedCell.childNodes[0].childNodes[0].focus();
    }

    /**
     * @private
     *
     * When enter is pressed when editing a field
     * @param {KeyboardEvent} event
     */
    _pressedEnterInEditField(event) {
        if (!event.target.classList.contains(CSS.inputField)) {
            return;
        }
        if (event.key === 'Enter' && !event.shiftKey) {
            event.preventDefault();
        }
    }

    /**
     * @private
     *
     * When clicking on a cell
     * @param {MouseEvent} event
     */
    _clickedOnCell(event) {
        console.log(event.target.classList.contains(CSS.cell));
        if (!event.target.classList.contains(CSS.cell)) {
            return;
        }
        console.log(event.target, '.' + CSS.inputField);
        const content = event.target.querySelector('.' + CSS.inputField);

        content.focus();
    }

    /**
     * @private
     *
     * detects button presses when editing a table's content
     * @param {KeyboardEvent} event
     */
    _containerKeydown(event) {
        if (event.key === 'Enter' && event.ctrlKey) {
            this._containerEnterPressed(event);
        }
    }

    /**
     * @private
     *
     * if "Ctrl + Enter" is pressed then create new line under current and focus it
     * @param {KeyboardEvent} event
     */
    _containerEnterPressed(event) {
        const newRow = this.insertRow(1);

        newRow.cells[0].click();
    }
}
