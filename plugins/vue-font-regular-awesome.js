
import { library } from '@fortawesome/fontawesome-svg-core';

import {
    faHeart,
    faFileAlt,
} from '@fortawesome/free-regular-svg-icons';

library.add(faFileAlt);
library.add(faHeart);
