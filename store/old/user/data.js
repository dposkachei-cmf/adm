import api from '../../../api';
import response from '../../../plugins/common/response';

const state = () => ({
    pending: false,
    loaded: false,
});

// getters
const getters = {
    pending(state) {
        return state.pending;
    },
    loaded(state) {
        return state.loaded;
    },
};

// actions
const actions = {
    async fetchData({ state, commit }, data) {
        const self = this;
        commit('pending', true);
        await self.$axios.get(api.user.data)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                self.dispatch('user/favorites/array/setData', result.data.favorites);
                self.dispatch('user/subscriptions/array/setData', result.data.subscriptions);
                self.dispatch('user/views/array/setData', result.data.views);
                self.dispatch('user/votes/array/setData', result.data.votes);
                self.dispatch('user/watches/array/setData', result.data.watches);
                self.dispatch('user/tags/array/setData', result.data.tags);
                if (!state.loaded) {
                    commit('loaded', true);
                }
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        //commit('errors', result.errors);
                    }
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, value) => {
        state.pending = value;
    },
    loaded: (state, value) => {
        state.loaded = value;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
