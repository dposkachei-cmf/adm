import response from '../../../../../plugins/common/response';
import api from '../../../../../api';

// initial state
const state = () => ({
    pending: false,
    errors: {},
});

// getters
const getters = {
    pending(state) {
        return state.pending;
    },
    errors(state) {
        return state.errors;
    },
};

// actions
const actions = {
    async fetchLockToggle({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        const url = api.user.chats.lock.toggle.replace(':id', data.id);
        await self.$axios.post(url)
            .then((res) => {
                commit('pending', false);
                const result = response.success(res);
                if (result.success && result.message !== undefined) {
                    self.$toast.success(result.message);
                }
                self.dispatch('user/chats/fetchChats', {});
                self.$router.push('/m');
            })
            .catch((error) => {
                if (error) {
                    commit('pending', false);
                    const result = response.error(error);
                    if (result.errors !== undefined) {
                        commit('errors', result.errors);
                    }
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    errors: (state, val) => {
        state.errors = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
