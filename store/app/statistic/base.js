import api from "../../../api";
import axiosBase from "../../../plugins/common/axiosBase";

const state = () => ({
    pending: false,
    data: null,
});

const getters = {
    pending(state) {
        return state.pending;
    },
    data(state) {
        return state.data;
    },
};

const actions = {
    /**
     *
     * @param commit
     * @param state
     * @param {} data
     */
    async fetchStatistic({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        //commit('data', null);
        let url = api.statistic.index;
        await axiosBase.init(self.$axios).get(url, [], function (result) {
            commit('pending', false);
            commit('data', result.data);
        }, function (result) {
            commit('pending', false);
            if (result.message !== undefined) {
                self.$toast.error(result.message);
            }
        });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, val) => {
        state.data = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
