export default {
    /**
     *
     * @returns {{limit: (function(*, *=, *=): string)}}
     */
    stringActions() {
        const self = this;

        /**
         *
         * @param {string} value
         * @param {int} max
         * @param {string} end
         * @returns {string}
         */
        const limit = (value, max = 100, end = '...') => {
            if (value && value.length > max) {
                value = value.substring(0, max) + end;
            }
            return value;
        };

        /**
         *
         * @param {string} value
         * @returns {string}
         */
        const limitCardGroup = (value) => {
            return limit(value, self.$device.isDesktop ? 50 : 10);
        };
        return {
            limit,
            limitCardGroup,
        };
    },
};
