import api from "../../../api";
import axiosStore from "../../../plugins/common/axiosStore";
import beforeFetch from "../../../plugins/common/beforeFetch";

const state = () => ({
    pending: false,
    success: false,
});

const getters = {
    pending(state) {
        return state.pending;
    },
    success(state) {
        return state.success;
    },
};

const actions = {
    async fetchBanned({ commit }, data) {
        const self = this;
        commit('pending', true);
        const url = beforeFetch.getTableBanned(api.table.banned, data.model, data.id);
        await axiosStore.init(self.$axios).post(url, {
            value: data.value === true ? 1 : 0,
        }, function (result) {
            commit('pending', false);
            commit('success', true);
            if (result.success && result.message !== undefined) {
                self.$toast.success(result.message);
            }
        }, function (result) {
            commit('pending', false);
        });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    success: (state, val) => {
        state.success = val;
        setTimeout(function () {
            state.success = false;
        }, 500);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
