import api from './../../api';
import axiosStore from "../../plugins/common/axiosStore";

const state = () => ({
    loaded: false,
    pending: false,
    data: {
        config: {
            ENV: 'default',
            DEVELOPMENT_MODE: false,
            LOGO: '',
            NAME: '',
        },
        editor: {},
        dev: [],
    },
});

const getters = {
    loaded(state) {
        return state.loaded;
    },
    data(state) {
        return state.data;
    },
    config(state) {
        return state.data.config;
    },
    pending(state) {
        return state.pending;
    },
};

const actions = {
    async setIndexData({ commit }, data) {
        commit('data', data);
    },
    async fetchIndexData({ commit, state }, data) {
        const self = this;
        if (state.loaded) {
            return;
        }
        commit('pending', true);
        await axiosStore.init(self.$axios).get(api.index.data, {}, function (result) {
            commit('pending', false);
            commit('data', result.data);
            if (!state.loaded) {
                commit('loaded', true);
            }
        }, function () {
            commit('pending', false);
        });
    },
    async fetchIndexDataForce({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        await axiosStore.init(self.$axios).get(api.index.data, {}, function (result) {
            commit('pending', false);
            commit('data', result.data);
            if (!state.loaded) {
                commit('loaded', true);
            }
        }, function () {
            commit('pending', false);
        });
    },
};

const mutations = {
    loaded: (state, val) => {
        state.loaded = val;
    },
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, val) => {
        state.data = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
