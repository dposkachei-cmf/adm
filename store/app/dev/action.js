import api from "../../../api";
import axiosBase from "../../../plugins/common/axiosBase";

const state = () => ({
    pending: false,
    success: false,
    action: null,
});

const getters = {
    pending(state) {
        return state.pending;
    },
    success(state) {
        return state.success;
    },
    action(state) {
        return state.action;
    },
};

const actions = {
    /**
     *
     * @param commit
     * @param state
     * @param {{action: string, callback: string|undefined}} data
     */
    async fetchAction({ commit, state }, data) {
        const self = this;
        commit('pending', true);
        commit('action', data.action);
        let url = api.dev.action;
        await axiosBase.init(self.$axios).post(url, data, function (result) {
            commit('pending', false);
            commit('success', true);
            if (result.message !== undefined) {
                self.$toast.success(result.message);
            }
            if (data.callback !== undefined) {
                if (data.callback === 'reload') {
                    document.location.reload();
                }
            }
        }, function (result) {
            commit('pending', false);
            if (result.message !== undefined) {
                self.$toast.error(result.message);
            }
        });
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    success: (state, val) => {
        state.success = val;
    },
    action: (state, val) => {
        state.action = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
