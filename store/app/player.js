const state = () => ({
    active: null,
    players: [],
});

const getters = {
    active(state) {
        return state.active;
    },
    players(state) {
        return state.players;
    },
};

const actions = {
    setActive({ commit }, value) {
        commit('active', value);
    },
    pushPlayers({ commit }, value) {
        commit('pushPlayers', value);
    },
    clearPlayers({ commit }, value) {
        commit('players', []);
    },
};

const mutations = {
    active: (state, val) => {
        state.active = val;
    },
    pushPlayers: (state, val) => {
        state.players.push(val);
    },
    players: (state, val) => {
        state.players = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
