export default ({ app, redirect, store }) => {
    // the following look directly for the cookie created by nuxtjs/auth
    // instead of using $auth.loggedIn

    if (app.$auth.$state.loggedIn) {
        const loaded = store.getters['user/favorites/array/loaded'];

        if (!loaded) {
            //store.dispatch('user/favorites/array/fetchFavorites');
        }
    }
};
