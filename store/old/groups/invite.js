import response from '../../../plugins/common/response';
import api from '../../../api';

const state = () => ({
    pending: false,
    success: null,
    data: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    success(state) {
        return state.success;
    },
};

// actions
const actions = {
    async fetchInvite({ commit }, data) {
        const self = this;
        commit('success', null);
        commit('pending', true);
        let url = api.groups.invite.replace(':id', data.id);
        if (data.token !== undefined) {
            url += '?token=' + data.token;
        }
        await self.$axios.post(url)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('success', true);
                if (result.data.token !== undefined) {
                    self.$auth.setUserToken(result.data.token).then(() => {
                        //window.location.href = '/';
                    });
                } else {
                    // setTimeout(function () {
                    //     self.dispatch('modal/setCurrent', null);
                    // }, 3000);
                }
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                    commit('success', false);
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    success: (state, val) => {
        state.success = val;
    },
    data: (state, data) => {
        state.data = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
