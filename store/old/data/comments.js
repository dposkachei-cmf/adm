import response from '../../../plugins/common/response';
import api from '../../../api';

const state = () => ({
    loaded: false,
    pending: true,
    withPending: true,
    data: [],
});

const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    loaded(state) {
        return state.loaded;
    },
};

const actions = {
    async fetchComments({ commit, state }, data) {
        const self = this;
        if (state.loaded) {
            return;
        }
        if (state.withPending) {
            commit('pending', true);
        }
        await self.$axios.get(api.index.comments)
            .then((res) => {
                if (state.withPending) {
                    commit('pending', false);
                } else {
                    commit('withPending', true);
                }
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
                if (!state.loaded) {
                    commit('loaded', true);
                }
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                }
            });
    },
    async fetchCommentsRefresh({ commit, state }, data) {
        const self = this;
        commit('loaded', false);
        await self.dispatch('data/comments/fetchComments', data);
    },
    async fetchCommentsRefreshWithoutPending({ commit, state }, data) {
        const self = this;
        commit('loaded', false);
        commit('withPending', false);
        await self.dispatch('data/comments/fetchComments', data);
    },
    async pushComment({ commit, state }, data) {
        await commit('dataPush', data);
    },
};

const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    withPending: (state, val) => {
        state.withPending = val;
    },
    data: (state, val) => {
        state.data = val;
    },
    dataPush: (state, data) => {
        state.data.unshift(data);
    },
    loaded: (state, val) => {
        state.loaded = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
