const state = () => ({
    chats: 0,
    notifications: 0,
});

// getters
const getters = {
    chats(state) {
        return state.chats;
    },
    notifications(state) {
        return state.notifications;
    },
};

// actions
const actions = {
    incrementChats({ commit, state }, data) {
        const value = state.chats + 1;
        commit('chats', value);
    },
    clearChatsCounters({ commit }, data) {
        commit('chats', 0);
    },
    incrementNotifications({ commit, state }, data) {
        const value = state.notifications + 1;
        commit('notifications', value);
    },
    clearNotificationsCounters({ commit }, data) {
        commit('notifications', 0);
    }
};

// mutations
const mutations = {
    chats: (state, value) => {
        state.chats = value;
    },
    notifications: (state, value) => {
        state.notifications = value;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
