import response from '../../../plugins/common/response';
import api from '../../../api';

const state = () => ({
    pending: false,
    data: null,
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
};

// actions
const actions = {
    async fetchData({ commit }, data) {
        const self = this;
        commit('pending', true);
        let url = api.groups.show.replace(':id', data.id);
        if (data.token !== undefined) {
            url += '?token=' + data.token;
        }
        await self.$axios.get(url)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('data', result.data);
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                }
            });
    },
    incrementCounterSubscribers({ commit, state }, data) {
        state.data.counters.subscribers++;
    },
    setCounters({ commit }, data) {
        commit('dataCounters', data);
    }
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    data: (state, data) => {
        state.data = data;
    },
    dataCounters: (state, data) => {
        state.data.counters = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
