import response from '../../../../../plugins/common/response';
import api from '../../../../../api';
import moment from "moment";

const state = () => ({
    success: false,
    pending: false,
    data: [],
});

// getters
const getters = {
    data(state) {
        return state.data;
    },
    pending(state) {
        return state.pending;
    },
    success(state) {
        return state.success;
    },
};

// actions
const actions = {
    async fetchStore({ commit }, data) {
        const self = this;
        commit('pending', true);
        const url = api.user.chats.messages.store.replace(':id', data.id);
        //delete data.id;
        await self.$axios.post(url, data)
            .then((res) => {
                commit('pending', false);
                if (res.status !== 200) {
                    return;
                }
                const result = response.success(res);
                commit('success', true);
                commit('data', result.data);
                // self.dispatch('user/chats/messages/fetchData', {
                //     id: data.id,
                // });
                // self.dispatch('user/chats/setLastMessage', {
                //     id: data.id,
                //     last_message_user_id: self.$auth.user.id,
                //     last_message: data.message,
                //     last_message_at: data.date,
                // });
            })
            .catch((error) => {
                if (error) {
                    response.error(error);
                    commit('pending', false);
                }
            });
    },
};

// mutations
const mutations = {
    pending: (state, val) => {
        state.pending = val;
    },
    success: (state, val) => {
        state.success = val;
        setTimeout(function () {
            state.success = false;
        }, 1500);
    },
    data: (state, data) => {
        state.data = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
