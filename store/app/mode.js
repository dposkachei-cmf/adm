const state = () => ({
    developer: false,
});

const getters = {
    developer(state) {
        return state.developer;
    },
};

const actions = {
    setDeveloper({ commit }, name) {
        commit('developer', name);
    },
};

const mutations = {
    developer: (state, val) => {
        state.developer = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
