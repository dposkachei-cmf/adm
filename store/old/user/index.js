// initial state
import response from "../../../plugins/common/response";

const state = () => ({
    counters: {
        chats: 0,
    }
});

// getters
const getters = {
    counters(state) {
        return state.counters;
    },
};

// actions
const actions = {
    incrementCountersChats({ commit, state }, data) {
        const value = state.counters.chats + 1;
        commit('countersChats', value);
    },
    clearCountersChats({ commit }, data) {
        commit('countersChats', 0);
    }
};

// mutations
const mutations = {
    countersChats: (state, value) => {
        state.counters.chats = value;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
